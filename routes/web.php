<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', 'SiteController@index')->name('home');

Route::get('/categories/{slug}', 'CategoriesController@show')->name('categories');

Route::get('/products/{slug}', 'ProductsController@show')->name('products');

Route::get('/about', 'PagesController@about')->name('about');
Route::get('/contacts', 'PagesController@contacts')->name('contacts');

Route::get('design-interior', 'DesignInteriorsController@index')->name('design-interior');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function(){
    Route::get('/', 'CategoriesController@index')->name('admin');

    Route::resource('/categories', 'CategoriesController');
    Route::get('/category-by-style/{id}', 'CategoriesController@getCategoriesByStyle')->name('category-by-style');

    Route::resource('/styles', 'StylesController');

    Route::resource('/pages', 'PagesController');

    Route::resource('/products', 'ProductsController');

    Route::resource('/designinteriors', 'DesignInteriorController');

    Route::get('/siteinfo', 'SiteinfosController@index')->name('siteinfo.index');
    Route::get('/siteinfo/edit', 'SiteinfosController@edit')->name('siteinfo.edit');
    Route::put('/siteinfo/{siteinfo}', 'SiteinfosController@update')->name('siteinfo.update');

    Route::resource('/administrators', 'AdministratorsController');
    Route::resource('/slideshows', 'BannerSlideshowController');
});