const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


//--- Admin Assets -----------------------------------------------

mix.styles([
    'resources/assets/admin/css/bootstrap.min.css',
    'resources/assets/admin/css/font-awesome.min.css',
    'resources/assets/admin/css/themify-icons.css',
    'resources/assets/admin/css/metisMenu.css',
    'resources/assets/admin/css/owl.carousel.min.css',
    'resources/assets/admin/css/slicknav.min.css',
    'resources/assets/admin/css/typography.css',
    'resources/assets/admin/css/default-css.css',
    'resources/assets/admin/css/styles.css',
    'resources/assets/admin/css/responsive.css',
], 'public/admin-assets/css/admin.css');

mix.scripts([
    'resources/assets/admin/js/vendor/jquery-2.2.4.min.js',
    'resources/assets/admin/js/popper.min.js',
    'resources/assets/admin/js/bootstrap.min.js',
    'resources/assets/admin/js/owl.carousel.min.js',
    'resources/assets/admin/js/metisMenu.min.js',
    'resources/assets/admin/js/jquery.slimscroll.min.js',
    'resources/assets/admin/js/jquery.slicknav.min.js',
], 'public/admin-assets/js/admin.js');

mix.scripts([
    'resources/assets/admin/js/line-chart.js',
    'resources/assets/admin/js/pie-chart.js',
    'resources/assets/admin/js/bar-chart.js',
], 'public/admin-assets/js/admin-charts.js');

mix.scripts('resources/assets/admin/js/maps.js', 'public/admin-assets/js/admin-maps.js');
mix.scripts('resources/assets/admin/js/plugins.js', 'public/admin-assets/js/admin-plugins.js');
mix.scripts('resources/assets/admin/js/scripts.js', 'public/admin-assets/js/admin-scripts.js');
mix.scripts('resources/assets/admin/js/vendor/modernizr-2.8.3.min.js', 'public/admin-assets/js/modernizr-2.8.3.min.js');

mix.copy('resources/assets/admin/images', 'public/admin-assets/img/');

mix.copy([
    'resources/assets/admin/fonts',
], 'public/admin-assets/fonts');

//----------------------------------------------------------------

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/common.js', 'public/js')
   .js('resources/js/index.js', 'public/js')
   .js('resources/js/contacts.js', 'public/js')
   .js('resources/js/product.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   // .sass('resources/sass/style.sass', 'public/css');

mix.copy('resources/css/style.css', 'public/css/');
mix.copy('resources/css/style.css.map', 'public/css/');
mix.copy('resources/fonts', 'public/fonts');
mix.copy('resources/img', 'public/img/');