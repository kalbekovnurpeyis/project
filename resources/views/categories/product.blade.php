@extends('layouts.base')
@section('title', $product->title)

@section('scripts')
    <script src="{{ asset('js/product.js') }}"></script>
@endsection

@section('content')
    <div class="product-images-slider">
        <div id="product-big-slick">
            <div class="item"><a data-fancybox="product-img" href="../../img/product-img-1.jpg"><img src="../img/product-img-1.jpg" alt="product-img-1.jpg"></a></div>
            <div class="item"><a data-fancybox="product-img" href="../../img/product-img-2.jpg"><img src="../img/product-img-2.jpg" alt="product-img-2.jpg"></a></div>
            <div class="item"><a data-fancybox="product-img" href="../../img/product-img-3.jpg"><img src="../img/product-img-3.jpg" alt="product-img-3.jpg"></a></div>
            <div class="item"><a data-fancybox="product-img" href="../../img/product-img-4.jpg"><img src="../img/product-img-4.jpg" alt="product-img-4.jpg"></a></div>
            <div class="item"><a data-fancybox="product-img" href="../../img/product-img-5.jpg"><img src="../img/product-img-5.jpg" alt="product-img-5.jpg"></a></div>
        </div>
    </div>
    <div class="container product-page">
        <div class="page-header">
            <h1>{{ $product->title }}</h1>

            {{ Breadcrumbs::render('product', $product) }}

            <div class="product-page-content">
                <div class="product-price"><span>от</span><span>{{ $product->price }}</span><span>сом.</span></div>
                <div class="product-attributes">
                    <table class="table table-striped">
                        <tr>
                            <th scope="row">Стиль:</th>
                            <td>{{ $product->style->title }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Основа фасада:</th>
                            <td>МДФ</td>
                        </tr>
                        <tr>
                            <th scope="row">Покрытие:</th>
                            <td>Супермат</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection