@extends('layouts.base')
@section('title', 'Категории')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>{{ $category->title }}</h1>

            {{ Breadcrumbs::render('category', $category) }}
        </div>
        <div class="page-content">
            {!! $category->description !!}
        </div>
        @if($products->total())
        <div class="product-card-list">
            <div class="row justify-content-center">
                @foreach($products as $product)
                    <div class="col-lg-4 col-md-6">
                        <a class="product-card" href="{{ route('products', $product->slug) }}">
                            <div class="card-image-holder">
                                <img src="{{ asset('uploads/348x261-' . $product->image) }}" alt="{{ $product->image }}">
                            </div>
                            <div class="card-content">
                                <div class="card-content-top">
                                    <h4 class="product-title">{{ $product->title }}</h4>
                                    <div class="price"><span>от {{ $product->price }} сом.</span></div>
                                </div>
                                <div class="card-content-bottom">
                                    <p><span>Стиль: </span>{{ $product->style->title }}</p>
                                    <p><span>Основа фасада: </span>МДФ</p>
                                    <p><span>Покрытие: </span>супермат</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="pagintation-row">
                {{ $products->links() }}
            </div>
        </div>
        @endif
    </div>
@endsection