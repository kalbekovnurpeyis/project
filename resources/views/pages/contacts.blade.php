@extends('layouts.base')
@section('title', 'Контакты')

@section('scripts')
<script src="https://api-maps.yandex.ru/2.1/?lang=en_RU&amp;amp;apikey=015c8e79-f718-46ff-b087-414cf51c8a13" type="text/javascript"></script>
<script src="{{ asset('js/contacts.js') }}"></script>
<script>
    ymaps.ready(function () {
        let dastanInterior = new ymaps.Map('map',
            {
                center: [{{ $siteInfo->coordinates }}],
                zoom: 15
            },
            {
                searchControlProvider: 'yandex#search'
            }
        );
        dastanInterior.geoObjects.add(new ymaps.Placemark([{{ $siteInfo->coordinates }}],
            {
                hintContent: "2-этаж, такой то бутик",
                balloonContent: "2-этаж, такой то бутик",
                iconContent: "DI"
            },
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/forMap.png',
                iconImageSize: [25, 44],
                iconImageOffset: [-20, -40]
            }
        ));
        dastanInterior.behaviors.disable("scrollZoom");
    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="page-header">
        <h1>Контакты</h1>

        {{ Breadcrumbs::render('contacts') }}
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="contacts-item">
                <div class="item-label"><i class="icon ion-ios-compass"></i>
                    <p>Наш адрес</p>
                </div>
                <div class="item-content">
                    <p>{{ $siteInfo->address }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="contacts-item">
                <div class="item-label"><i class="icon ion-ios-call"></i>
                    <p>Наши телефоны</p>
                </div>
                <div class="item-content">
                    @if(!empty($siteInfo->phone_number_1))
                        <p>{{ $siteInfo->phone_number_1 }}</p>
                    @endif
                    @if(!empty($siteInfo->phone_number_2))
                        <p>{{ $siteInfo->phone_number_2 }}</p>
                    @endif
                    @if(!empty($siteInfo->phone_number_3))
                        <p>{{ $siteInfo->phone_number_3 }}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="contacts-item">
                <div class="item-label"><i class="icon ion-ios-time"></i>
                    <p>График работы</p>
                </div>
                <div class="item-content">
                    <p>{{ $siteInfo->work_time }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="map"></div>
@endsection