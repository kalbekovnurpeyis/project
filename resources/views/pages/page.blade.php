@extends('layouts.base')
@section('title', $page->title)

@section('content')
<div class="container">
    <div class="page-header">
        <h1>{{ $page->title }}</h1>

        {{ Breadcrumbs::render('contacts', $page) }}
    </div>
    <div class="page-content">
        {!! $page->content !!}
    </div>
</div>
@endsection