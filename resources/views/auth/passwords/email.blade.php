@extends('admin.layouts.base')

@section('base')
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box ptb--100">
                {!! Form::open(['route' => 'password.email', 'method' => 'post', 'autocomplete' => 'off']) !!}
                <div class="login-form-head">
                    <h4>{{ __('Сбросить пароль') }}</h4>
                    <p>Чтобы сбросить пароль, вам на почту будет отправлено ссылка.</p>
                    @if (session('status'))
                        <p class="text-success">{{ session('status') }}</p>
                    @endif
                    @if ($errors->has('email'))
                        <p class="form-text text-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="login-form-body">
                    <div class="form-gp{{ $errors->has('email') ? ' is-invalid' : '' }}">
                        <label for="email">{{ __('E-Mail адрес') }}</label>
                        <i class="ti-email"></i>
                        <input type="email" id="email" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>
                    </div>

                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">{{ __('Отправить ссылку для сброса') }} <i class="ti-arrow-right"></i></button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- login area end -->
@endsection

