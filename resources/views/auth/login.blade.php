@extends('admin.layouts.base')

@section('base')
<!-- login area start -->
<div class="login-area login-s2">
    <div class="container">
        <div class="login-box ptb--100">
            {!! Form::open(['route' => 'login', 'method' => 'post', 'autocomplete' => 'off']) !!}
                <div class="login-form-head">
                    <h4>{{ __('Войти') }}</h4>
                    <p>Здравствуйте! Войдите в систему, чтобы управлять вашим сайтом.</p>
                    @if ($errors->has('email'))
                        <p class="form-text text-danger">{{ $errors->first('email') }}</p>
                    @endif
                    @if ($errors->has('password'))
                        <p class="form-text text-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>
                <div class="login-form-body">
                    <div class="form-gp{{ $errors->has('email') ? ' is-invalid' : '' }}">
                        <label for="email">{{ __('E-Mail адрес') }}</label>
                        <i class="ti-email"></i>
                        <input type="email" id="email" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>
                    </div>

                    <div class="form-gp{{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <label for="password">{{ __('Пароль') }}</label>
                        <i class="ti-lock"></i>
                        <input type="password" id="password" name="password" required autocomplete="off">
                    </div>

                    <div class="row mb-4 rmber-area">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="remember">{{ __('Запомнить') }}</label>
                            </div>
                        </div>
                        @if (Route::has('password.request'))
                            <div class="col-6 text-right">
                                <a href="{{ route('password.request') }}">{{ __('Забыли пароль?') }}</a>
                            </div>
                        @endif
                    </div>

                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">{{ __('Войти') }} <i class="ti-arrow-right"></i></button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- login area end -->
@endsection
