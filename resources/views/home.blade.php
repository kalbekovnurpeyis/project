@extends('layouts.base')

@section('scripts')
<script src="{{ asset('js/index.js') }}"></script>
@endsection

@section('content')
<div id="banner-slider">
    <div id="banner-slick"><a class="item" href="#">
            <div class="caption"><span>Индивидуальность в каждой детали</span></div><img src="../img/slide.jpg" alt="slide.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>То, о чем мечтали</span></div><img src="../img/slide2.jpg" alt="slide2.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>Мы подумали об о всем</span></div><img src="../img/slide3.jpg" alt="slide3.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>То, о чем мечтали</span></div><img src="../img/slide2.jpg" alt="slide2.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>Индивидуальность в каждой детали</span></div><img src="../img/slide.jpg" alt="slide.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>То, о чем мечтали</span></div><img src="../img/slide2.jpg" alt="slide2.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>Мы подумали об о всем</span></div><img src="../img/slide3.jpg" alt="slide3.jpg">
        </a><a class="item" href="#">
            <div class="caption"><span>То, о чем мечтали</span></div><img src="../img/slide2.jpg" alt="slide2.jpg">
        </a></div>
</div>
<div id="advantages">
    <div class="advantage">
        <div class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width: 100%; height: 100%;">
                <use xlink:href="../../img/007-kitchen-1.svg#kitchen"></use>
            </svg>
        </div>
        <div class="txt">
            <p>500</p>
            <div>Эксклюзивных моделей кухонь</div>
        </div>
    </div>
    <div class="advantage">
        <div class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width: 100%; height: 100%;">
                <use xlink:href="../../img/010-new-year.svg#calendar"></use>
            </svg>
        </div>
        <div class="txt">
            <p>10 <span>лет</span></p>
            <div>Изготавливаем качественную мебель</div>
        </div>
    </div>
    <div class="advantage">
        <div class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 155.123 155.123" style="width: 100%; height: 100%;">
                <use xlink:href="../../img/009-thumb-up.svg#thumb-up"></use>
            </svg>
        </div>
        <div class="txt">
            <p>37000<span>+</span></p>
            <div>Довольных клиентов</div>
        </div>
    </div>
    <div class="advantage">
        <div class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="width: 100%; height: 100%;">
                <use xlink:href="../../img/008-stopwatch.svg#stopwatch"></use>
            </svg>
        </div>
        <div class="txt">
            <p><span>от </span>14 <span>дней</span></p>
            <div>Изготавливаем кухни вашей мечты</div>
        </div>
    </div>
</div>
<div class="vertical-padding" id="welocome-txt">
    <div class="container">
        <h1 class="d-title color-grey">Планируете купить Мебель на заказ по индивидуальному проекту?</h1>
        <p>Тогда добро пожаловать на сайт творческой мастерской дизайна и мебели «ДАСТАН», которая специализируется на изготовлении мебели на заказ и готова предложить Вам возможность купить мебель, которая будет полностью отвечать Вашим представлениям о красоте, практичности, функциональности и удобстве в эксплуатации, а её стоимость не будет обременительна для вашего бюджета.</p>
    </div>
</div>
<div class="vertical-padding top-false" id="last-works">
    <div class="container">
        <h2 class="d-title color-grey">Последние Наши Работы</h2>
    </div>
    <div id="last-work-slick">
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-1.jpg"><img src="../img/portfolio-1.jpg" alt="portfolio-1.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-2.jpg"><img src="../img/portfolio-2.jpg" alt="portfolio-2.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-3.jpg"><img src="../img/portfolio-3.jpg" alt="portfolio-3.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-4.jpg"><img src="../img/portfolio-4.jpg" alt="portfolio-4.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-1.jpg"><img src="../img/portfolio-1.jpg" alt="portfolio-1.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-2.jpg"><img src="../img/portfolio-2.jpg" alt="portfolio-2.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-3.jpg"><img src="../img/portfolio-3.jpg" alt="portfolio-3.jpg"></a></div>
        <div class="item"><a data-fancybox="portfolio" href="../../img/portfolio-4.jpg"><img src="../img/portfolio-4.jpg" alt="portfolio-4.jpg"></a></div>
    </div><a class="link-btn" href="#"><span>Посмотреть другие работы</span></a>
</div>
<div class="vertical-padding" id="order-subscribe">
    <div class="container">
        <h2 class="d-title color-aqua">Записаться на бесплатный замер, дизайн проекта, обсуждение изготовления Вашей мебели</h2>
        <form class="subscribe-form" action="#" autocomplete="off">
            <div class="row">
                <div class="col-md-4">
                    <div class="subscribe-input-field">
                        <input class="subscribe-input" type="text" id="fullname" autocomplete="off">
                        <label class="field-label" for="fullname">Введите имя</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="subscribe-input-field">
                        <input class="subscribe-input" type="text" id="telephone" autocomplete="off">
                        <label class="field-label" for="telephone">Введите номер телефона</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="subscribe-input-field">
                        <input class="subscribe-input" type="text" id="email" autocomplete="off">
                        <label class="field-label" for="email">Введите e-mail</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="vertical-padding" id="brands-list">
    <div class="container">
        <h2 class="d-title color-grey">При изготовлении Мебелей Мы Используем Материалы и Фурнитуры Именитых Брендов</h2>
    </div>
    <div class="brand-logo-list">
        <div class="brand-logo"><img src="../img/brand-1.png" alt="brand-1.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-2.png" alt="brand-2.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-3.png" alt="brand-3.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-4.png" alt="brand-4.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-5.png" alt="brand-5.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-6.png" alt="brand-6.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-7.png" alt="brand-7.png">
        </div>
        <div class="brand-logo"><img src="../img/brand-8.png" alt="brand-8.png">
        </div>
    </div>
</div>
@endsection
