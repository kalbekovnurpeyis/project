@extends('admin.layouts.base')

@section('base')
<!-- page container area start -->
<div class="page-container">

    @include('admin.layouts.sidebar')

    <!-- main content area start -->
    <div class="main-content">
        @include('admin.layouts.header')

        <div class="main-content-inner">
            @yield('content')
        </div>
    </div>
    <!-- main content area end -->

    @include('admin.layouts.footer')
</div>
<!-- page container area end -->
@endsection