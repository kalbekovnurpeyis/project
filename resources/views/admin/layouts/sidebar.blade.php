<?php
    $sideBarMenu = [
//        ['icon' => 'dashboard', 'route' => 'admin', 'title' => 'Панель'],
        ['icon' => 'folder', 'route' => 'categories', 'title' => 'Категории'],
        ['icon' => 'layers', 'route' => 'styles', 'title' => 'Стили'],
        ['icon' => 'archive', 'route' => 'products', 'title' => 'Продукты'],
        ['icon' => 'files', 'route' => 'pages', 'title' => 'Страницы'],
        ['icon' => 'brush-alt', 'route' => 'designinteriors', 'title' => 'Дизайн интерьер'],
        ['icon' => 'image', 'route' => 'slideshows', 'title' => 'Слайдеры'],
        ['icon' => 'agenda', 'route' => 'siteinfo', 'title' => 'Данные сайта'],
        ['icon' => 'lock', 'route' => 'administrators', 'title' => 'Администраторы'],
    ];
?>

<!-- sidebar menu area start -->
<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="{{ route('admin') }}"><img src="{{ asset('admin-assets/img/icon/logo.svg') }}" alt="logo"></a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    @foreach($sideBarMenu as $menu)
                        <li {{ url()->current() == route($menu['route'] . '.index') ? 'class=active' : '' }}>
                            <a href="{{ route($menu['route'] . '.index') }}">
                                <i class="ti-{{ $menu['icon'] }}"></i>
                                <span>{{ $menu['title'] }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- sidebar menu area end -->