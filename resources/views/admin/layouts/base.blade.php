<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dastan Интерьер</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('admin-assets/img/icon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('admin-assets/img/icon/favicon-96x96.png') }}" sizes="96x96">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

    <link rel="stylesheet" href="{{ asset('admin-assets/css/admin.css') }}">

    <!-- modernizr css -->
    <script src="{{ asset('admin-assets/js/modernizr-2.8.3.min.js') }}"></script>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->

@yield('base')

<script src="{{ asset('admin-assets/js/admin.js') }}"></script>

<!-- start chart js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

<!-- start highcharts js -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- start amcharts -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/ammap.js"></script>
<script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<!-- all chart activation -->
<script src="{{ asset('admin-assets/js/admin-charts.js') }}"></script>

<!-- all map chart -->
<script src="{{ asset('admin-assets/js/admin-maps.js') }}"></script>

@yield('scripts')

<!-- others plugins -->
<script src="{{ asset('admin-assets/js/admin-plugins.js') }}"></script>
<script src="{{ asset('admin-assets/js/admin-scripts.js') }}"></script>
</body>

</html>
