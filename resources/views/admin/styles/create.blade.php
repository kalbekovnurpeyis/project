@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            {!! Form::open(['route' => 'styles.store', 'method' => 'post']) !!}
                <div class="row align-items-center">
                    <div class="col-md-9">
                        <h4 class="header-title">Создание записи</h4>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary btn-xs btn-block form-group">Сохранить</button>
                    </div>
                </div>

                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">{{ $error }}</div>
                    @endforeach
                @endif

                <div class="form-group">
                    <label for="title" class="col-form-label">Название</label>
                    <input class="form-control" type="text" name="title" value="{{ old('title') }}" id="title">
                </div>
                <div class="form-group">
                    <label for="slug" class="col-form-label">Slug</label>
                    <input class="form-control" type="text" name="slug" value="{{ old('slug') }}" id="slug">
                    <small class="form-text text-muted">Оставьте пустым, что бы создать автоматически</small>
                </div>
                <div class="form-group">
                    <label for="description" class="col-form-label">Описание</label>
                    <textarea name="description" id="description" class="ckeditor" cols="30" rows="10">
                        {{ old('description') }}
                    </textarea>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection