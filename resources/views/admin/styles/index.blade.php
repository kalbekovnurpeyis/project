@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Список записей</h4>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('styles.create') }}">
                        <button type="button" class="btn btn-primary btn-xs btn-block form-group">
                            Создать новую
                        </button>
                    </a>
                </div>
            </div>

            <div class="single-table">
                <div class="table-responsive">
                    @if($styles->total())
                        <table class="table table-hover progress-table text-center">
                            <thead class="text-uppercase">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($styles as $style)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $style->title }}</td>
                                    <td>
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3">
                                                <a href="{{ route('styles.edit', $style->id) }}" class="text-secondary">
                                                    <i class="ti-pencil-alt"></i>
                                                </a>
                                            </li>
                                            <li>
                                                {!! Form::open(['route' => ['styles.destroy', $style->id], 'method' => 'delete']) !!}
                                                <button type="submit" class="text-danger wrap-btn" onclick="return confirm('Вы уверены?')">
                                                    <i class="ti-trash"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav class="d-flex justify-content-end mt-3 pt-3 border-top">
                            {{ $styles->links() }}
                        </nav>
                    @else
                        <p class="text-muted font-14 mb-4">Нет записей</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection