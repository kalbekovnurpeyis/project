@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Список записей</h4>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('pages.create') }}">
                        <button type="button" class="btn btn-primary btn-xs btn-block form-group">
                            Создать новую
                        </button>
                    </a>
                </div>
            </div>

            <div class="single-table">
                <div class="table-responsive">
                    @if($pages->total())
                        <table class="table table-hover progress-table text-center">
                            <thead class="text-uppercase">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Изображение</th>
                                <th scope="col">Название</th>
                                <th scope="col">Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $page)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <th scope="row">
                                        <img src="/uploads/200x200-{{ $page->image }}" width="50px" height="50px" alt="">
                                    </th>
                                    <td>{{ $page->title }}</td>
                                    <td>
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3">
                                                <a href="{{ route('pages.edit', $page->id) }}" class="text-secondary">
                                                    <i class="ti-pencil-alt"></i>
                                                </a>
                                            </li>
                                            <li>
                                                {!! Form::open(['route' => ['pages.destroy', $page->id], 'method' => 'delete']) !!}
                                                <button type="submit" class="text-danger wrap-btn" onclick="return confirm('Вы уверены?')">
                                                    <i class="ti-trash"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav class="d-flex justify-content-end mt-3 pt-3 border-top">
                            {{ $pages->links() }}
                        </nav>
                    @else
                        <p class="text-muted font-14 mb-4">Нет записей</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection