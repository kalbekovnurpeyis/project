@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            {!! Form::open(['route' => 'products.store', 'method' => 'post', 'files' => true]) !!}
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Создание записи</h4>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-xs btn-block form-group">Сохранить</button>
                </div>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">{{ $error }}</div>
                @endforeach
            @endif

            <div class="form-group">
                <label for="title" class="col-form-label">Название</label>
                <input class="form-control" type="text" name="title" value="{{ old('title') }}" id="title">
            </div>
            <div class="form-group">
                <label for="slug" class="col-form-label">Slug</label>
                <input class="form-control" type="text" name="slug" value="{{ old('slug') }}" id="slug">
                <small class="form-text text-muted">Оставьте пустым, что бы создать автоматически</small>
            </div>
            <div class="form-group">
                <label class="col-form-label">Стиль</label>
                <select class="form-control" name="style_id" id="productStyle" onchange="getCategoryByStyle(this.value, 'productCategory')">
                    <option value="">Выберите стиль</option>
                    @foreach($styles as $id => $title)
                        <option value="{{ $id }}">{{ $title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="col-form-label">Категория</label>
                <select class="form-control" name="category_id"id="productCategory" disabled>
                    <option value="">Выберите категорию</option>
                </select>
            </div>
            <div class="form-group">
                <label for="price" class="col-form-label">Цена</label>
                <input class="form-control" type="text" name="price" value="{{ old('price') }}" id="price">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Загрузка миниатюры</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input custom-image-uploader" name="image" id="image" accept="image/*">
                                <label class="custom-file-label" for="image">Выберите изображение</label>
                            </div>
                        </div>
                        <div class="image-drop-zone" drop-from="image"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Загрузка изображений</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input custom-image-uploader" name="gallery[]" id="gallery" accept="image/*" multiple>
                                <label class="custom-file-label" for="gallery">Выберите изображении</label>
                            </div>
                        </div>
                        <div class="image-drop-zone" drop-from="gallery"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-form-label">Описание</label>
                <textarea name="description" id="description" class="ckeditor" cols="30" rows="10">
                    {{ old('description') }}
                </textarea>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection