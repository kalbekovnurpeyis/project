@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            {!! Form::open(['route' => ['siteinfo.update', $siteinfo->id], 'method' => 'put']) !!}
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Редактирование записи</h4>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-xs btn-block form-group">Сохранить</button>
                </div>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">{{ $error }}</div>
                @endforeach
            @endif

            <div class="form-group">
                <label for="phone_number_1" class="col-form-label">Телефон</label>
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="phone_number_1" value="{{ $siteinfo->phone_number_1 }}" id="phone_number_1">
                    </div>
                    <div class="col-md-2">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="whatsapp_1" name="whatsapp" value="1" class="custom-control-input" {{ $siteinfo->whatsapp == 1 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="whatsapp_1">what's app</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="phone_number_2" class="col-form-label">Дополнительный телефон</label>
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="phone_number_2" value="{{ $siteinfo->phone_number_2 }}" id="phone_number_2">
                    </div>
                    <div class="col-md-2">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="whatsapp_2" name="whatsapp" value="2" class="custom-control-input" {{ $siteinfo->whatsapp == 2 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="whatsapp_2">what's app</label>
                        </div>
                    </div>
                </div>
                <small class="form-text text-muted">Оставьте пустым, если этого пункта не нужно</small>
            </div>
            <div class="form-group">
                <label for="phone_number_3" class="col-form-label">Дополнительный телефон</label>
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="phone_number_3" value="{{ $siteinfo->phone_number_3 }}" id="phone_number_3">
                    </div>
                    <div class="col-md-2">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="whatsapp_3" name="whatsapp" value="3" class="custom-control-input" {{ $siteinfo->whatsapp == 3 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="whatsapp_3">what's app</label>
                        </div>
                    </div>
                </div>
                <small class="form-text text-muted">Оставьте пустым, если этого пункта не нужно</small>
            </div>
            <div class="form-group">
                <label for="email" class="col-form-label">E-mail</label>
                <input class="form-control" type="text" name="email" value="{{ $siteinfo->email }}" id="email">
            </div>
            <div class="form-group">
                <label for="address" class="col-form-label">Адрес</label>
                <input class="form-control" type="text" name="address" value="{{ $siteinfo->address }}" id="address">
            </div>
            <div class="form-group">
                <label for="coordinates" class="col-form-label">Координаты на карте</label>
                <input class="form-control" type="text" name="coordinates" value="{{ $siteinfo->coordinates }}" id="coordinates">
            </div>
            <div class="form-group">
                <label for="work_time" class="col-form-label">График работы</label>
                <input class="form-control" type="text" name="work_time" value="{{ $siteinfo->work_time }}" id="work_time">
            </div>
            <div class="form-group">
                <label for="facebook" class="col-form-label">Facebook</label>
                <input class="form-control" type="text" name="facebook" value="{{ $siteinfo->facebook }}" id="facebook">
            </div>
            <div class="form-group">
                <label for="instagram" class="col-form-label">Instagram</label>
                <input class="form-control" type="text" name="instagram" value="{{ $siteinfo->instagram }}" id="instagram">
            </div>
            <div class="form-group">
                <label for="odnoklassniki" class="col-form-label">Одноклассники</label>
                <input class="form-control" type="text" name="odnoklassniki" value="{{ $siteinfo->odnoklassniki }}" id="odnoklassniki">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection