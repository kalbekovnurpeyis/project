@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Список записей</h4>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('siteinfo.edit') }}">
                        <button type="button" class="btn btn-primary btn-xs btn-block form-group">
                            Редактировать
                        </button>
                    </a>
                </div>
            </div>

            <div class="single-table">
                <div class="table-responsive">
                    @if($siteinfo)
                        <table class="table table-hover progress-table">
                            <tbody>
                                <tr>
                                    <th scope="row">Телефон</th>
                                    <td>{{ $siteinfo->phone_number_1?:'Не указано' }}</td>
                                    <td>{{ $siteinfo->whatsapp == 1 ? 'what\'s app' : '' }}</td>
                                </tr>
                                @if(!empty($siteinfo->phone_number_2))
                                <tr>
                                    <th scope="row">Дополнительный телефон</th>
                                    <td>{{ $siteinfo->phone_number_2 }}</td>
                                    <td>{{ $siteinfo->whatsapp == 2 ? 'what\'s app' : '' }}</td>
                                </tr>
                                @endif
                                @if(!empty($siteinfo->phone_number_3))
                                    <tr>
                                        <th scope="row">Дополнительный телефон</th>
                                        <td>{{ $siteinfo->phone_number_3 }}</td>
                                        <td>{{ $siteinfo->whatsapp == 3 ? 'what\'s app' : '' }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th scope="row">E-mail</th>
                                    <td>{{ $siteinfo->email?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">Адрес</th>
                                    <td>{{ $siteinfo->address?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">Координаты на карте</th>
                                    <td>{{ $siteinfo->coordinates?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">График работы</th>
                                    <td>{{ $siteinfo->work_time?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">Facebook</th>
                                    <td>{{ $siteinfo->facebook?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">Instagram</th>
                                    <td>{{ $siteinfo->instagram?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">Одноклассники</th>
                                    <td>{{ $siteinfo->odnoklassniki?:'Не указано' }}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted font-14 mb-4">Нет записей</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection