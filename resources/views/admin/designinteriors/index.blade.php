@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Список записей</h4>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('designinteriors.create') }}">
                        <button type="button" class="btn btn-primary btn-xs btn-block form-group">
                            Создать новую
                        </button>
                    </a>
                </div>
            </div>

            <div class="single-table">
                <div class="table-responsive">
                    @if($designinteriors->total())
                        <table class="table table-hover progress-table text-center">
                            <thead class="text-uppercase">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Состояние</th>
                                <th scope="col">Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($designinteriors as $designinterior)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $designinterior->title }}</td>
                                    <td>{{ $designinterior->is_active ? 'Активный' : 'Скрыто' }}</td>
                                    <td>
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3">
                                                <a href="{{ route('designinteriors.edit', $designinterior->id) }}" class="text-secondary">
                                                    <i class="ti-pencil-alt"></i>
                                                </a>
                                            </li>
                                            <li>
                                                {!! Form::open(['route' => ['designinteriors.destroy', $designinterior->id], 'method' => 'delete']) !!}
                                                <button type="submit" class="text-danger wrap-btn" onclick="return confirm('Вы уверены?')">
                                                    <i class="ti-trash"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav class="d-flex justify-content-end mt-3 pt-3 border-top">
                            {{ $designinteriors->links() }}
                        </nav>
                    @else
                        <p class="text-muted font-14 mb-4">Нет записей</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection