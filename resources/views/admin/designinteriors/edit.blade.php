@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            {!! Form::open(['route' => ['designinteriors.update', $designinterior->id], 'method' => 'put', 'files' => true]) !!}
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Редактирование записи</h4>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-xs btn-block form-group">Сохранить</button>
                </div>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">{{ $error }}</div>
                @endforeach
            @endif

            <div class="form-group">
                <label for="title" class="col-form-label">Название</label>
                <input class="form-control" type="text" name="title" value="{{ $designinterior->title }}" id="title">
            </div>
            <div class="form-group">
                <label for="slug" class="col-form-label">Slug</label>
                <input class="form-control" type="text" name="slug" value="{{ $designinterior->slug }}" id="slug">
                <small class="form-text text-muted">Оставьте поле пустым, что бы создать автоматически</small>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" checked class="custom-control-input" name="is_active" id="is-active" value="{{ $designinterior->is_active }}">
                    <label class="custom-control-label" for="is-active">Показать / Скрыть запись</label>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-form-label">Описание</label>
                <textarea name="description" id="description" class="ckeditor" cols="30" rows="10">
                    {{ $designinterior->description }}
                </textarea>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Загрузка миниатюры</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input custom-image-uploader" name="image" id="image" accept="image/*">
                                <label class="custom-file-label" for="image">Выберите изображение</label>
                            </div>
                        </div>
                        <div class="image-drop-zone" drop-from="image">
                            @if( $designinterior->image )
                                <div class="img-to-upload">
                                    <img src="/uploads/{{ $designinterior->image }}">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-form-label">Загрузка изображений</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input custom-image-uploader" name="gallery[]" id="gallery" accept="image/*" multiple>
                                <label class="custom-file-label" for="gallery">Выберите изображении</label>
                            </div>
                        </div>
                        <div class="image-drop-zone" drop-from="gallery">
                            @if( $images )
                                @foreach($images as $key => $image)
                                    <div class="img-to-upload" imgId="{{ $key }}">
                                        <img src="/uploads/{{ $image }}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection