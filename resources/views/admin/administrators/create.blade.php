@extends('admin.layouts.layout')

@section('content')
    <div class="card mt-5">
        <div class="card-body">
            {!! Form::open(['route' => 'administrators.store', 'method' => 'post']) !!}
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h4 class="header-title">Создание записи</h4>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-xs btn-block form-group">Сохранить</button>
                </div>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">{{ $error }}</div>
                @endforeach
            @endif

            <div class="form-group">
                <label for="name" class="col-form-label">Имя</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" id="name">
            </div>
            <div class="form-group">
                <label for="email" class="col-form-label">Email</label>
                <input class="form-control" type="text" name="email" value="{{ old('email') }}" id="email">
            </div>
            <div class="form-group">
                <label for="password" class="col-form-label">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <div class="form-group">
                <label for="password-confirm" class="col-form-label">Password</label>
                <input class="form-control" type="password" name="password_confirmation" id="password-confirm">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection