<footer>
    <div class="footer-top horizontal-padding">
        <div class="footer-nav">
            <div class="row">

                <div class="col-lg-2 col-md-4 col-6">
                    <div class="nav-section">
                        <h5 class="nav-section-title"><a href="#">Стили кухонь</a></h5>
                        <ul>
                            <li><a href="#">Диван</a></li>
                            <li><a href="#">Тумба</a></li>
                            <li><a href="#">Кресло</a></li>
                            <li><a href="#">Шкаф</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <div class="nav-section">
                        <h5 class="nav-section-title"><a href="#">Стили кухонь</a></h5>
                        <ul>
                            <li><a href="#">Диван</a></li>
                            <li><a href="#">Тумба</a></li>
                            <li><a href="#">Кресло</a></li>
                            <li><a href="#">Шкаф</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <div class="nav-section">
                        <h5 class="nav-section-title"><a href="#">Стили кухонь</a></h5>
                        <ul>
                            <li><a href="#">Диван</a></li>
                            <li><a href="#">Тумба</a></li>
                            <li><a href="#">Кресло</a></li>
                            <li><a href="#">Шкаф</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <div class="nav-section">
                        <h5 class="nav-section-title"><a href="#">Стили кухонь</a></h5>
                        <ul>
                            <li><a href="#">Диван</a></li>
                            <li><a href="#">Тумба</a></li>
                            <li><a href="#">Кресло</a></li>
                            <li><a href="#">Шкаф</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <div class="nav-section">
                        <h5 class="nav-section-title"><a href="#">Стили кухонь</a></h5>
                        <ul>
                            <li><a href="#">Диван</a></li>
                            <li><a href="#">Тумба</a></li>
                            <li><a href="#">Кресло</a></li>
                            <li><a href="#">Шкаф</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <div class="nav-section">
                        <h5 class="nav-section-title"><a href="#">Стили кухонь</a></h5>
                        <ul>
                            <li><a href="#">Диван</a></li>
                            <li><a href="#">Тумба</a></li>
                            <li><a href="#">Кресло</a></li>
                            <li><a href="#">Шкаф</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom horizontal-padding">
        <div class="copyright-txt"><span>Copyright © 2018. Все права защищены.</span></div>
        <div class="footer-page-nav">
            <ul>
                @if(!empty($pages))
                    @foreach($pages as $page)
                        <li>
                            <a href="{{ route($page->slug) }}">{{ $page->title }}</a>
                        </li>
                    @endforeach
                @endif
                <li><a href="{{ route('contacts') }}">Контакты</a></li>
            </ul>
        </div>
    </div>
</footer>