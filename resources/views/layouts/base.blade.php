<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <title>
        @if(trim($__env->yieldContent('title')))
            @yield('title') -
        @endif
        Dastan Интерьер
    </title>

    <link rel="icon" type="image/png" href="{{ asset('img/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon-96x96.png') }}" sizes="96x96">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
<div id="wrapper">
    @include('layouts.header')

    <div id="main-content">
    @yield('content')
    </div>

    @include('layouts.footer')
</div>
<script src="{{ asset('js/common.js') }}"></script>
@yield('scripts')
</body>

</html>