<header>
    <div id="header-inner">
        <div id="logo"><a href="{{ route('home') }}"><img src="{{ asset('img/logo.svg') }}" alt="logo.svg"></a></div>
        <div id="header-content">
            <div class="slogan"><span>Творческая </span><span>Мастерская </span><span>Дизайна и Мебели</span></div>
            <div id="header-content-right">
                <div id="page-nav">
                    <ul>
                        @if(!empty($pages))
                            @foreach($pages as $page)
                            <li>
                                <a href="{{ route($page->slug) }}">{{ $page->title }}</a>
                            </li>
                            @endforeach
                        @endif
                        <li><a href="{{ route('contacts') }}">Контакты</a></li>
                    </ul>
                </div>
                <div id="social-links">
                    @if(!empty($siteInfo->instagram))
                    <a href="https://www.instagram.com/{{ $siteInfo->instagram }}" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.00096 512.00096" style="width: 100%; height: 100%;">
                            <use xlink:href="{{ asset('img/005-instagram.svg#instagram') }}"></use>
                        </svg>
                    </a>
                    @endif
                    @if(!empty($siteInfo->odnoklassniki))
                    <a href="https://ok.ru/profile/{{ $siteInfo->odnoklassniki }}" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width: 100%; height: 100%;">
                            <use xlink:href="{{ asset('img/004-odnoklassniki.svg#odnoklassniki') }}"></use>
                        </svg>
                    </a>
                    @endif
                    @if(!empty($siteInfo->whatsapp))
                    <a href="https://wa.me/{{ preg_replace('/[^0-9.]+/', '', $siteInfo['phone_number_' . $siteInfo->whatsapp]) }}" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width: 100%; height: 100%;">
                            <use xlink:href="{{ asset('img/003-whatsapp.svg#whatsapp') }}"></use>
                        </svg>
                    </a>
                    @endif
                </div>
                @if(!empty($siteInfo->phone_number_1))
                    <div id="phone"><a href="tel:{{ $siteInfo->phone_number_1 }}">{{ $siteInfo->phone_number_1 }}</a></div>
                @elseif(!empty($siteInfo->phone_number_2))
                    <div id="phone"><a href="tel:{{ $siteInfo->phone_number_2 }}">{{ $siteInfo->phone_number_2 }}</a></div>
                @elseif(!empty($siteInfo->phone_number_3))
                    <div id="phone"><a href="tel:{{ $siteInfo->phone_number_3 }}">{{ $siteInfo->phone_number_3 }}</a></div>
                @endif
                <div id="catalog-nav">
                    <div class="catalog-nav-content">
                        <div class="home-link">
                            <a href="{{ route('home') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 611.997 611.998" style="width: 100%; height: 100%;">
                                    <use xlink:href="{{ asset('img/001-house-outline.svg#house') }}"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="catalog-list">
                            <div class="mobile-nav-close">
                                <div class="close-tgl">
                                    <i class="icon ion-md-arrow-forward"></i>
                                </div>
                            </div>
                            <ul class="nav-wrap">
                            @if(!empty($parentCategories))
                                @foreach($parentCategories as $category)
                                <li>
                                    <a class="category-group" href="{{ route('categories', $category->slug) }}">{{ $category->title }}</a>
                                    <div class="categories-list">
                                        <div class="list-wrap">
                                            @foreach($styleCategories as $style)
                                                <div class="category-style-section">
                                                    <p>{{ $style->title }}</p>
                                                    <ul>
                                                        @foreach($subCats[$category->slug][$style->slug] as $subCat)
                                                            <li><a href="{{ route('categories', $subCat->slug) }}">{{ $subCat->title }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            @endif
                            </ul>
                            <div class="mobile-page-nav">
                                <ul>
                                    @if(!empty($pages))
                                        @foreach($pages as $page)
                                            <li>
                                                <a href="{{ route($page->slug) }}">{{ $page->title }}</a>
                                            </li>
                                        @endforeach
                                    @endif
                                    <li><a href="#">Контакты</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="fillSpace"></div>
                        <div class="mobile-menu-tgl">
                            <i class="icon ion-ios-menu"></i>
                        </div>
                        <a class="design-interior" href="{{ route('design-interior') }}">
                            <span>Дизайн Интерьер</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>