/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/common.js":
/*!********************************!*\
  !*** ./resources/js/common.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (t) {
  var e = {};

  function n(o) {
    if (e[o]) return e[o].exports;
    var i = e[o] = {
      i: o,
      l: !1,
      exports: {}
    };
    return t[o].call(i.exports, i, i.exports, n), i.l = !0, i.exports;
  }

  n.m = t, n.c = e, n.d = function (t, e, o) {
    n.o(t, e) || Object.defineProperty(t, e, {
      enumerable: !0,
      get: o
    });
  }, n.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
      value: "Module"
    }), Object.defineProperty(t, "__esModule", {
      value: !0
    });
  }, n.t = function (t, e) {
    if (1 & e && (t = n(t)), 8 & e) return t;
    if (4 & e && "object" == _typeof(t) && t && t.__esModule) return t;
    var o = Object.create(null);
    if (n.r(o), Object.defineProperty(o, "default", {
      enumerable: !0,
      value: t
    }), 2 & e && "string" != typeof t) for (var i in t) {
      n.d(o, i, function (e) {
        return t[e];
      }.bind(null, i));
    }
    return o;
  }, n.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };
    return n.d(e, "a", e), e;
  }, n.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, n.p = "", n(n.s = 2);
}([function (t, e, n) {
  var o;
  /*!
   * jQuery JavaScript Library v3.3.1
   * https://jquery.com/
   *
   * Includes Sizzle.js
   * https://sizzlejs.com/
   *
   * Copyright JS Foundation and other contributors
   * Released under the MIT license
   * https://jquery.org/license
   *
   * Date: 2018-01-20T17:24Z
   */

  /*!
   * jQuery JavaScript Library v3.3.1
   * https://jquery.com/
   *
   * Includes Sizzle.js
   * https://sizzlejs.com/
   *
   * Copyright JS Foundation and other contributors
   * Released under the MIT license
   * https://jquery.org/license
   *
   * Date: 2018-01-20T17:24Z
   */

  !function (e, n) {
    "use strict";

    "object" == _typeof(t.exports) ? t.exports = e.document ? n(e, !0) : function (t) {
      if (!t.document) throw new Error("jQuery requires a window with a document");
      return n(t);
    } : n(e);
  }("undefined" != typeof window ? window : this, function (n, i) {
    "use strict";

    var r = [],
        s = n.document,
        a = Object.getPrototypeOf,
        c = r.slice,
        l = r.concat,
        u = r.push,
        f = r.indexOf,
        d = {},
        p = d.toString,
        h = d.hasOwnProperty,
        g = h.toString,
        m = g.call(Object),
        v = {},
        y = function y(t) {
      return "function" == typeof t && "number" != typeof t.nodeType;
    },
        b = function b(t) {
      return null != t && t === t.window;
    },
        x = {
      type: !0,
      src: !0,
      noModule: !0
    };

    function w(t, e, n) {
      var o,
          i = (e = e || s).createElement("script");
      if (i.text = t, n) for (o in x) {
        n[o] && (i[o] = n[o]);
      }
      e.head.appendChild(i).parentNode.removeChild(i);
    }

    function T(t) {
      return null == t ? t + "" : "object" == _typeof(t) || "function" == typeof t ? d[p.call(t)] || "object" : _typeof(t);
    }

    var C = function C(t, e) {
      return new C.fn.init(t, e);
    },
        S = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

    function $(t) {
      var e = !!t && "length" in t && t.length,
          n = T(t);
      return !y(t) && !b(t) && ("array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t);
    }

    C.fn = C.prototype = {
      jquery: "3.3.1",
      constructor: C,
      length: 0,
      toArray: function toArray() {
        return c.call(this);
      },
      get: function get(t) {
        return null == t ? c.call(this) : t < 0 ? this[t + this.length] : this[t];
      },
      pushStack: function pushStack(t) {
        var e = C.merge(this.constructor(), t);
        return e.prevObject = this, e;
      },
      each: function each(t) {
        return C.each(this, t);
      },
      map: function map(t) {
        return this.pushStack(C.map(this, function (e, n) {
          return t.call(e, n, e);
        }));
      },
      slice: function slice() {
        return this.pushStack(c.apply(this, arguments));
      },
      first: function first() {
        return this.eq(0);
      },
      last: function last() {
        return this.eq(-1);
      },
      eq: function eq(t) {
        var e = this.length,
            n = +t + (t < 0 ? e : 0);
        return this.pushStack(n >= 0 && n < e ? [this[n]] : []);
      },
      end: function end() {
        return this.prevObject || this.constructor();
      },
      push: u,
      sort: r.sort,
      splice: r.splice
    }, C.extend = C.fn.extend = function () {
      var t,
          e,
          n,
          o,
          i,
          r,
          s = arguments[0] || {},
          a = 1,
          c = arguments.length,
          l = !1;

      for ("boolean" == typeof s && (l = s, s = arguments[a] || {}, a++), "object" == _typeof(s) || y(s) || (s = {}), a === c && (s = this, a--); a < c; a++) {
        if (null != (t = arguments[a])) for (e in t) {
          n = s[e], s !== (o = t[e]) && (l && o && (C.isPlainObject(o) || (i = Array.isArray(o))) ? (i ? (i = !1, r = n && Array.isArray(n) ? n : []) : r = n && C.isPlainObject(n) ? n : {}, s[e] = C.extend(l, r, o)) : void 0 !== o && (s[e] = o));
        }
      }

      return s;
    }, C.extend({
      expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
      isReady: !0,
      error: function error(t) {
        throw new Error(t);
      },
      noop: function noop() {},
      isPlainObject: function isPlainObject(t) {
        var e, n;
        return !(!t || "[object Object]" !== p.call(t)) && (!(e = a(t)) || "function" == typeof (n = h.call(e, "constructor") && e.constructor) && g.call(n) === m);
      },
      isEmptyObject: function isEmptyObject(t) {
        var e;

        for (e in t) {
          return !1;
        }

        return !0;
      },
      globalEval: function globalEval(t) {
        w(t);
      },
      each: function each(t, e) {
        var n,
            o = 0;
        if ($(t)) for (n = t.length; o < n && !1 !== e.call(t[o], o, t[o]); o++) {
          ;
        } else for (o in t) {
          if (!1 === e.call(t[o], o, t[o])) break;
        }
        return t;
      },
      trim: function trim(t) {
        return null == t ? "" : (t + "").replace(S, "");
      },
      makeArray: function makeArray(t, e) {
        var n = e || [];
        return null != t && ($(Object(t)) ? C.merge(n, "string" == typeof t ? [t] : t) : u.call(n, t)), n;
      },
      inArray: function inArray(t, e, n) {
        return null == e ? -1 : f.call(e, t, n);
      },
      merge: function merge(t, e) {
        for (var n = +e.length, o = 0, i = t.length; o < n; o++) {
          t[i++] = e[o];
        }

        return t.length = i, t;
      },
      grep: function grep(t, e, n) {
        for (var o = [], i = 0, r = t.length, s = !n; i < r; i++) {
          !e(t[i], i) !== s && o.push(t[i]);
        }

        return o;
      },
      map: function map(t, e, n) {
        var o,
            i,
            r = 0,
            s = [];
        if ($(t)) for (o = t.length; r < o; r++) {
          null != (i = e(t[r], r, n)) && s.push(i);
        } else for (r in t) {
          null != (i = e(t[r], r, n)) && s.push(i);
        }
        return l.apply([], s);
      },
      guid: 1,
      support: v
    }), "function" == typeof Symbol && (C.fn[Symbol.iterator] = r[Symbol.iterator]), C.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
      d["[object " + e + "]"] = e.toLowerCase();
    });

    var E =
    /*!
     * Sizzle CSS Selector Engine v2.3.3
     * https://sizzlejs.com/
     *
     * Copyright jQuery Foundation and other contributors
     * Released under the MIT license
     * http://jquery.org/license
     *
     * Date: 2016-08-08
     */
    function (t) {
      var e,
          n,
          o,
          i,
          r,
          s,
          a,
          c,
          l,
          u,
          f,
          d,
          p,
          h,
          g,
          m,
          v,
          y,
          b,
          x = "sizzle" + 1 * new Date(),
          w = t.document,
          T = 0,
          C = 0,
          S = st(),
          $ = st(),
          E = st(),
          P = function P(t, e) {
        return t === e && (f = !0), 0;
      },
          k = {}.hasOwnProperty,
          A = [],
          L = A.pop,
          D = A.push,
          j = A.push,
          M = A.slice,
          N = function N(t, e) {
        for (var n = 0, o = t.length; n < o; n++) {
          if (t[n] === e) return n;
        }

        return -1;
      },
          H = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
          q = "[\\x20\\t\\r\\n\\f]",
          I = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
          F = "\\[" + q + "*(" + I + ")(?:" + q + "*([*^$|!~]?=)" + q + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + q + "*\\]",
          O = ":(" + I + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + F + ")*)|.*)\\)|)",
          R = new RegExp(q + "+", "g"),
          z = new RegExp("^" + q + "+|((?:^|[^\\\\])(?:\\\\.)*)" + q + "+$", "g"),
          B = new RegExp("^" + q + "*," + q + "*"),
          _ = new RegExp("^" + q + "*([>+~]|" + q + ")" + q + "*"),
          W = new RegExp("=" + q + "*([^\\]'\"]*?)" + q + "*\\]", "g"),
          X = new RegExp(O),
          Y = new RegExp("^" + I + "$"),
          V = {
        ID: new RegExp("^#(" + I + ")"),
        CLASS: new RegExp("^\\.(" + I + ")"),
        TAG: new RegExp("^(" + I + "|[*])"),
        ATTR: new RegExp("^" + F),
        PSEUDO: new RegExp("^" + O),
        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + q + "*(even|odd|(([+-]|)(\\d*)n|)" + q + "*(?:([+-]|)" + q + "*(\\d+)|))" + q + "*\\)|)", "i"),
        bool: new RegExp("^(?:" + H + ")$", "i"),
        needsContext: new RegExp("^" + q + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + q + "*((?:-\\d)?\\d*)" + q + "*\\)|)(?=[^-]|$)", "i")
      },
          U = /^(?:input|select|textarea|button)$/i,
          Z = /^h\d$/i,
          G = /^[^{]+\{\s*\[native \w/,
          K = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
          Q = /[+~]/,
          J = new RegExp("\\\\([\\da-f]{1,6}" + q + "?|(" + q + ")|.)", "ig"),
          tt = function tt(t, e, n) {
        var o = "0x" + e - 65536;
        return o != o || n ? e : o < 0 ? String.fromCharCode(o + 65536) : String.fromCharCode(o >> 10 | 55296, 1023 & o | 56320);
      },
          et = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
          nt = function nt(t, e) {
        return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t;
      },
          ot = function ot() {
        d();
      },
          it = yt(function (t) {
        return !0 === t.disabled && ("form" in t || "label" in t);
      }, {
        dir: "parentNode",
        next: "legend"
      });

      try {
        j.apply(A = M.call(w.childNodes), w.childNodes), A[w.childNodes.length].nodeType;
      } catch (t) {
        j = {
          apply: A.length ? function (t, e) {
            D.apply(t, M.call(e));
          } : function (t, e) {
            for (var n = t.length, o = 0; t[n++] = e[o++];) {
              ;
            }

            t.length = n - 1;
          }
        };
      }

      function rt(t, e, o, i) {
        var r,
            a,
            l,
            u,
            f,
            h,
            v,
            y = e && e.ownerDocument,
            T = e ? e.nodeType : 9;
        if (o = o || [], "string" != typeof t || !t || 1 !== T && 9 !== T && 11 !== T) return o;

        if (!i && ((e ? e.ownerDocument || e : w) !== p && d(e), e = e || p, g)) {
          if (11 !== T && (f = K.exec(t))) if (r = f[1]) {
            if (9 === T) {
              if (!(l = e.getElementById(r))) return o;
              if (l.id === r) return o.push(l), o;
            } else if (y && (l = y.getElementById(r)) && b(e, l) && l.id === r) return o.push(l), o;
          } else {
            if (f[2]) return j.apply(o, e.getElementsByTagName(t)), o;
            if ((r = f[3]) && n.getElementsByClassName && e.getElementsByClassName) return j.apply(o, e.getElementsByClassName(r)), o;
          }

          if (n.qsa && !E[t + " "] && (!m || !m.test(t))) {
            if (1 !== T) y = e, v = t;else if ("object" !== e.nodeName.toLowerCase()) {
              for ((u = e.getAttribute("id")) ? u = u.replace(et, nt) : e.setAttribute("id", u = x), a = (h = s(t)).length; a--;) {
                h[a] = "#" + u + " " + vt(h[a]);
              }

              v = h.join(","), y = Q.test(t) && gt(e.parentNode) || e;
            }
            if (v) try {
              return j.apply(o, y.querySelectorAll(v)), o;
            } catch (t) {} finally {
              u === x && e.removeAttribute("id");
            }
          }
        }

        return c(t.replace(z, "$1"), e, o, i);
      }

      function st() {
        var t = [];
        return function e(n, i) {
          return t.push(n + " ") > o.cacheLength && delete e[t.shift()], e[n + " "] = i;
        };
      }

      function at(t) {
        return t[x] = !0, t;
      }

      function ct(t) {
        var e = p.createElement("fieldset");

        try {
          return !!t(e);
        } catch (t) {
          return !1;
        } finally {
          e.parentNode && e.parentNode.removeChild(e), e = null;
        }
      }

      function lt(t, e) {
        for (var n = t.split("|"), i = n.length; i--;) {
          o.attrHandle[n[i]] = e;
        }
      }

      function ut(t, e) {
        var n = e && t,
            o = n && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
        if (o) return o;
        if (n) for (; n = n.nextSibling;) {
          if (n === e) return -1;
        }
        return t ? 1 : -1;
      }

      function ft(t) {
        return function (e) {
          return "input" === e.nodeName.toLowerCase() && e.type === t;
        };
      }

      function dt(t) {
        return function (e) {
          var n = e.nodeName.toLowerCase();
          return ("input" === n || "button" === n) && e.type === t;
        };
      }

      function pt(t) {
        return function (e) {
          return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && it(e) === t : e.disabled === t : "label" in e && e.disabled === t;
        };
      }

      function ht(t) {
        return at(function (e) {
          return e = +e, at(function (n, o) {
            for (var i, r = t([], n.length, e), s = r.length; s--;) {
              n[i = r[s]] && (n[i] = !(o[i] = n[i]));
            }
          });
        });
      }

      function gt(t) {
        return t && void 0 !== t.getElementsByTagName && t;
      }

      for (e in n = rt.support = {}, r = rt.isXML = function (t) {
        var e = t && (t.ownerDocument || t).documentElement;
        return !!e && "HTML" !== e.nodeName;
      }, d = rt.setDocument = function (t) {
        var e,
            i,
            s = t ? t.ownerDocument || t : w;
        return s !== p && 9 === s.nodeType && s.documentElement ? (h = (p = s).documentElement, g = !r(p), w !== p && (i = p.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", ot, !1) : i.attachEvent && i.attachEvent("onunload", ot)), n.attributes = ct(function (t) {
          return t.className = "i", !t.getAttribute("className");
        }), n.getElementsByTagName = ct(function (t) {
          return t.appendChild(p.createComment("")), !t.getElementsByTagName("*").length;
        }), n.getElementsByClassName = G.test(p.getElementsByClassName), n.getById = ct(function (t) {
          return h.appendChild(t).id = x, !p.getElementsByName || !p.getElementsByName(x).length;
        }), n.getById ? (o.filter.ID = function (t) {
          var e = t.replace(J, tt);
          return function (t) {
            return t.getAttribute("id") === e;
          };
        }, o.find.ID = function (t, e) {
          if (void 0 !== e.getElementById && g) {
            var n = e.getElementById(t);
            return n ? [n] : [];
          }
        }) : (o.filter.ID = function (t) {
          var e = t.replace(J, tt);
          return function (t) {
            var n = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
            return n && n.value === e;
          };
        }, o.find.ID = function (t, e) {
          if (void 0 !== e.getElementById && g) {
            var n,
                o,
                i,
                r = e.getElementById(t);

            if (r) {
              if ((n = r.getAttributeNode("id")) && n.value === t) return [r];

              for (i = e.getElementsByName(t), o = 0; r = i[o++];) {
                if ((n = r.getAttributeNode("id")) && n.value === t) return [r];
              }
            }

            return [];
          }
        }), o.find.TAG = n.getElementsByTagName ? function (t, e) {
          return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : n.qsa ? e.querySelectorAll(t) : void 0;
        } : function (t, e) {
          var n,
              o = [],
              i = 0,
              r = e.getElementsByTagName(t);

          if ("*" === t) {
            for (; n = r[i++];) {
              1 === n.nodeType && o.push(n);
            }

            return o;
          }

          return r;
        }, o.find.CLASS = n.getElementsByClassName && function (t, e) {
          if (void 0 !== e.getElementsByClassName && g) return e.getElementsByClassName(t);
        }, v = [], m = [], (n.qsa = G.test(p.querySelectorAll)) && (ct(function (t) {
          h.appendChild(t).innerHTML = "<a id='" + x + "'></a><select id='" + x + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && m.push("[*^$]=" + q + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || m.push("\\[" + q + "*(?:value|" + H + ")"), t.querySelectorAll("[id~=" + x + "-]").length || m.push("~="), t.querySelectorAll(":checked").length || m.push(":checked"), t.querySelectorAll("a#" + x + "+*").length || m.push(".#.+[+~]");
        }), ct(function (t) {
          t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
          var e = p.createElement("input");
          e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && m.push("name" + q + "*[*^$|!~]?="), 2 !== t.querySelectorAll(":enabled").length && m.push(":enabled", ":disabled"), h.appendChild(t).disabled = !0, 2 !== t.querySelectorAll(":disabled").length && m.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), m.push(",.*:");
        })), (n.matchesSelector = G.test(y = h.matches || h.webkitMatchesSelector || h.mozMatchesSelector || h.oMatchesSelector || h.msMatchesSelector)) && ct(function (t) {
          n.disconnectedMatch = y.call(t, "*"), y.call(t, "[s!='']:x"), v.push("!=", O);
        }), m = m.length && new RegExp(m.join("|")), v = v.length && new RegExp(v.join("|")), e = G.test(h.compareDocumentPosition), b = e || G.test(h.contains) ? function (t, e) {
          var n = 9 === t.nodeType ? t.documentElement : t,
              o = e && e.parentNode;
          return t === o || !(!o || 1 !== o.nodeType || !(n.contains ? n.contains(o) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(o)));
        } : function (t, e) {
          if (e) for (; e = e.parentNode;) {
            if (e === t) return !0;
          }
          return !1;
        }, P = e ? function (t, e) {
          if (t === e) return f = !0, 0;
          var o = !t.compareDocumentPosition - !e.compareDocumentPosition;
          return o || (1 & (o = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !n.sortDetached && e.compareDocumentPosition(t) === o ? t === p || t.ownerDocument === w && b(w, t) ? -1 : e === p || e.ownerDocument === w && b(w, e) ? 1 : u ? N(u, t) - N(u, e) : 0 : 4 & o ? -1 : 1);
        } : function (t, e) {
          if (t === e) return f = !0, 0;
          var n,
              o = 0,
              i = t.parentNode,
              r = e.parentNode,
              s = [t],
              a = [e];
          if (!i || !r) return t === p ? -1 : e === p ? 1 : i ? -1 : r ? 1 : u ? N(u, t) - N(u, e) : 0;
          if (i === r) return ut(t, e);

          for (n = t; n = n.parentNode;) {
            s.unshift(n);
          }

          for (n = e; n = n.parentNode;) {
            a.unshift(n);
          }

          for (; s[o] === a[o];) {
            o++;
          }

          return o ? ut(s[o], a[o]) : s[o] === w ? -1 : a[o] === w ? 1 : 0;
        }, p) : p;
      }, rt.matches = function (t, e) {
        return rt(t, null, null, e);
      }, rt.matchesSelector = function (t, e) {
        if ((t.ownerDocument || t) !== p && d(t), e = e.replace(W, "='$1']"), n.matchesSelector && g && !E[e + " "] && (!v || !v.test(e)) && (!m || !m.test(e))) try {
          var o = y.call(t, e);
          if (o || n.disconnectedMatch || t.document && 11 !== t.document.nodeType) return o;
        } catch (t) {}
        return rt(e, p, null, [t]).length > 0;
      }, rt.contains = function (t, e) {
        return (t.ownerDocument || t) !== p && d(t), b(t, e);
      }, rt.attr = function (t, e) {
        (t.ownerDocument || t) !== p && d(t);
        var i = o.attrHandle[e.toLowerCase()],
            r = i && k.call(o.attrHandle, e.toLowerCase()) ? i(t, e, !g) : void 0;
        return void 0 !== r ? r : n.attributes || !g ? t.getAttribute(e) : (r = t.getAttributeNode(e)) && r.specified ? r.value : null;
      }, rt.escape = function (t) {
        return (t + "").replace(et, nt);
      }, rt.error = function (t) {
        throw new Error("Syntax error, unrecognized expression: " + t);
      }, rt.uniqueSort = function (t) {
        var e,
            o = [],
            i = 0,
            r = 0;

        if (f = !n.detectDuplicates, u = !n.sortStable && t.slice(0), t.sort(P), f) {
          for (; e = t[r++];) {
            e === t[r] && (i = o.push(r));
          }

          for (; i--;) {
            t.splice(o[i], 1);
          }
        }

        return u = null, t;
      }, i = rt.getText = function (t) {
        var e,
            n = "",
            o = 0,
            r = t.nodeType;

        if (r) {
          if (1 === r || 9 === r || 11 === r) {
            if ("string" == typeof t.textContent) return t.textContent;

            for (t = t.firstChild; t; t = t.nextSibling) {
              n += i(t);
            }
          } else if (3 === r || 4 === r) return t.nodeValue;
        } else for (; e = t[o++];) {
          n += i(e);
        }

        return n;
      }, (o = rt.selectors = {
        cacheLength: 50,
        createPseudo: at,
        match: V,
        attrHandle: {},
        find: {},
        relative: {
          ">": {
            dir: "parentNode",
            first: !0
          },
          " ": {
            dir: "parentNode"
          },
          "+": {
            dir: "previousSibling",
            first: !0
          },
          "~": {
            dir: "previousSibling"
          }
        },
        preFilter: {
          ATTR: function ATTR(t) {
            return t[1] = t[1].replace(J, tt), t[3] = (t[3] || t[4] || t[5] || "").replace(J, tt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4);
          },
          CHILD: function CHILD(t) {
            return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || rt.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && rt.error(t[0]), t;
          },
          PSEUDO: function PSEUDO(t) {
            var e,
                n = !t[6] && t[2];
            return V.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && X.test(n) && (e = s(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3));
          }
        },
        filter: {
          TAG: function TAG(t) {
            var e = t.replace(J, tt).toLowerCase();
            return "*" === t ? function () {
              return !0;
            } : function (t) {
              return t.nodeName && t.nodeName.toLowerCase() === e;
            };
          },
          CLASS: function CLASS(t) {
            var e = S[t + " "];
            return e || (e = new RegExp("(^|" + q + ")" + t + "(" + q + "|$)")) && S(t, function (t) {
              return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "");
            });
          },
          ATTR: function ATTR(t, e, n) {
            return function (o) {
              var i = rt.attr(o, t);
              return null == i ? "!=" === e : !e || (i += "", "=" === e ? i === n : "!=" === e ? i !== n : "^=" === e ? n && 0 === i.indexOf(n) : "*=" === e ? n && i.indexOf(n) > -1 : "$=" === e ? n && i.slice(-n.length) === n : "~=" === e ? (" " + i.replace(R, " ") + " ").indexOf(n) > -1 : "|=" === e && (i === n || i.slice(0, n.length + 1) === n + "-"));
            };
          },
          CHILD: function CHILD(t, e, n, o, i) {
            var r = "nth" !== t.slice(0, 3),
                s = "last" !== t.slice(-4),
                a = "of-type" === e;
            return 1 === o && 0 === i ? function (t) {
              return !!t.parentNode;
            } : function (e, n, c) {
              var l,
                  u,
                  f,
                  d,
                  p,
                  h,
                  g = r !== s ? "nextSibling" : "previousSibling",
                  m = e.parentNode,
                  v = a && e.nodeName.toLowerCase(),
                  y = !c && !a,
                  b = !1;

              if (m) {
                if (r) {
                  for (; g;) {
                    for (d = e; d = d[g];) {
                      if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                    }

                    h = g = "only" === t && !h && "nextSibling";
                  }

                  return !0;
                }

                if (h = [s ? m.firstChild : m.lastChild], s && y) {
                  for (b = (p = (l = (u = (f = (d = m)[x] || (d[x] = {}))[d.uniqueID] || (f[d.uniqueID] = {}))[t] || [])[0] === T && l[1]) && l[2], d = p && m.childNodes[p]; d = ++p && d && d[g] || (b = p = 0) || h.pop();) {
                    if (1 === d.nodeType && ++b && d === e) {
                      u[t] = [T, p, b];
                      break;
                    }
                  }
                } else if (y && (b = p = (l = (u = (f = (d = e)[x] || (d[x] = {}))[d.uniqueID] || (f[d.uniqueID] = {}))[t] || [])[0] === T && l[1]), !1 === b) for (; (d = ++p && d && d[g] || (b = p = 0) || h.pop()) && ((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++b || (y && ((u = (f = d[x] || (d[x] = {}))[d.uniqueID] || (f[d.uniqueID] = {}))[t] = [T, b]), d !== e));) {
                  ;
                }

                return (b -= i) === o || b % o == 0 && b / o >= 0;
              }
            };
          },
          PSEUDO: function PSEUDO(t, e) {
            var n,
                i = o.pseudos[t] || o.setFilters[t.toLowerCase()] || rt.error("unsupported pseudo: " + t);
            return i[x] ? i(e) : i.length > 1 ? (n = [t, t, "", e], o.setFilters.hasOwnProperty(t.toLowerCase()) ? at(function (t, n) {
              for (var o, r = i(t, e), s = r.length; s--;) {
                t[o = N(t, r[s])] = !(n[o] = r[s]);
              }
            }) : function (t) {
              return i(t, 0, n);
            }) : i;
          }
        },
        pseudos: {
          not: at(function (t) {
            var e = [],
                n = [],
                o = a(t.replace(z, "$1"));
            return o[x] ? at(function (t, e, n, i) {
              for (var r, s = o(t, null, i, []), a = t.length; a--;) {
                (r = s[a]) && (t[a] = !(e[a] = r));
              }
            }) : function (t, i, r) {
              return e[0] = t, o(e, null, r, n), e[0] = null, !n.pop();
            };
          }),
          has: at(function (t) {
            return function (e) {
              return rt(t, e).length > 0;
            };
          }),
          contains: at(function (t) {
            return t = t.replace(J, tt), function (e) {
              return (e.textContent || e.innerText || i(e)).indexOf(t) > -1;
            };
          }),
          lang: at(function (t) {
            return Y.test(t || "") || rt.error("unsupported lang: " + t), t = t.replace(J, tt).toLowerCase(), function (e) {
              var n;

              do {
                if (n = g ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (n = n.toLowerCase()) === t || 0 === n.indexOf(t + "-");
              } while ((e = e.parentNode) && 1 === e.nodeType);

              return !1;
            };
          }),
          target: function target(e) {
            var n = t.location && t.location.hash;
            return n && n.slice(1) === e.id;
          },
          root: function root(t) {
            return t === h;
          },
          focus: function focus(t) {
            return t === p.activeElement && (!p.hasFocus || p.hasFocus()) && !!(t.type || t.href || ~t.tabIndex);
          },
          enabled: pt(!1),
          disabled: pt(!0),
          checked: function checked(t) {
            var e = t.nodeName.toLowerCase();
            return "input" === e && !!t.checked || "option" === e && !!t.selected;
          },
          selected: function selected(t) {
            return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected;
          },
          empty: function empty(t) {
            for (t = t.firstChild; t; t = t.nextSibling) {
              if (t.nodeType < 6) return !1;
            }

            return !0;
          },
          parent: function parent(t) {
            return !o.pseudos.empty(t);
          },
          header: function header(t) {
            return Z.test(t.nodeName);
          },
          input: function input(t) {
            return U.test(t.nodeName);
          },
          button: function button(t) {
            var e = t.nodeName.toLowerCase();
            return "input" === e && "button" === t.type || "button" === e;
          },
          text: function text(t) {
            var e;
            return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase());
          },
          first: ht(function () {
            return [0];
          }),
          last: ht(function (t, e) {
            return [e - 1];
          }),
          eq: ht(function (t, e, n) {
            return [n < 0 ? n + e : n];
          }),
          even: ht(function (t, e) {
            for (var n = 0; n < e; n += 2) {
              t.push(n);
            }

            return t;
          }),
          odd: ht(function (t, e) {
            for (var n = 1; n < e; n += 2) {
              t.push(n);
            }

            return t;
          }),
          lt: ht(function (t, e, n) {
            for (var o = n < 0 ? n + e : n; --o >= 0;) {
              t.push(o);
            }

            return t;
          }),
          gt: ht(function (t, e, n) {
            for (var o = n < 0 ? n + e : n; ++o < e;) {
              t.push(o);
            }

            return t;
          })
        }
      }).pseudos.nth = o.pseudos.eq, {
        radio: !0,
        checkbox: !0,
        file: !0,
        password: !0,
        image: !0
      }) {
        o.pseudos[e] = ft(e);
      }

      for (e in {
        submit: !0,
        reset: !0
      }) {
        o.pseudos[e] = dt(e);
      }

      function mt() {}

      function vt(t) {
        for (var e = 0, n = t.length, o = ""; e < n; e++) {
          o += t[e].value;
        }

        return o;
      }

      function yt(t, e, n) {
        var o = e.dir,
            i = e.next,
            r = i || o,
            s = n && "parentNode" === r,
            a = C++;
        return e.first ? function (e, n, i) {
          for (; e = e[o];) {
            if (1 === e.nodeType || s) return t(e, n, i);
          }

          return !1;
        } : function (e, n, c) {
          var l,
              u,
              f,
              d = [T, a];

          if (c) {
            for (; e = e[o];) {
              if ((1 === e.nodeType || s) && t(e, n, c)) return !0;
            }
          } else for (; e = e[o];) {
            if (1 === e.nodeType || s) if (u = (f = e[x] || (e[x] = {}))[e.uniqueID] || (f[e.uniqueID] = {}), i && i === e.nodeName.toLowerCase()) e = e[o] || e;else {
              if ((l = u[r]) && l[0] === T && l[1] === a) return d[2] = l[2];
              if (u[r] = d, d[2] = t(e, n, c)) return !0;
            }
          }

          return !1;
        };
      }

      function bt(t) {
        return t.length > 1 ? function (e, n, o) {
          for (var i = t.length; i--;) {
            if (!t[i](e, n, o)) return !1;
          }

          return !0;
        } : t[0];
      }

      function xt(t, e, n, o, i) {
        for (var r, s = [], a = 0, c = t.length, l = null != e; a < c; a++) {
          (r = t[a]) && (n && !n(r, o, i) || (s.push(r), l && e.push(a)));
        }

        return s;
      }

      function wt(t, e, n, o, i, r) {
        return o && !o[x] && (o = wt(o)), i && !i[x] && (i = wt(i, r)), at(function (r, s, a, c) {
          var l,
              u,
              f,
              d = [],
              p = [],
              h = s.length,
              g = r || function (t, e, n) {
            for (var o = 0, i = e.length; o < i; o++) {
              rt(t, e[o], n);
            }

            return n;
          }(e || "*", a.nodeType ? [a] : a, []),
              m = !t || !r && e ? g : xt(g, d, t, a, c),
              v = n ? i || (r ? t : h || o) ? [] : s : m;

          if (n && n(m, v, a, c), o) for (l = xt(v, p), o(l, [], a, c), u = l.length; u--;) {
            (f = l[u]) && (v[p[u]] = !(m[p[u]] = f));
          }

          if (r) {
            if (i || t) {
              if (i) {
                for (l = [], u = v.length; u--;) {
                  (f = v[u]) && l.push(m[u] = f);
                }

                i(null, v = [], l, c);
              }

              for (u = v.length; u--;) {
                (f = v[u]) && (l = i ? N(r, f) : d[u]) > -1 && (r[l] = !(s[l] = f));
              }
            }
          } else v = xt(v === s ? v.splice(h, v.length) : v), i ? i(null, s, v, c) : j.apply(s, v);
        });
      }

      function Tt(t) {
        for (var e, n, i, r = t.length, s = o.relative[t[0].type], a = s || o.relative[" "], c = s ? 1 : 0, u = yt(function (t) {
          return t === e;
        }, a, !0), f = yt(function (t) {
          return N(e, t) > -1;
        }, a, !0), d = [function (t, n, o) {
          var i = !s && (o || n !== l) || ((e = n).nodeType ? u(t, n, o) : f(t, n, o));
          return e = null, i;
        }]; c < r; c++) {
          if (n = o.relative[t[c].type]) d = [yt(bt(d), n)];else {
            if ((n = o.filter[t[c].type].apply(null, t[c].matches))[x]) {
              for (i = ++c; i < r && !o.relative[t[i].type]; i++) {
                ;
              }

              return wt(c > 1 && bt(d), c > 1 && vt(t.slice(0, c - 1).concat({
                value: " " === t[c - 2].type ? "*" : ""
              })).replace(z, "$1"), n, c < i && Tt(t.slice(c, i)), i < r && Tt(t = t.slice(i)), i < r && vt(t));
            }

            d.push(n);
          }
        }

        return bt(d);
      }

      return mt.prototype = o.filters = o.pseudos, o.setFilters = new mt(), s = rt.tokenize = function (t, e) {
        var n,
            i,
            r,
            s,
            a,
            c,
            l,
            u = $[t + " "];
        if (u) return e ? 0 : u.slice(0);

        for (a = t, c = [], l = o.preFilter; a;) {
          for (s in n && !(i = B.exec(a)) || (i && (a = a.slice(i[0].length) || a), c.push(r = [])), n = !1, (i = _.exec(a)) && (n = i.shift(), r.push({
            value: n,
            type: i[0].replace(z, " ")
          }), a = a.slice(n.length)), o.filter) {
            !(i = V[s].exec(a)) || l[s] && !(i = l[s](i)) || (n = i.shift(), r.push({
              value: n,
              type: s,
              matches: i
            }), a = a.slice(n.length));
          }

          if (!n) break;
        }

        return e ? a.length : a ? rt.error(t) : $(t, c).slice(0);
      }, a = rt.compile = function (t, e) {
        var n,
            i = [],
            r = [],
            a = E[t + " "];

        if (!a) {
          for (e || (e = s(t)), n = e.length; n--;) {
            (a = Tt(e[n]))[x] ? i.push(a) : r.push(a);
          }

          (a = E(t, function (t, e) {
            var n = e.length > 0,
                i = t.length > 0,
                r = function r(_r, s, a, c, u) {
              var f,
                  h,
                  m,
                  v = 0,
                  y = "0",
                  b = _r && [],
                  x = [],
                  w = l,
                  C = _r || i && o.find.TAG("*", u),
                  S = T += null == w ? 1 : Math.random() || .1,
                  $ = C.length;

              for (u && (l = s === p || s || u); y !== $ && null != (f = C[y]); y++) {
                if (i && f) {
                  for (h = 0, s || f.ownerDocument === p || (d(f), a = !g); m = t[h++];) {
                    if (m(f, s || p, a)) {
                      c.push(f);
                      break;
                    }
                  }

                  u && (T = S);
                }

                n && ((f = !m && f) && v--, _r && b.push(f));
              }

              if (v += y, n && y !== v) {
                for (h = 0; m = e[h++];) {
                  m(b, x, s, a);
                }

                if (_r) {
                  if (v > 0) for (; y--;) {
                    b[y] || x[y] || (x[y] = L.call(c));
                  }
                  x = xt(x);
                }

                j.apply(c, x), u && !_r && x.length > 0 && v + e.length > 1 && rt.uniqueSort(c);
              }

              return u && (T = S, l = w), b;
            };

            return n ? at(r) : r;
          }(r, i))).selector = t;
        }

        return a;
      }, c = rt.select = function (t, e, n, i) {
        var r,
            c,
            l,
            u,
            f,
            d = "function" == typeof t && t,
            p = !i && s(t = d.selector || t);

        if (n = n || [], 1 === p.length) {
          if ((c = p[0] = p[0].slice(0)).length > 2 && "ID" === (l = c[0]).type && 9 === e.nodeType && g && o.relative[c[1].type]) {
            if (!(e = (o.find.ID(l.matches[0].replace(J, tt), e) || [])[0])) return n;
            d && (e = e.parentNode), t = t.slice(c.shift().value.length);
          }

          for (r = V.needsContext.test(t) ? 0 : c.length; r-- && (l = c[r], !o.relative[u = l.type]);) {
            if ((f = o.find[u]) && (i = f(l.matches[0].replace(J, tt), Q.test(c[0].type) && gt(e.parentNode) || e))) {
              if (c.splice(r, 1), !(t = i.length && vt(c))) return j.apply(n, i), n;
              break;
            }
          }
        }

        return (d || a(t, p))(i, e, !g, n, !e || Q.test(t) && gt(e.parentNode) || e), n;
      }, n.sortStable = x.split("").sort(P).join("") === x, n.detectDuplicates = !!f, d(), n.sortDetached = ct(function (t) {
        return 1 & t.compareDocumentPosition(p.createElement("fieldset"));
      }), ct(function (t) {
        return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href");
      }) || lt("type|href|height|width", function (t, e, n) {
        if (!n) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2);
      }), n.attributes && ct(function (t) {
        return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value");
      }) || lt("value", function (t, e, n) {
        if (!n && "input" === t.nodeName.toLowerCase()) return t.defaultValue;
      }), ct(function (t) {
        return null == t.getAttribute("disabled");
      }) || lt(H, function (t, e, n) {
        var o;
        if (!n) return !0 === t[e] ? e.toLowerCase() : (o = t.getAttributeNode(e)) && o.specified ? o.value : null;
      }), rt;
    }(n);

    C.find = E, C.expr = E.selectors, C.expr[":"] = C.expr.pseudos, C.uniqueSort = C.unique = E.uniqueSort, C.text = E.getText, C.isXMLDoc = E.isXML, C.contains = E.contains, C.escapeSelector = E.escape;

    var P = function P(t, e, n) {
      for (var o = [], i = void 0 !== n; (t = t[e]) && 9 !== t.nodeType;) {
        if (1 === t.nodeType) {
          if (i && C(t).is(n)) break;
          o.push(t);
        }
      }

      return o;
    },
        k = function k(t, e) {
      for (var n = []; t; t = t.nextSibling) {
        1 === t.nodeType && t !== e && n.push(t);
      }

      return n;
    },
        A = C.expr.match.needsContext;

    function L(t, e) {
      return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
    }

    var D = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

    function j(t, e, n) {
      return y(e) ? C.grep(t, function (t, o) {
        return !!e.call(t, o, t) !== n;
      }) : e.nodeType ? C.grep(t, function (t) {
        return t === e !== n;
      }) : "string" != typeof e ? C.grep(t, function (t) {
        return f.call(e, t) > -1 !== n;
      }) : C.filter(e, t, n);
    }

    C.filter = function (t, e, n) {
      var o = e[0];
      return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === o.nodeType ? C.find.matchesSelector(o, t) ? [o] : [] : C.find.matches(t, C.grep(e, function (t) {
        return 1 === t.nodeType;
      }));
    }, C.fn.extend({
      find: function find(t) {
        var e,
            n,
            o = this.length,
            i = this;
        if ("string" != typeof t) return this.pushStack(C(t).filter(function () {
          for (e = 0; e < o; e++) {
            if (C.contains(i[e], this)) return !0;
          }
        }));

        for (n = this.pushStack([]), e = 0; e < o; e++) {
          C.find(t, i[e], n);
        }

        return o > 1 ? C.uniqueSort(n) : n;
      },
      filter: function filter(t) {
        return this.pushStack(j(this, t || [], !1));
      },
      not: function not(t) {
        return this.pushStack(j(this, t || [], !0));
      },
      is: function is(t) {
        return !!j(this, "string" == typeof t && A.test(t) ? C(t) : t || [], !1).length;
      }
    });
    var M,
        N = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (C.fn.init = function (t, e, n) {
      var o, i;
      if (!t) return this;

      if (n = n || M, "string" == typeof t) {
        if (!(o = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : N.exec(t)) || !o[1] && e) return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);

        if (o[1]) {
          if (e = e instanceof C ? e[0] : e, C.merge(this, C.parseHTML(o[1], e && e.nodeType ? e.ownerDocument || e : s, !0)), D.test(o[1]) && C.isPlainObject(e)) for (o in e) {
            y(this[o]) ? this[o](e[o]) : this.attr(o, e[o]);
          }
          return this;
        }

        return (i = s.getElementById(o[2])) && (this[0] = i, this.length = 1), this;
      }

      return t.nodeType ? (this[0] = t, this.length = 1, this) : y(t) ? void 0 !== n.ready ? n.ready(t) : t(C) : C.makeArray(t, this);
    }).prototype = C.fn, M = C(s);
    var H = /^(?:parents|prev(?:Until|All))/,
        q = {
      children: !0,
      contents: !0,
      next: !0,
      prev: !0
    };

    function I(t, e) {
      for (; (t = t[e]) && 1 !== t.nodeType;) {
        ;
      }

      return t;
    }

    C.fn.extend({
      has: function has(t) {
        var e = C(t, this),
            n = e.length;
        return this.filter(function () {
          for (var t = 0; t < n; t++) {
            if (C.contains(this, e[t])) return !0;
          }
        });
      },
      closest: function closest(t, e) {
        var n,
            o = 0,
            i = this.length,
            r = [],
            s = "string" != typeof t && C(t);
        if (!A.test(t)) for (; o < i; o++) {
          for (n = this[o]; n && n !== e; n = n.parentNode) {
            if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && C.find.matchesSelector(n, t))) {
              r.push(n);
              break;
            }
          }
        }
        return this.pushStack(r.length > 1 ? C.uniqueSort(r) : r);
      },
      index: function index(t) {
        return t ? "string" == typeof t ? f.call(C(t), this[0]) : f.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
      },
      add: function add(t, e) {
        return this.pushStack(C.uniqueSort(C.merge(this.get(), C(t, e))));
      },
      addBack: function addBack(t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
      }
    }), C.each({
      parent: function parent(t) {
        var e = t.parentNode;
        return e && 11 !== e.nodeType ? e : null;
      },
      parents: function parents(t) {
        return P(t, "parentNode");
      },
      parentsUntil: function parentsUntil(t, e, n) {
        return P(t, "parentNode", n);
      },
      next: function next(t) {
        return I(t, "nextSibling");
      },
      prev: function prev(t) {
        return I(t, "previousSibling");
      },
      nextAll: function nextAll(t) {
        return P(t, "nextSibling");
      },
      prevAll: function prevAll(t) {
        return P(t, "previousSibling");
      },
      nextUntil: function nextUntil(t, e, n) {
        return P(t, "nextSibling", n);
      },
      prevUntil: function prevUntil(t, e, n) {
        return P(t, "previousSibling", n);
      },
      siblings: function siblings(t) {
        return k((t.parentNode || {}).firstChild, t);
      },
      children: function children(t) {
        return k(t.firstChild);
      },
      contents: function contents(t) {
        return L(t, "iframe") ? t.contentDocument : (L(t, "template") && (t = t.content || t), C.merge([], t.childNodes));
      }
    }, function (t, e) {
      C.fn[t] = function (n, o) {
        var i = C.map(this, e, n);
        return "Until" !== t.slice(-5) && (o = n), o && "string" == typeof o && (i = C.filter(o, i)), this.length > 1 && (q[t] || C.uniqueSort(i), H.test(t) && i.reverse()), this.pushStack(i);
      };
    });
    var F = /[^\x20\t\r\n\f]+/g;

    function O(t) {
      return t;
    }

    function R(t) {
      throw t;
    }

    function z(t, e, n, o) {
      var i;

      try {
        t && y(i = t.promise) ? i.call(t).done(e).fail(n) : t && y(i = t.then) ? i.call(t, e, n) : e.apply(void 0, [t].slice(o));
      } catch (t) {
        n.apply(void 0, [t]);
      }
    }

    C.Callbacks = function (t) {
      t = "string" == typeof t ? function (t) {
        var e = {};
        return C.each(t.match(F) || [], function (t, n) {
          e[n] = !0;
        }), e;
      }(t) : C.extend({}, t);

      var e,
          n,
          o,
          i,
          r = [],
          s = [],
          a = -1,
          c = function c() {
        for (i = i || t.once, o = e = !0; s.length; a = -1) {
          for (n = s.shift(); ++a < r.length;) {
            !1 === r[a].apply(n[0], n[1]) && t.stopOnFalse && (a = r.length, n = !1);
          }
        }

        t.memory || (n = !1), e = !1, i && (r = n ? [] : "");
      },
          l = {
        add: function add() {
          return r && (n && !e && (a = r.length - 1, s.push(n)), function e(n) {
            C.each(n, function (n, o) {
              y(o) ? t.unique && l.has(o) || r.push(o) : o && o.length && "string" !== T(o) && e(o);
            });
          }(arguments), n && !e && c()), this;
        },
        remove: function remove() {
          return C.each(arguments, function (t, e) {
            for (var n; (n = C.inArray(e, r, n)) > -1;) {
              r.splice(n, 1), n <= a && a--;
            }
          }), this;
        },
        has: function has(t) {
          return t ? C.inArray(t, r) > -1 : r.length > 0;
        },
        empty: function empty() {
          return r && (r = []), this;
        },
        disable: function disable() {
          return i = s = [], r = n = "", this;
        },
        disabled: function disabled() {
          return !r;
        },
        lock: function lock() {
          return i = s = [], n || e || (r = n = ""), this;
        },
        locked: function locked() {
          return !!i;
        },
        fireWith: function fireWith(t, n) {
          return i || (n = [t, (n = n || []).slice ? n.slice() : n], s.push(n), e || c()), this;
        },
        fire: function fire() {
          return l.fireWith(this, arguments), this;
        },
        fired: function fired() {
          return !!o;
        }
      };

      return l;
    }, C.extend({
      Deferred: function Deferred(t) {
        var e = [["notify", "progress", C.Callbacks("memory"), C.Callbacks("memory"), 2], ["resolve", "done", C.Callbacks("once memory"), C.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", C.Callbacks("once memory"), C.Callbacks("once memory"), 1, "rejected"]],
            o = "pending",
            i = {
          state: function state() {
            return o;
          },
          always: function always() {
            return r.done(arguments).fail(arguments), this;
          },
          catch: function _catch(t) {
            return i.then(null, t);
          },
          pipe: function pipe() {
            var t = arguments;
            return C.Deferred(function (n) {
              C.each(e, function (e, o) {
                var i = y(t[o[4]]) && t[o[4]];
                r[o[1]](function () {
                  var t = i && i.apply(this, arguments);
                  t && y(t.promise) ? t.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[o[0] + "With"](this, i ? [t] : arguments);
                });
              }), t = null;
            }).promise();
          },
          then: function then(t, o, i) {
            var r = 0;

            function s(t, e, o, i) {
              return function () {
                var a = this,
                    c = arguments,
                    l = function l() {
                  var n, l;

                  if (!(t < r)) {
                    if ((n = o.apply(a, c)) === e.promise()) throw new TypeError("Thenable self-resolution");
                    l = n && ("object" == _typeof(n) || "function" == typeof n) && n.then, y(l) ? i ? l.call(n, s(r, e, O, i), s(r, e, R, i)) : (r++, l.call(n, s(r, e, O, i), s(r, e, R, i), s(r, e, O, e.notifyWith))) : (o !== O && (a = void 0, c = [n]), (i || e.resolveWith)(a, c));
                  }
                },
                    u = i ? l : function () {
                  try {
                    l();
                  } catch (n) {
                    C.Deferred.exceptionHook && C.Deferred.exceptionHook(n, u.stackTrace), t + 1 >= r && (o !== R && (a = void 0, c = [n]), e.rejectWith(a, c));
                  }
                };

                t ? u() : (C.Deferred.getStackHook && (u.stackTrace = C.Deferred.getStackHook()), n.setTimeout(u));
              };
            }

            return C.Deferred(function (n) {
              e[0][3].add(s(0, n, y(i) ? i : O, n.notifyWith)), e[1][3].add(s(0, n, y(t) ? t : O)), e[2][3].add(s(0, n, y(o) ? o : R));
            }).promise();
          },
          promise: function promise(t) {
            return null != t ? C.extend(t, i) : i;
          }
        },
            r = {};
        return C.each(e, function (t, n) {
          var s = n[2],
              a = n[5];
          i[n[1]] = s.add, a && s.add(function () {
            o = a;
          }, e[3 - t][2].disable, e[3 - t][3].disable, e[0][2].lock, e[0][3].lock), s.add(n[3].fire), r[n[0]] = function () {
            return r[n[0] + "With"](this === r ? void 0 : this, arguments), this;
          }, r[n[0] + "With"] = s.fireWith;
        }), i.promise(r), t && t.call(r, r), r;
      },
      when: function when(t) {
        var e = arguments.length,
            n = e,
            o = Array(n),
            i = c.call(arguments),
            r = C.Deferred(),
            s = function s(t) {
          return function (n) {
            o[t] = this, i[t] = arguments.length > 1 ? c.call(arguments) : n, --e || r.resolveWith(o, i);
          };
        };

        if (e <= 1 && (z(t, r.done(s(n)).resolve, r.reject, !e), "pending" === r.state() || y(i[n] && i[n].then))) return r.then();

        for (; n--;) {
          z(i[n], s(n), r.reject);
        }

        return r.promise();
      }
    });
    var B = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    C.Deferred.exceptionHook = function (t, e) {
      n.console && n.console.warn && t && B.test(t.name) && n.console.warn("jQuery.Deferred exception: " + t.message, t.stack, e);
    }, C.readyException = function (t) {
      n.setTimeout(function () {
        throw t;
      });
    };

    var _ = C.Deferred();

    function W() {
      s.removeEventListener("DOMContentLoaded", W), n.removeEventListener("load", W), C.ready();
    }

    C.fn.ready = function (t) {
      return _.then(t).catch(function (t) {
        C.readyException(t);
      }), this;
    }, C.extend({
      isReady: !1,
      readyWait: 1,
      ready: function ready(t) {
        (!0 === t ? --C.readyWait : C.isReady) || (C.isReady = !0, !0 !== t && --C.readyWait > 0 || _.resolveWith(s, [C]));
      }
    }), C.ready.then = _.then, "complete" === s.readyState || "loading" !== s.readyState && !s.documentElement.doScroll ? n.setTimeout(C.ready) : (s.addEventListener("DOMContentLoaded", W), n.addEventListener("load", W));

    var X = function X(t, e, n, o, i, r, s) {
      var a = 0,
          c = t.length,
          l = null == n;
      if ("object" === T(n)) for (a in i = !0, n) {
        X(t, e, a, n[a], !0, r, s);
      } else if (void 0 !== o && (i = !0, y(o) || (s = !0), l && (s ? (e.call(t, o), e = null) : (l = e, e = function e(t, _e2, n) {
        return l.call(C(t), n);
      })), e)) for (; a < c; a++) {
        e(t[a], n, s ? o : o.call(t[a], a, e(t[a], n)));
      }
      return i ? t : l ? e.call(t) : c ? e(t[0], n) : r;
    },
        Y = /^-ms-/,
        V = /-([a-z])/g;

    function U(t, e) {
      return e.toUpperCase();
    }

    function Z(t) {
      return t.replace(Y, "ms-").replace(V, U);
    }

    var G = function G(t) {
      return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType;
    };

    function K() {
      this.expando = C.expando + K.uid++;
    }

    K.uid = 1, K.prototype = {
      cache: function cache(t) {
        var e = t[this.expando];
        return e || (e = {}, G(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
          value: e,
          configurable: !0
        }))), e;
      },
      set: function set(t, e, n) {
        var o,
            i = this.cache(t);
        if ("string" == typeof e) i[Z(e)] = n;else for (o in e) {
          i[Z(o)] = e[o];
        }
        return i;
      },
      get: function get(t, e) {
        return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][Z(e)];
      },
      access: function access(t, e, n) {
        return void 0 === e || e && "string" == typeof e && void 0 === n ? this.get(t, e) : (this.set(t, e, n), void 0 !== n ? n : e);
      },
      remove: function remove(t, e) {
        var n,
            o = t[this.expando];

        if (void 0 !== o) {
          if (void 0 !== e) {
            n = (e = Array.isArray(e) ? e.map(Z) : (e = Z(e)) in o ? [e] : e.match(F) || []).length;

            for (; n--;) {
              delete o[e[n]];
            }
          }

          (void 0 === e || C.isEmptyObject(o)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando]);
        }
      },
      hasData: function hasData(t) {
        var e = t[this.expando];
        return void 0 !== e && !C.isEmptyObject(e);
      }
    };
    var Q = new K(),
        J = new K(),
        tt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        et = /[A-Z]/g;

    function nt(t, e, n) {
      var o;
      if (void 0 === n && 1 === t.nodeType) if (o = "data-" + e.replace(et, "-$&").toLowerCase(), "string" == typeof (n = t.getAttribute(o))) {
        try {
          n = function (t) {
            return "true" === t || "false" !== t && ("null" === t ? null : t === +t + "" ? +t : tt.test(t) ? JSON.parse(t) : t);
          }(n);
        } catch (t) {}

        J.set(t, e, n);
      } else n = void 0;
      return n;
    }

    C.extend({
      hasData: function hasData(t) {
        return J.hasData(t) || Q.hasData(t);
      },
      data: function data(t, e, n) {
        return J.access(t, e, n);
      },
      removeData: function removeData(t, e) {
        J.remove(t, e);
      },
      _data: function _data(t, e, n) {
        return Q.access(t, e, n);
      },
      _removeData: function _removeData(t, e) {
        Q.remove(t, e);
      }
    }), C.fn.extend({
      data: function data(t, e) {
        var n,
            o,
            i,
            r = this[0],
            s = r && r.attributes;

        if (void 0 === t) {
          if (this.length && (i = J.get(r), 1 === r.nodeType && !Q.get(r, "hasDataAttrs"))) {
            for (n = s.length; n--;) {
              s[n] && 0 === (o = s[n].name).indexOf("data-") && (o = Z(o.slice(5)), nt(r, o, i[o]));
            }

            Q.set(r, "hasDataAttrs", !0);
          }

          return i;
        }

        return "object" == _typeof(t) ? this.each(function () {
          J.set(this, t);
        }) : X(this, function (e) {
          var n;
          if (r && void 0 === e) return void 0 !== (n = J.get(r, t)) ? n : void 0 !== (n = nt(r, t)) ? n : void 0;
          this.each(function () {
            J.set(this, t, e);
          });
        }, null, e, arguments.length > 1, null, !0);
      },
      removeData: function removeData(t) {
        return this.each(function () {
          J.remove(this, t);
        });
      }
    }), C.extend({
      queue: function queue(t, e, n) {
        var o;
        if (t) return e = (e || "fx") + "queue", o = Q.get(t, e), n && (!o || Array.isArray(n) ? o = Q.access(t, e, C.makeArray(n)) : o.push(n)), o || [];
      },
      dequeue: function dequeue(t, e) {
        e = e || "fx";

        var n = C.queue(t, e),
            o = n.length,
            i = n.shift(),
            r = C._queueHooks(t, e);

        "inprogress" === i && (i = n.shift(), o--), i && ("fx" === e && n.unshift("inprogress"), delete r.stop, i.call(t, function () {
          C.dequeue(t, e);
        }, r)), !o && r && r.empty.fire();
      },
      _queueHooks: function _queueHooks(t, e) {
        var n = e + "queueHooks";
        return Q.get(t, n) || Q.access(t, n, {
          empty: C.Callbacks("once memory").add(function () {
            Q.remove(t, [e + "queue", n]);
          })
        });
      }
    }), C.fn.extend({
      queue: function queue(t, e) {
        var n = 2;
        return "string" != typeof t && (e = t, t = "fx", n--), arguments.length < n ? C.queue(this[0], t) : void 0 === e ? this : this.each(function () {
          var n = C.queue(this, t, e);
          C._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && C.dequeue(this, t);
        });
      },
      dequeue: function dequeue(t) {
        return this.each(function () {
          C.dequeue(this, t);
        });
      },
      clearQueue: function clearQueue(t) {
        return this.queue(t || "fx", []);
      },
      promise: function promise(t, e) {
        var n,
            o = 1,
            i = C.Deferred(),
            r = this,
            s = this.length,
            a = function a() {
          --o || i.resolveWith(r, [r]);
        };

        for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; s--;) {
          (n = Q.get(r[s], t + "queueHooks")) && n.empty && (o++, n.empty.add(a));
        }

        return a(), i.promise(e);
      }
    });

    var ot = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        it = new RegExp("^(?:([+-])=|)(" + ot + ")([a-z%]*)$", "i"),
        rt = ["Top", "Right", "Bottom", "Left"],
        st = function st(t, e) {
      return "none" === (t = e || t).style.display || "" === t.style.display && C.contains(t.ownerDocument, t) && "none" === C.css(t, "display");
    },
        at = function at(t, e, n, o) {
      var i,
          r,
          s = {};

      for (r in e) {
        s[r] = t.style[r], t.style[r] = e[r];
      }

      for (r in i = n.apply(t, o || []), e) {
        t.style[r] = s[r];
      }

      return i;
    };

    function ct(t, e, n, o) {
      var i,
          r,
          s = 20,
          a = o ? function () {
        return o.cur();
      } : function () {
        return C.css(t, e, "");
      },
          c = a(),
          l = n && n[3] || (C.cssNumber[e] ? "" : "px"),
          u = (C.cssNumber[e] || "px" !== l && +c) && it.exec(C.css(t, e));

      if (u && u[3] !== l) {
        for (c /= 2, l = l || u[3], u = +c || 1; s--;) {
          C.style(t, e, u + l), (1 - r) * (1 - (r = a() / c || .5)) <= 0 && (s = 0), u /= r;
        }

        u *= 2, C.style(t, e, u + l), n = n || [];
      }

      return n && (u = +u || +c || 0, i = n[1] ? u + (n[1] + 1) * n[2] : +n[2], o && (o.unit = l, o.start = u, o.end = i)), i;
    }

    var lt = {};

    function ut(t) {
      var e,
          n = t.ownerDocument,
          o = t.nodeName,
          i = lt[o];
      return i || (e = n.body.appendChild(n.createElement(o)), i = C.css(e, "display"), e.parentNode.removeChild(e), "none" === i && (i = "block"), lt[o] = i, i);
    }

    function ft(t, e) {
      for (var n, o, i = [], r = 0, s = t.length; r < s; r++) {
        (o = t[r]).style && (n = o.style.display, e ? ("none" === n && (i[r] = Q.get(o, "display") || null, i[r] || (o.style.display = "")), "" === o.style.display && st(o) && (i[r] = ut(o))) : "none" !== n && (i[r] = "none", Q.set(o, "display", n)));
      }

      for (r = 0; r < s; r++) {
        null != i[r] && (t[r].style.display = i[r]);
      }

      return t;
    }

    C.fn.extend({
      show: function show() {
        return ft(this, !0);
      },
      hide: function hide() {
        return ft(this);
      },
      toggle: function toggle(t) {
        return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
          st(this) ? C(this).show() : C(this).hide();
        });
      }
    });
    var dt = /^(?:checkbox|radio)$/i,
        pt = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        ht = /^$|^module$|\/(?:java|ecma)script/i,
        gt = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      thead: [1, "<table>", "</table>"],
      col: [2, "<table><colgroup>", "</colgroup></table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: [0, "", ""]
    };

    function mt(t, e) {
      var n;
      return n = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [], void 0 === e || e && L(t, e) ? C.merge([t], n) : n;
    }

    function vt(t, e) {
      for (var n = 0, o = t.length; n < o; n++) {
        Q.set(t[n], "globalEval", !e || Q.get(e[n], "globalEval"));
      }
    }

    gt.optgroup = gt.option, gt.tbody = gt.tfoot = gt.colgroup = gt.caption = gt.thead, gt.th = gt.td;
    var yt = /<|&#?\w+;/;

    function bt(t, e, n, o, i) {
      for (var r, s, a, c, l, u, f = e.createDocumentFragment(), d = [], p = 0, h = t.length; p < h; p++) {
        if ((r = t[p]) || 0 === r) if ("object" === T(r)) C.merge(d, r.nodeType ? [r] : r);else if (yt.test(r)) {
          for (s = s || f.appendChild(e.createElement("div")), a = (pt.exec(r) || ["", ""])[1].toLowerCase(), c = gt[a] || gt._default, s.innerHTML = c[1] + C.htmlPrefilter(r) + c[2], u = c[0]; u--;) {
            s = s.lastChild;
          }

          C.merge(d, s.childNodes), (s = f.firstChild).textContent = "";
        } else d.push(e.createTextNode(r));
      }

      for (f.textContent = "", p = 0; r = d[p++];) {
        if (o && C.inArray(r, o) > -1) i && i.push(r);else if (l = C.contains(r.ownerDocument, r), s = mt(f.appendChild(r), "script"), l && vt(s), n) for (u = 0; r = s[u++];) {
          ht.test(r.type || "") && n.push(r);
        }
      }

      return f;
    }

    !function () {
      var t = s.createDocumentFragment().appendChild(s.createElement("div")),
          e = s.createElement("input");
      e.setAttribute("type", "radio"), e.setAttribute("checked", "checked"), e.setAttribute("name", "t"), t.appendChild(e), v.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", v.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue;
    }();
    var xt = s.documentElement,
        wt = /^key/,
        Tt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Ct = /^([^.]*)(?:\.(.+)|)/;

    function St() {
      return !0;
    }

    function $t() {
      return !1;
    }

    function Et() {
      try {
        return s.activeElement;
      } catch (t) {}
    }

    function Pt(t, e, n, o, i, r) {
      var s, a;

      if ("object" == _typeof(e)) {
        for (a in "string" != typeof n && (o = o || n, n = void 0), e) {
          Pt(t, a, n, o, e[a], r);
        }

        return t;
      }

      if (null == o && null == i ? (i = n, o = n = void 0) : null == i && ("string" == typeof n ? (i = o, o = void 0) : (i = o, o = n, n = void 0)), !1 === i) i = $t;else if (!i) return t;
      return 1 === r && (s = i, (i = function i(t) {
        return C().off(t), s.apply(this, arguments);
      }).guid = s.guid || (s.guid = C.guid++)), t.each(function () {
        C.event.add(this, e, i, o, n);
      });
    }

    C.event = {
      global: {},
      add: function add(t, e, n, o, i) {
        var r,
            s,
            a,
            c,
            l,
            u,
            f,
            d,
            p,
            h,
            g,
            m = Q.get(t);
        if (m) for (n.handler && (n = (r = n).handler, i = r.selector), i && C.find.matchesSelector(xt, i), n.guid || (n.guid = C.guid++), (c = m.events) || (c = m.events = {}), (s = m.handle) || (s = m.handle = function (e) {
          return void 0 !== C && C.event.triggered !== e.type ? C.event.dispatch.apply(t, arguments) : void 0;
        }), l = (e = (e || "").match(F) || [""]).length; l--;) {
          p = g = (a = Ct.exec(e[l]) || [])[1], h = (a[2] || "").split(".").sort(), p && (f = C.event.special[p] || {}, p = (i ? f.delegateType : f.bindType) || p, f = C.event.special[p] || {}, u = C.extend({
            type: p,
            origType: g,
            data: o,
            handler: n,
            guid: n.guid,
            selector: i,
            needsContext: i && C.expr.match.needsContext.test(i),
            namespace: h.join(".")
          }, r), (d = c[p]) || ((d = c[p] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(t, o, h, s) || t.addEventListener && t.addEventListener(p, s)), f.add && (f.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), i ? d.splice(d.delegateCount++, 0, u) : d.push(u), C.event.global[p] = !0);
        }
      },
      remove: function remove(t, e, n, o, i) {
        var r,
            s,
            a,
            c,
            l,
            u,
            f,
            d,
            p,
            h,
            g,
            m = Q.hasData(t) && Q.get(t);

        if (m && (c = m.events)) {
          for (l = (e = (e || "").match(F) || [""]).length; l--;) {
            if (p = g = (a = Ct.exec(e[l]) || [])[1], h = (a[2] || "").split(".").sort(), p) {
              for (f = C.event.special[p] || {}, d = c[p = (o ? f.delegateType : f.bindType) || p] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = d.length; r--;) {
                u = d[r], !i && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || o && o !== u.selector && ("**" !== o || !u.selector) || (d.splice(r, 1), u.selector && d.delegateCount--, f.remove && f.remove.call(t, u));
              }

              s && !d.length && (f.teardown && !1 !== f.teardown.call(t, h, m.handle) || C.removeEvent(t, p, m.handle), delete c[p]);
            } else for (p in c) {
              C.event.remove(t, p + e[l], n, o, !0);
            }
          }

          C.isEmptyObject(c) && Q.remove(t, "handle events");
        }
      },
      dispatch: function dispatch(t) {
        var e,
            n,
            o,
            i,
            r,
            s,
            a = C.event.fix(t),
            c = new Array(arguments.length),
            l = (Q.get(this, "events") || {})[a.type] || [],
            u = C.event.special[a.type] || {};

        for (c[0] = a, e = 1; e < arguments.length; e++) {
          c[e] = arguments[e];
        }

        if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
          for (s = C.event.handlers.call(this, a, l), e = 0; (i = s[e++]) && !a.isPropagationStopped();) {
            for (a.currentTarget = i.elem, n = 0; (r = i.handlers[n++]) && !a.isImmediatePropagationStopped();) {
              a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (o = ((C.event.special[r.origType] || {}).handle || r.handler).apply(i.elem, c)) && !1 === (a.result = o) && (a.preventDefault(), a.stopPropagation()));
            }
          }

          return u.postDispatch && u.postDispatch.call(this, a), a.result;
        }
      },
      handlers: function handlers(t, e) {
        var n,
            o,
            i,
            r,
            s,
            a = [],
            c = e.delegateCount,
            l = t.target;
        if (c && l.nodeType && !("click" === t.type && t.button >= 1)) for (; l !== this; l = l.parentNode || this) {
          if (1 === l.nodeType && ("click" !== t.type || !0 !== l.disabled)) {
            for (r = [], s = {}, n = 0; n < c; n++) {
              void 0 === s[i = (o = e[n]).selector + " "] && (s[i] = o.needsContext ? C(i, this).index(l) > -1 : C.find(i, this, null, [l]).length), s[i] && r.push(o);
            }

            r.length && a.push({
              elem: l,
              handlers: r
            });
          }
        }
        return l = this, c < e.length && a.push({
          elem: l,
          handlers: e.slice(c)
        }), a;
      },
      addProp: function addProp(t, e) {
        Object.defineProperty(C.Event.prototype, t, {
          enumerable: !0,
          configurable: !0,
          get: y(e) ? function () {
            if (this.originalEvent) return e(this.originalEvent);
          } : function () {
            if (this.originalEvent) return this.originalEvent[t];
          },
          set: function set(e) {
            Object.defineProperty(this, t, {
              enumerable: !0,
              configurable: !0,
              writable: !0,
              value: e
            });
          }
        });
      },
      fix: function fix(t) {
        return t[C.expando] ? t : new C.Event(t);
      },
      special: {
        load: {
          noBubble: !0
        },
        focus: {
          trigger: function trigger() {
            if (this !== Et() && this.focus) return this.focus(), !1;
          },
          delegateType: "focusin"
        },
        blur: {
          trigger: function trigger() {
            if (this === Et() && this.blur) return this.blur(), !1;
          },
          delegateType: "focusout"
        },
        click: {
          trigger: function trigger() {
            if ("checkbox" === this.type && this.click && L(this, "input")) return this.click(), !1;
          },
          _default: function _default(t) {
            return L(t.target, "a");
          }
        },
        beforeunload: {
          postDispatch: function postDispatch(t) {
            void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result);
          }
        }
      }
    }, C.removeEvent = function (t, e, n) {
      t.removeEventListener && t.removeEventListener(e, n);
    }, C.Event = function (t, e) {
      if (!(this instanceof C.Event)) return new C.Event(t, e);
      t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? St : $t, this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target, this.currentTarget = t.currentTarget, this.relatedTarget = t.relatedTarget) : this.type = t, e && C.extend(this, e), this.timeStamp = t && t.timeStamp || Date.now(), this[C.expando] = !0;
    }, C.Event.prototype = {
      constructor: C.Event,
      isDefaultPrevented: $t,
      isPropagationStopped: $t,
      isImmediatePropagationStopped: $t,
      isSimulated: !1,
      preventDefault: function preventDefault() {
        var t = this.originalEvent;
        this.isDefaultPrevented = St, t && !this.isSimulated && t.preventDefault();
      },
      stopPropagation: function stopPropagation() {
        var t = this.originalEvent;
        this.isPropagationStopped = St, t && !this.isSimulated && t.stopPropagation();
      },
      stopImmediatePropagation: function stopImmediatePropagation() {
        var t = this.originalEvent;
        this.isImmediatePropagationStopped = St, t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation();
      }
    }, C.each({
      altKey: !0,
      bubbles: !0,
      cancelable: !0,
      changedTouches: !0,
      ctrlKey: !0,
      detail: !0,
      eventPhase: !0,
      metaKey: !0,
      pageX: !0,
      pageY: !0,
      shiftKey: !0,
      view: !0,
      char: !0,
      charCode: !0,
      key: !0,
      keyCode: !0,
      button: !0,
      buttons: !0,
      clientX: !0,
      clientY: !0,
      offsetX: !0,
      offsetY: !0,
      pointerId: !0,
      pointerType: !0,
      screenX: !0,
      screenY: !0,
      targetTouches: !0,
      toElement: !0,
      touches: !0,
      which: function which(t) {
        var e = t.button;
        return null == t.which && wt.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && Tt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which;
      }
    }, C.event.addProp), C.each({
      mouseenter: "mouseover",
      mouseleave: "mouseout",
      pointerenter: "pointerover",
      pointerleave: "pointerout"
    }, function (t, e) {
      C.event.special[t] = {
        delegateType: e,
        bindType: e,
        handle: function handle(t) {
          var n,
              o = t.relatedTarget,
              i = t.handleObj;
          return o && (o === this || C.contains(this, o)) || (t.type = i.origType, n = i.handler.apply(this, arguments), t.type = e), n;
        }
      };
    }), C.fn.extend({
      on: function on(t, e, n, o) {
        return Pt(this, t, e, n, o);
      },
      one: function one(t, e, n, o) {
        return Pt(this, t, e, n, o, 1);
      },
      off: function off(t, e, n) {
        var o, i;
        if (t && t.preventDefault && t.handleObj) return o = t.handleObj, C(t.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler), this;

        if ("object" == _typeof(t)) {
          for (i in t) {
            this.off(i, e, t[i]);
          }

          return this;
        }

        return !1 !== e && "function" != typeof e || (n = e, e = void 0), !1 === n && (n = $t), this.each(function () {
          C.event.remove(this, t, n, e);
        });
      }
    });
    var kt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        At = /<script|<style|<link/i,
        Lt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Dt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function jt(t, e) {
      return L(t, "table") && L(11 !== e.nodeType ? e : e.firstChild, "tr") && C(t).children("tbody")[0] || t;
    }

    function Mt(t) {
      return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t;
    }

    function Nt(t) {
      return "true/" === (t.type || "").slice(0, 5) ? t.type = t.type.slice(5) : t.removeAttribute("type"), t;
    }

    function Ht(t, e) {
      var n, o, i, r, s, a, c, l;

      if (1 === e.nodeType) {
        if (Q.hasData(t) && (r = Q.access(t), s = Q.set(e, r), l = r.events)) for (i in delete s.handle, s.events = {}, l) {
          for (n = 0, o = l[i].length; n < o; n++) {
            C.event.add(e, i, l[i][n]);
          }
        }
        J.hasData(t) && (a = J.access(t), c = C.extend({}, a), J.set(e, c));
      }
    }

    function qt(t, e) {
      var n = e.nodeName.toLowerCase();
      "input" === n && dt.test(t.type) ? e.checked = t.checked : "input" !== n && "textarea" !== n || (e.defaultValue = t.defaultValue);
    }

    function It(t, e, n, o) {
      e = l.apply([], e);
      var i,
          r,
          s,
          a,
          c,
          u,
          f = 0,
          d = t.length,
          p = d - 1,
          h = e[0],
          g = y(h);
      if (g || d > 1 && "string" == typeof h && !v.checkClone && Lt.test(h)) return t.each(function (i) {
        var r = t.eq(i);
        g && (e[0] = h.call(this, i, r.html())), It(r, e, n, o);
      });

      if (d && (r = (i = bt(e, t[0].ownerDocument, !1, t, o)).firstChild, 1 === i.childNodes.length && (i = r), r || o)) {
        for (a = (s = C.map(mt(i, "script"), Mt)).length; f < d; f++) {
          c = i, f !== p && (c = C.clone(c, !0, !0), a && C.merge(s, mt(c, "script"))), n.call(t[f], c, f);
        }

        if (a) for (u = s[s.length - 1].ownerDocument, C.map(s, Nt), f = 0; f < a; f++) {
          c = s[f], ht.test(c.type || "") && !Q.access(c, "globalEval") && C.contains(u, c) && (c.src && "module" !== (c.type || "").toLowerCase() ? C._evalUrl && C._evalUrl(c.src) : w(c.textContent.replace(Dt, ""), u, c));
        }
      }

      return t;
    }

    function Ft(t, e, n) {
      for (var o, i = e ? C.filter(e, t) : t, r = 0; null != (o = i[r]); r++) {
        n || 1 !== o.nodeType || C.cleanData(mt(o)), o.parentNode && (n && C.contains(o.ownerDocument, o) && vt(mt(o, "script")), o.parentNode.removeChild(o));
      }

      return t;
    }

    C.extend({
      htmlPrefilter: function htmlPrefilter(t) {
        return t.replace(kt, "<$1></$2>");
      },
      clone: function clone(t, e, n) {
        var o,
            i,
            r,
            s,
            a = t.cloneNode(!0),
            c = C.contains(t.ownerDocument, t);
        if (!(v.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || C.isXMLDoc(t))) for (s = mt(a), o = 0, i = (r = mt(t)).length; o < i; o++) {
          qt(r[o], s[o]);
        }
        if (e) if (n) for (r = r || mt(t), s = s || mt(a), o = 0, i = r.length; o < i; o++) {
          Ht(r[o], s[o]);
        } else Ht(t, a);
        return (s = mt(a, "script")).length > 0 && vt(s, !c && mt(t, "script")), a;
      },
      cleanData: function cleanData(t) {
        for (var e, n, o, i = C.event.special, r = 0; void 0 !== (n = t[r]); r++) {
          if (G(n)) {
            if (e = n[Q.expando]) {
              if (e.events) for (o in e.events) {
                i[o] ? C.event.remove(n, o) : C.removeEvent(n, o, e.handle);
              }
              n[Q.expando] = void 0;
            }

            n[J.expando] && (n[J.expando] = void 0);
          }
        }
      }
    }), C.fn.extend({
      detach: function detach(t) {
        return Ft(this, t, !0);
      },
      remove: function remove(t) {
        return Ft(this, t);
      },
      text: function text(t) {
        return X(this, function (t) {
          return void 0 === t ? C.text(this) : this.empty().each(function () {
            1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t);
          });
        }, null, t, arguments.length);
      },
      append: function append() {
        return It(this, arguments, function (t) {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || jt(this, t).appendChild(t);
        });
      },
      prepend: function prepend() {
        return It(this, arguments, function (t) {
          if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
            var e = jt(this, t);
            e.insertBefore(t, e.firstChild);
          }
        });
      },
      before: function before() {
        return It(this, arguments, function (t) {
          this.parentNode && this.parentNode.insertBefore(t, this);
        });
      },
      after: function after() {
        return It(this, arguments, function (t) {
          this.parentNode && this.parentNode.insertBefore(t, this.nextSibling);
        });
      },
      empty: function empty() {
        for (var t, e = 0; null != (t = this[e]); e++) {
          1 === t.nodeType && (C.cleanData(mt(t, !1)), t.textContent = "");
        }

        return this;
      },
      clone: function clone(t, e) {
        return t = null != t && t, e = null == e ? t : e, this.map(function () {
          return C.clone(this, t, e);
        });
      },
      html: function html(t) {
        return X(this, function (t) {
          var e = this[0] || {},
              n = 0,
              o = this.length;
          if (void 0 === t && 1 === e.nodeType) return e.innerHTML;

          if ("string" == typeof t && !At.test(t) && !gt[(pt.exec(t) || ["", ""])[1].toLowerCase()]) {
            t = C.htmlPrefilter(t);

            try {
              for (; n < o; n++) {
                1 === (e = this[n] || {}).nodeType && (C.cleanData(mt(e, !1)), e.innerHTML = t);
              }

              e = 0;
            } catch (t) {}
          }

          e && this.empty().append(t);
        }, null, t, arguments.length);
      },
      replaceWith: function replaceWith() {
        var t = [];
        return It(this, arguments, function (e) {
          var n = this.parentNode;
          C.inArray(this, t) < 0 && (C.cleanData(mt(this)), n && n.replaceChild(e, this));
        }, t);
      }
    }), C.each({
      appendTo: "append",
      prependTo: "prepend",
      insertBefore: "before",
      insertAfter: "after",
      replaceAll: "replaceWith"
    }, function (t, e) {
      C.fn[t] = function (t) {
        for (var n, o = [], i = C(t), r = i.length - 1, s = 0; s <= r; s++) {
          n = s === r ? this : this.clone(!0), C(i[s])[e](n), u.apply(o, n.get());
        }

        return this.pushStack(o);
      };
    });

    var Ot = new RegExp("^(" + ot + ")(?!px)[a-z%]+$", "i"),
        Rt = function Rt(t) {
      var e = t.ownerDocument.defaultView;
      return e && e.opener || (e = n), e.getComputedStyle(t);
    },
        zt = new RegExp(rt.join("|"), "i");

    function Bt(t, e, n) {
      var o,
          i,
          r,
          s,
          a = t.style;
      return (n = n || Rt(t)) && ("" !== (s = n.getPropertyValue(e) || n[e]) || C.contains(t.ownerDocument, t) || (s = C.style(t, e)), !v.pixelBoxStyles() && Ot.test(s) && zt.test(e) && (o = a.width, i = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = o, a.minWidth = i, a.maxWidth = r)), void 0 !== s ? s + "" : s;
    }

    function _t(t, e) {
      return {
        get: function get() {
          if (!t()) return (this.get = e).apply(this, arguments);
          delete this.get;
        }
      };
    }

    !function () {
      function t() {
        if (u) {
          l.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", u.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", xt.appendChild(l).appendChild(u);
          var t = n.getComputedStyle(u);
          o = "1%" !== t.top, c = 12 === e(t.marginLeft), u.style.right = "60%", a = 36 === e(t.right), i = 36 === e(t.width), u.style.position = "absolute", r = 36 === u.offsetWidth || "absolute", xt.removeChild(l), u = null;
        }
      }

      function e(t) {
        return Math.round(parseFloat(t));
      }

      var o,
          i,
          r,
          a,
          c,
          l = s.createElement("div"),
          u = s.createElement("div");
      u.style && (u.style.backgroundClip = "content-box", u.cloneNode(!0).style.backgroundClip = "", v.clearCloneStyle = "content-box" === u.style.backgroundClip, C.extend(v, {
        boxSizingReliable: function boxSizingReliable() {
          return t(), i;
        },
        pixelBoxStyles: function pixelBoxStyles() {
          return t(), a;
        },
        pixelPosition: function pixelPosition() {
          return t(), o;
        },
        reliableMarginLeft: function reliableMarginLeft() {
          return t(), c;
        },
        scrollboxSize: function scrollboxSize() {
          return t(), r;
        }
      }));
    }();
    var Wt = /^(none|table(?!-c[ea]).+)/,
        Xt = /^--/,
        Yt = {
      position: "absolute",
      visibility: "hidden",
      display: "block"
    },
        Vt = {
      letterSpacing: "0",
      fontWeight: "400"
    },
        Ut = ["Webkit", "Moz", "ms"],
        Zt = s.createElement("div").style;

    function Gt(t) {
      var e = C.cssProps[t];
      return e || (e = C.cssProps[t] = function (t) {
        if (t in Zt) return t;

        for (var e = t[0].toUpperCase() + t.slice(1), n = Ut.length; n--;) {
          if ((t = Ut[n] + e) in Zt) return t;
        }
      }(t) || t), e;
    }

    function Kt(t, e, n) {
      var o = it.exec(e);
      return o ? Math.max(0, o[2] - (n || 0)) + (o[3] || "px") : e;
    }

    function Qt(t, e, n, o, i, r) {
      var s = "width" === e ? 1 : 0,
          a = 0,
          c = 0;
      if (n === (o ? "border" : "content")) return 0;

      for (; s < 4; s += 2) {
        "margin" === n && (c += C.css(t, n + rt[s], !0, i)), o ? ("content" === n && (c -= C.css(t, "padding" + rt[s], !0, i)), "margin" !== n && (c -= C.css(t, "border" + rt[s] + "Width", !0, i))) : (c += C.css(t, "padding" + rt[s], !0, i), "padding" !== n ? c += C.css(t, "border" + rt[s] + "Width", !0, i) : a += C.css(t, "border" + rt[s] + "Width", !0, i));
      }

      return !o && r >= 0 && (c += Math.max(0, Math.ceil(t["offset" + e[0].toUpperCase() + e.slice(1)] - r - c - a - .5))), c;
    }

    function Jt(t, e, n) {
      var o = Rt(t),
          i = Bt(t, e, o),
          r = "border-box" === C.css(t, "boxSizing", !1, o),
          s = r;

      if (Ot.test(i)) {
        if (!n) return i;
        i = "auto";
      }

      return s = s && (v.boxSizingReliable() || i === t.style[e]), ("auto" === i || !parseFloat(i) && "inline" === C.css(t, "display", !1, o)) && (i = t["offset" + e[0].toUpperCase() + e.slice(1)], s = !0), (i = parseFloat(i) || 0) + Qt(t, e, n || (r ? "border" : "content"), s, o, i) + "px";
    }

    function te(t, e, n, o, i) {
      return new te.prototype.init(t, e, n, o, i);
    }

    C.extend({
      cssHooks: {
        opacity: {
          get: function get(t, e) {
            if (e) {
              var n = Bt(t, "opacity");
              return "" === n ? "1" : n;
            }
          }
        }
      },
      cssNumber: {
        animationIterationCount: !0,
        columnCount: !0,
        fillOpacity: !0,
        flexGrow: !0,
        flexShrink: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        order: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0
      },
      cssProps: {},
      style: function style(t, e, n, o) {
        if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
          var i,
              r,
              s,
              a = Z(e),
              c = Xt.test(e),
              l = t.style;
          if (c || (e = Gt(a)), s = C.cssHooks[e] || C.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (i = s.get(t, !1, o)) ? i : l[e];
          "string" === (r = _typeof(n)) && (i = it.exec(n)) && i[1] && (n = ct(t, e, i), r = "number"), null != n && n == n && ("number" === r && (n += i && i[3] || (C.cssNumber[a] ? "" : "px")), v.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"), s && "set" in s && void 0 === (n = s.set(t, n, o)) || (c ? l.setProperty(e, n) : l[e] = n));
        }
      },
      css: function css(t, e, n, o) {
        var i,
            r,
            s,
            a = Z(e);
        return Xt.test(e) || (e = Gt(a)), (s = C.cssHooks[e] || C.cssHooks[a]) && "get" in s && (i = s.get(t, !0, n)), void 0 === i && (i = Bt(t, e, o)), "normal" === i && e in Vt && (i = Vt[e]), "" === n || n ? (r = parseFloat(i), !0 === n || isFinite(r) ? r || 0 : i) : i;
      }
    }), C.each(["height", "width"], function (t, e) {
      C.cssHooks[e] = {
        get: function get(t, n, o) {
          if (n) return !Wt.test(C.css(t, "display")) || t.getClientRects().length && t.getBoundingClientRect().width ? Jt(t, e, o) : at(t, Yt, function () {
            return Jt(t, e, o);
          });
        },
        set: function set(t, n, o) {
          var i,
              r = Rt(t),
              s = "border-box" === C.css(t, "boxSizing", !1, r),
              a = o && Qt(t, e, o, s, r);
          return s && v.scrollboxSize() === r.position && (a -= Math.ceil(t["offset" + e[0].toUpperCase() + e.slice(1)] - parseFloat(r[e]) - Qt(t, e, "border", !1, r) - .5)), a && (i = it.exec(n)) && "px" !== (i[3] || "px") && (t.style[e] = n, n = C.css(t, e)), Kt(0, n, a);
        }
      };
    }), C.cssHooks.marginLeft = _t(v.reliableMarginLeft, function (t, e) {
      if (e) return (parseFloat(Bt(t, "marginLeft")) || t.getBoundingClientRect().left - at(t, {
        marginLeft: 0
      }, function () {
        return t.getBoundingClientRect().left;
      })) + "px";
    }), C.each({
      margin: "",
      padding: "",
      border: "Width"
    }, function (t, e) {
      C.cssHooks[t + e] = {
        expand: function expand(n) {
          for (var o = 0, i = {}, r = "string" == typeof n ? n.split(" ") : [n]; o < 4; o++) {
            i[t + rt[o] + e] = r[o] || r[o - 2] || r[0];
          }

          return i;
        }
      }, "margin" !== t && (C.cssHooks[t + e].set = Kt);
    }), C.fn.extend({
      css: function css(t, e) {
        return X(this, function (t, e, n) {
          var o,
              i,
              r = {},
              s = 0;

          if (Array.isArray(e)) {
            for (o = Rt(t), i = e.length; s < i; s++) {
              r[e[s]] = C.css(t, e[s], !1, o);
            }

            return r;
          }

          return void 0 !== n ? C.style(t, e, n) : C.css(t, e);
        }, t, e, arguments.length > 1);
      }
    }), C.Tween = te, te.prototype = {
      constructor: te,
      init: function init(t, e, n, o, i, r) {
        this.elem = t, this.prop = n, this.easing = i || C.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = o, this.unit = r || (C.cssNumber[n] ? "" : "px");
      },
      cur: function cur() {
        var t = te.propHooks[this.prop];
        return t && t.get ? t.get(this) : te.propHooks._default.get(this);
      },
      run: function run(t) {
        var e,
            n = te.propHooks[this.prop];
        return this.options.duration ? this.pos = e = C.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : te.propHooks._default.set(this), this;
      }
    }, te.prototype.init.prototype = te.prototype, te.propHooks = {
      _default: {
        get: function get(t) {
          var e;
          return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = C.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0;
        },
        set: function set(t) {
          C.fx.step[t.prop] ? C.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[C.cssProps[t.prop]] && !C.cssHooks[t.prop] ? t.elem[t.prop] = t.now : C.style(t.elem, t.prop, t.now + t.unit);
        }
      }
    }, te.propHooks.scrollTop = te.propHooks.scrollLeft = {
      set: function set(t) {
        t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
      }
    }, C.easing = {
      linear: function linear(t) {
        return t;
      },
      swing: function swing(t) {
        return .5 - Math.cos(t * Math.PI) / 2;
      },
      _default: "swing"
    }, C.fx = te.prototype.init, C.fx.step = {};
    var ee,
        ne,
        oe = /^(?:toggle|show|hide)$/,
        ie = /queueHooks$/;

    function re() {
      ne && (!1 === s.hidden && n.requestAnimationFrame ? n.requestAnimationFrame(re) : n.setTimeout(re, C.fx.interval), C.fx.tick());
    }

    function se() {
      return n.setTimeout(function () {
        ee = void 0;
      }), ee = Date.now();
    }

    function ae(t, e) {
      var n,
          o = 0,
          i = {
        height: t
      };

      for (e = e ? 1 : 0; o < 4; o += 2 - e) {
        i["margin" + (n = rt[o])] = i["padding" + n] = t;
      }

      return e && (i.opacity = i.width = t), i;
    }

    function ce(t, e, n) {
      for (var o, i = (le.tweeners[e] || []).concat(le.tweeners["*"]), r = 0, s = i.length; r < s; r++) {
        if (o = i[r].call(n, e, t)) return o;
      }
    }

    function le(t, e, n) {
      var o,
          i,
          r = 0,
          s = le.prefilters.length,
          a = C.Deferred().always(function () {
        delete c.elem;
      }),
          c = function c() {
        if (i) return !1;

        for (var e = ee || se(), n = Math.max(0, l.startTime + l.duration - e), o = 1 - (n / l.duration || 0), r = 0, s = l.tweens.length; r < s; r++) {
          l.tweens[r].run(o);
        }

        return a.notifyWith(t, [l, o, n]), o < 1 && s ? n : (s || a.notifyWith(t, [l, 1, 0]), a.resolveWith(t, [l]), !1);
      },
          l = a.promise({
        elem: t,
        props: C.extend({}, e),
        opts: C.extend(!0, {
          specialEasing: {},
          easing: C.easing._default
        }, n),
        originalProperties: e,
        originalOptions: n,
        startTime: ee || se(),
        duration: n.duration,
        tweens: [],
        createTween: function createTween(e, n) {
          var o = C.Tween(t, l.opts, e, n, l.opts.specialEasing[e] || l.opts.easing);
          return l.tweens.push(o), o;
        },
        stop: function stop(e) {
          var n = 0,
              o = e ? l.tweens.length : 0;
          if (i) return this;

          for (i = !0; n < o; n++) {
            l.tweens[n].run(1);
          }

          return e ? (a.notifyWith(t, [l, 1, 0]), a.resolveWith(t, [l, e])) : a.rejectWith(t, [l, e]), this;
        }
      }),
          u = l.props;

      for (!function (t, e) {
        var n, o, i, r, s;

        for (n in t) {
          if (i = e[o = Z(n)], r = t[n], Array.isArray(r) && (i = r[1], r = t[n] = r[0]), n !== o && (t[o] = r, delete t[n]), (s = C.cssHooks[o]) && ("expand" in s)) for (n in r = s.expand(r), delete t[o], r) {
            (n in t) || (t[n] = r[n], e[n] = i);
          } else e[o] = i;
        }
      }(u, l.opts.specialEasing); r < s; r++) {
        if (o = le.prefilters[r].call(l, t, u, l.opts)) return y(o.stop) && (C._queueHooks(l.elem, l.opts.queue).stop = o.stop.bind(o)), o;
      }

      return C.map(u, ce, l), y(l.opts.start) && l.opts.start.call(t, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), C.fx.timer(C.extend(c, {
        elem: t,
        anim: l,
        queue: l.opts.queue
      })), l;
    }

    C.Animation = C.extend(le, {
      tweeners: {
        "*": [function (t, e) {
          var n = this.createTween(t, e);
          return ct(n.elem, t, it.exec(e), n), n;
        }]
      },
      tweener: function tweener(t, e) {
        y(t) ? (e = t, t = ["*"]) : t = t.match(F);

        for (var n, o = 0, i = t.length; o < i; o++) {
          n = t[o], le.tweeners[n] = le.tweeners[n] || [], le.tweeners[n].unshift(e);
        }
      },
      prefilters: [function (t, e, n) {
        var o,
            i,
            r,
            s,
            a,
            c,
            l,
            u,
            f = "width" in e || "height" in e,
            d = this,
            p = {},
            h = t.style,
            g = t.nodeType && st(t),
            m = Q.get(t, "fxshow");

        for (o in n.queue || (null == (s = C._queueHooks(t, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
          s.unqueued || a();
        }), s.unqueued++, d.always(function () {
          d.always(function () {
            s.unqueued--, C.queue(t, "fx").length || s.empty.fire();
          });
        })), e) {
          if (i = e[o], oe.test(i)) {
            if (delete e[o], r = r || "toggle" === i, i === (g ? "hide" : "show")) {
              if ("show" !== i || !m || void 0 === m[o]) continue;
              g = !0;
            }

            p[o] = m && m[o] || C.style(t, o);
          }
        }

        if ((c = !C.isEmptyObject(e)) || !C.isEmptyObject(p)) for (o in f && 1 === t.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = m && m.display) && (l = Q.get(t, "display")), "none" === (u = C.css(t, "display")) && (l ? u = l : (ft([t], !0), l = t.style.display || l, u = C.css(t, "display"), ft([t]))), ("inline" === u || "inline-block" === u && null != l) && "none" === C.css(t, "float") && (c || (d.done(function () {
          h.display = l;
        }), null == l && (u = h.display, l = "none" === u ? "" : u)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", d.always(function () {
          h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2];
        })), c = !1, p) {
          c || (m ? "hidden" in m && (g = m.hidden) : m = Q.access(t, "fxshow", {
            display: l
          }), r && (m.hidden = !g), g && ft([t], !0), d.done(function () {
            for (o in g || ft([t]), Q.remove(t, "fxshow"), p) {
              C.style(t, o, p[o]);
            }
          })), c = ce(g ? m[o] : 0, o, d), o in m || (m[o] = c.start, g && (c.end = c.start, c.start = 0));
        }
      }],
      prefilter: function prefilter(t, e) {
        e ? le.prefilters.unshift(t) : le.prefilters.push(t);
      }
    }), C.speed = function (t, e, n) {
      var o = t && "object" == _typeof(t) ? C.extend({}, t) : {
        complete: n || !n && e || y(t) && t,
        duration: t,
        easing: n && e || e && !y(e) && e
      };
      return C.fx.off ? o.duration = 0 : "number" != typeof o.duration && (o.duration in C.fx.speeds ? o.duration = C.fx.speeds[o.duration] : o.duration = C.fx.speeds._default), null != o.queue && !0 !== o.queue || (o.queue = "fx"), o.old = o.complete, o.complete = function () {
        y(o.old) && o.old.call(this), o.queue && C.dequeue(this, o.queue);
      }, o;
    }, C.fn.extend({
      fadeTo: function fadeTo(t, e, n, o) {
        return this.filter(st).css("opacity", 0).show().end().animate({
          opacity: e
        }, t, n, o);
      },
      animate: function animate(t, e, n, o) {
        var i = C.isEmptyObject(t),
            r = C.speed(e, n, o),
            s = function s() {
          var e = le(this, C.extend({}, t), r);
          (i || Q.get(this, "finish")) && e.stop(!0);
        };

        return s.finish = s, i || !1 === r.queue ? this.each(s) : this.queue(r.queue, s);
      },
      stop: function stop(t, e, n) {
        var o = function o(t) {
          var e = t.stop;
          delete t.stop, e(n);
        };

        return "string" != typeof t && (n = e, e = t, t = void 0), e && !1 !== t && this.queue(t || "fx", []), this.each(function () {
          var e = !0,
              i = null != t && t + "queueHooks",
              r = C.timers,
              s = Q.get(this);
          if (i) s[i] && s[i].stop && o(s[i]);else for (i in s) {
            s[i] && s[i].stop && ie.test(i) && o(s[i]);
          }

          for (i = r.length; i--;) {
            r[i].elem !== this || null != t && r[i].queue !== t || (r[i].anim.stop(n), e = !1, r.splice(i, 1));
          }

          !e && n || C.dequeue(this, t);
        });
      },
      finish: function finish(t) {
        return !1 !== t && (t = t || "fx"), this.each(function () {
          var e,
              n = Q.get(this),
              o = n[t + "queue"],
              i = n[t + "queueHooks"],
              r = C.timers,
              s = o ? o.length : 0;

          for (n.finish = !0, C.queue(this, t, []), i && i.stop && i.stop.call(this, !0), e = r.length; e--;) {
            r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
          }

          for (e = 0; e < s; e++) {
            o[e] && o[e].finish && o[e].finish.call(this);
          }

          delete n.finish;
        });
      }
    }), C.each(["toggle", "show", "hide"], function (t, e) {
      var n = C.fn[e];

      C.fn[e] = function (t, o, i) {
        return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(ae(e, !0), t, o, i);
      };
    }), C.each({
      slideDown: ae("show"),
      slideUp: ae("hide"),
      slideToggle: ae("toggle"),
      fadeIn: {
        opacity: "show"
      },
      fadeOut: {
        opacity: "hide"
      },
      fadeToggle: {
        opacity: "toggle"
      }
    }, function (t, e) {
      C.fn[t] = function (t, n, o) {
        return this.animate(e, t, n, o);
      };
    }), C.timers = [], C.fx.tick = function () {
      var t,
          e = 0,
          n = C.timers;

      for (ee = Date.now(); e < n.length; e++) {
        (t = n[e])() || n[e] !== t || n.splice(e--, 1);
      }

      n.length || C.fx.stop(), ee = void 0;
    }, C.fx.timer = function (t) {
      C.timers.push(t), C.fx.start();
    }, C.fx.interval = 13, C.fx.start = function () {
      ne || (ne = !0, re());
    }, C.fx.stop = function () {
      ne = null;
    }, C.fx.speeds = {
      slow: 600,
      fast: 200,
      _default: 400
    }, C.fn.delay = function (t, e) {
      return t = C.fx && C.fx.speeds[t] || t, e = e || "fx", this.queue(e, function (e, o) {
        var i = n.setTimeout(e, t);

        o.stop = function () {
          n.clearTimeout(i);
        };
      });
    }, function () {
      var t = s.createElement("input"),
          e = s.createElement("select").appendChild(s.createElement("option"));
      t.type = "checkbox", v.checkOn = "" !== t.value, v.optSelected = e.selected, (t = s.createElement("input")).value = "t", t.type = "radio", v.radioValue = "t" === t.value;
    }();
    var ue,
        fe = C.expr.attrHandle;
    C.fn.extend({
      attr: function attr(t, e) {
        return X(this, C.attr, t, e, arguments.length > 1);
      },
      removeAttr: function removeAttr(t) {
        return this.each(function () {
          C.removeAttr(this, t);
        });
      }
    }), C.extend({
      attr: function attr(t, e, n) {
        var o,
            i,
            r = t.nodeType;
        if (3 !== r && 8 !== r && 2 !== r) return void 0 === t.getAttribute ? C.prop(t, e, n) : (1 === r && C.isXMLDoc(t) || (i = C.attrHooks[e.toLowerCase()] || (C.expr.match.bool.test(e) ? ue : void 0)), void 0 !== n ? null === n ? void C.removeAttr(t, e) : i && "set" in i && void 0 !== (o = i.set(t, n, e)) ? o : (t.setAttribute(e, n + ""), n) : i && "get" in i && null !== (o = i.get(t, e)) ? o : null == (o = C.find.attr(t, e)) ? void 0 : o);
      },
      attrHooks: {
        type: {
          set: function set(t, e) {
            if (!v.radioValue && "radio" === e && L(t, "input")) {
              var n = t.value;
              return t.setAttribute("type", e), n && (t.value = n), e;
            }
          }
        }
      },
      removeAttr: function removeAttr(t, e) {
        var n,
            o = 0,
            i = e && e.match(F);
        if (i && 1 === t.nodeType) for (; n = i[o++];) {
          t.removeAttribute(n);
        }
      }
    }), ue = {
      set: function set(t, e, n) {
        return !1 === e ? C.removeAttr(t, n) : t.setAttribute(n, n), n;
      }
    }, C.each(C.expr.match.bool.source.match(/\w+/g), function (t, e) {
      var n = fe[e] || C.find.attr;

      fe[e] = function (t, e, o) {
        var i,
            r,
            s = e.toLowerCase();
        return o || (r = fe[s], fe[s] = i, i = null != n(t, e, o) ? s : null, fe[s] = r), i;
      };
    });
    var de = /^(?:input|select|textarea|button)$/i,
        pe = /^(?:a|area)$/i;

    function he(t) {
      return (t.match(F) || []).join(" ");
    }

    function ge(t) {
      return t.getAttribute && t.getAttribute("class") || "";
    }

    function me(t) {
      return Array.isArray(t) ? t : "string" == typeof t && t.match(F) || [];
    }

    C.fn.extend({
      prop: function prop(t, e) {
        return X(this, C.prop, t, e, arguments.length > 1);
      },
      removeProp: function removeProp(t) {
        return this.each(function () {
          delete this[C.propFix[t] || t];
        });
      }
    }), C.extend({
      prop: function prop(t, e, n) {
        var o,
            i,
            r = t.nodeType;
        if (3 !== r && 8 !== r && 2 !== r) return 1 === r && C.isXMLDoc(t) || (e = C.propFix[e] || e, i = C.propHooks[e]), void 0 !== n ? i && "set" in i && void 0 !== (o = i.set(t, n, e)) ? o : t[e] = n : i && "get" in i && null !== (o = i.get(t, e)) ? o : t[e];
      },
      propHooks: {
        tabIndex: {
          get: function get(t) {
            var e = C.find.attr(t, "tabindex");
            return e ? parseInt(e, 10) : de.test(t.nodeName) || pe.test(t.nodeName) && t.href ? 0 : -1;
          }
        }
      },
      propFix: {
        for: "htmlFor",
        class: "className"
      }
    }), v.optSelected || (C.propHooks.selected = {
      get: function get(t) {
        var e = t.parentNode;
        return e && e.parentNode && e.parentNode.selectedIndex, null;
      },
      set: function set(t) {
        var e = t.parentNode;
        e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex);
      }
    }), C.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
      C.propFix[this.toLowerCase()] = this;
    }), C.fn.extend({
      addClass: function addClass(t) {
        var e,
            n,
            o,
            i,
            r,
            s,
            a,
            c = 0;
        if (y(t)) return this.each(function (e) {
          C(this).addClass(t.call(this, e, ge(this)));
        });
        if ((e = me(t)).length) for (; n = this[c++];) {
          if (i = ge(n), o = 1 === n.nodeType && " " + he(i) + " ") {
            for (s = 0; r = e[s++];) {
              o.indexOf(" " + r + " ") < 0 && (o += r + " ");
            }

            i !== (a = he(o)) && n.setAttribute("class", a);
          }
        }
        return this;
      },
      removeClass: function removeClass(t) {
        var e,
            n,
            o,
            i,
            r,
            s,
            a,
            c = 0;
        if (y(t)) return this.each(function (e) {
          C(this).removeClass(t.call(this, e, ge(this)));
        });
        if (!arguments.length) return this.attr("class", "");
        if ((e = me(t)).length) for (; n = this[c++];) {
          if (i = ge(n), o = 1 === n.nodeType && " " + he(i) + " ") {
            for (s = 0; r = e[s++];) {
              for (; o.indexOf(" " + r + " ") > -1;) {
                o = o.replace(" " + r + " ", " ");
              }
            }

            i !== (a = he(o)) && n.setAttribute("class", a);
          }
        }
        return this;
      },
      toggleClass: function toggleClass(t, e) {
        var n = _typeof(t),
            o = "string" === n || Array.isArray(t);

        return "boolean" == typeof e && o ? e ? this.addClass(t) : this.removeClass(t) : y(t) ? this.each(function (n) {
          C(this).toggleClass(t.call(this, n, ge(this), e), e);
        }) : this.each(function () {
          var e, i, r, s;
          if (o) for (i = 0, r = C(this), s = me(t); e = s[i++];) {
            r.hasClass(e) ? r.removeClass(e) : r.addClass(e);
          } else void 0 !== t && "boolean" !== n || ((e = ge(this)) && Q.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === t ? "" : Q.get(this, "__className__") || ""));
        });
      },
      hasClass: function hasClass(t) {
        var e,
            n,
            o = 0;

        for (e = " " + t + " "; n = this[o++];) {
          if (1 === n.nodeType && (" " + he(ge(n)) + " ").indexOf(e) > -1) return !0;
        }

        return !1;
      }
    });
    var ve = /\r/g;
    C.fn.extend({
      val: function val(t) {
        var e,
            n,
            o,
            i = this[0];
        return arguments.length ? (o = y(t), this.each(function (n) {
          var i;
          1 === this.nodeType && (null == (i = o ? t.call(this, n, C(this).val()) : t) ? i = "" : "number" == typeof i ? i += "" : Array.isArray(i) && (i = C.map(i, function (t) {
            return null == t ? "" : t + "";
          })), (e = C.valHooks[this.type] || C.valHooks[this.nodeName.toLowerCase()]) && "set" in e && void 0 !== e.set(this, i, "value") || (this.value = i));
        })) : i ? (e = C.valHooks[i.type] || C.valHooks[i.nodeName.toLowerCase()]) && "get" in e && void 0 !== (n = e.get(i, "value")) ? n : "string" == typeof (n = i.value) ? n.replace(ve, "") : null == n ? "" : n : void 0;
      }
    }), C.extend({
      valHooks: {
        option: {
          get: function get(t) {
            var e = C.find.attr(t, "value");
            return null != e ? e : he(C.text(t));
          }
        },
        select: {
          get: function get(t) {
            var e,
                n,
                o,
                i = t.options,
                r = t.selectedIndex,
                s = "select-one" === t.type,
                a = s ? null : [],
                c = s ? r + 1 : i.length;

            for (o = r < 0 ? c : s ? r : 0; o < c; o++) {
              if (((n = i[o]).selected || o === r) && !n.disabled && (!n.parentNode.disabled || !L(n.parentNode, "optgroup"))) {
                if (e = C(n).val(), s) return e;
                a.push(e);
              }
            }

            return a;
          },
          set: function set(t, e) {
            for (var n, o, i = t.options, r = C.makeArray(e), s = i.length; s--;) {
              ((o = i[s]).selected = C.inArray(C.valHooks.option.get(o), r) > -1) && (n = !0);
            }

            return n || (t.selectedIndex = -1), r;
          }
        }
      }
    }), C.each(["radio", "checkbox"], function () {
      C.valHooks[this] = {
        set: function set(t, e) {
          if (Array.isArray(e)) return t.checked = C.inArray(C(t).val(), e) > -1;
        }
      }, v.checkOn || (C.valHooks[this].get = function (t) {
        return null === t.getAttribute("value") ? "on" : t.value;
      });
    }), v.focusin = "onfocusin" in n;

    var ye = /^(?:focusinfocus|focusoutblur)$/,
        be = function be(t) {
      t.stopPropagation();
    };

    C.extend(C.event, {
      trigger: function trigger(t, e, o, i) {
        var r,
            a,
            c,
            l,
            u,
            f,
            d,
            p,
            g = [o || s],
            m = h.call(t, "type") ? t.type : t,
            v = h.call(t, "namespace") ? t.namespace.split(".") : [];

        if (a = p = c = o = o || s, 3 !== o.nodeType && 8 !== o.nodeType && !ye.test(m + C.event.triggered) && (m.indexOf(".") > -1 && (m = (v = m.split(".")).shift(), v.sort()), u = m.indexOf(":") < 0 && "on" + m, (t = t[C.expando] ? t : new C.Event(m, "object" == _typeof(t) && t)).isTrigger = i ? 2 : 3, t.namespace = v.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = o), e = null == e ? [t] : C.makeArray(e, [t]), d = C.event.special[m] || {}, i || !d.trigger || !1 !== d.trigger.apply(o, e))) {
          if (!i && !d.noBubble && !b(o)) {
            for (l = d.delegateType || m, ye.test(l + m) || (a = a.parentNode); a; a = a.parentNode) {
              g.push(a), c = a;
            }

            c === (o.ownerDocument || s) && g.push(c.defaultView || c.parentWindow || n);
          }

          for (r = 0; (a = g[r++]) && !t.isPropagationStopped();) {
            p = a, t.type = r > 1 ? l : d.bindType || m, (f = (Q.get(a, "events") || {})[t.type] && Q.get(a, "handle")) && f.apply(a, e), (f = u && a[u]) && f.apply && G(a) && (t.result = f.apply(a, e), !1 === t.result && t.preventDefault());
          }

          return t.type = m, i || t.isDefaultPrevented() || d._default && !1 !== d._default.apply(g.pop(), e) || !G(o) || u && y(o[m]) && !b(o) && ((c = o[u]) && (o[u] = null), C.event.triggered = m, t.isPropagationStopped() && p.addEventListener(m, be), o[m](), t.isPropagationStopped() && p.removeEventListener(m, be), C.event.triggered = void 0, c && (o[u] = c)), t.result;
        }
      },
      simulate: function simulate(t, e, n) {
        var o = C.extend(new C.Event(), n, {
          type: t,
          isSimulated: !0
        });
        C.event.trigger(o, null, e);
      }
    }), C.fn.extend({
      trigger: function trigger(t, e) {
        return this.each(function () {
          C.event.trigger(t, e, this);
        });
      },
      triggerHandler: function triggerHandler(t, e) {
        var n = this[0];
        if (n) return C.event.trigger(t, e, n, !0);
      }
    }), v.focusin || C.each({
      focus: "focusin",
      blur: "focusout"
    }, function (t, e) {
      var n = function n(t) {
        C.event.simulate(e, t.target, C.event.fix(t));
      };

      C.event.special[e] = {
        setup: function setup() {
          var o = this.ownerDocument || this,
              i = Q.access(o, e);
          i || o.addEventListener(t, n, !0), Q.access(o, e, (i || 0) + 1);
        },
        teardown: function teardown() {
          var o = this.ownerDocument || this,
              i = Q.access(o, e) - 1;
          i ? Q.access(o, e, i) : (o.removeEventListener(t, n, !0), Q.remove(o, e));
        }
      };
    });
    var xe = n.location,
        we = Date.now(),
        Te = /\?/;

    C.parseXML = function (t) {
      var e;
      if (!t || "string" != typeof t) return null;

      try {
        e = new n.DOMParser().parseFromString(t, "text/xml");
      } catch (t) {
        e = void 0;
      }

      return e && !e.getElementsByTagName("parsererror").length || C.error("Invalid XML: " + t), e;
    };

    var Ce = /\[\]$/,
        Se = /\r?\n/g,
        $e = /^(?:submit|button|image|reset|file)$/i,
        Ee = /^(?:input|select|textarea|keygen)/i;

    function Pe(t, e, n, o) {
      var i;
      if (Array.isArray(e)) C.each(e, function (e, i) {
        n || Ce.test(t) ? o(t, i) : Pe(t + "[" + ("object" == _typeof(i) && null != i ? e : "") + "]", i, n, o);
      });else if (n || "object" !== T(e)) o(t, e);else for (i in e) {
        Pe(t + "[" + i + "]", e[i], n, o);
      }
    }

    C.param = function (t, e) {
      var n,
          o = [],
          i = function i(t, e) {
        var n = y(e) ? e() : e;
        o[o.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == n ? "" : n);
      };

      if (Array.isArray(t) || t.jquery && !C.isPlainObject(t)) C.each(t, function () {
        i(this.name, this.value);
      });else for (n in t) {
        Pe(n, t[n], e, i);
      }
      return o.join("&");
    }, C.fn.extend({
      serialize: function serialize() {
        return C.param(this.serializeArray());
      },
      serializeArray: function serializeArray() {
        return this.map(function () {
          var t = C.prop(this, "elements");
          return t ? C.makeArray(t) : this;
        }).filter(function () {
          var t = this.type;
          return this.name && !C(this).is(":disabled") && Ee.test(this.nodeName) && !$e.test(t) && (this.checked || !dt.test(t));
        }).map(function (t, e) {
          var n = C(this).val();
          return null == n ? null : Array.isArray(n) ? C.map(n, function (t) {
            return {
              name: e.name,
              value: t.replace(Se, "\r\n")
            };
          }) : {
            name: e.name,
            value: n.replace(Se, "\r\n")
          };
        }).get();
      }
    });
    var ke = /%20/g,
        Ae = /#.*$/,
        Le = /([?&])_=[^&]*/,
        De = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        je = /^(?:GET|HEAD)$/,
        Me = /^\/\//,
        Ne = {},
        He = {},
        qe = "*/".concat("*"),
        Ie = s.createElement("a");

    function Fe(t) {
      return function (e, n) {
        "string" != typeof e && (n = e, e = "*");
        var o,
            i = 0,
            r = e.toLowerCase().match(F) || [];
        if (y(n)) for (; o = r[i++];) {
          "+" === o[0] ? (o = o.slice(1) || "*", (t[o] = t[o] || []).unshift(n)) : (t[o] = t[o] || []).push(n);
        }
      };
    }

    function Oe(t, e, n, o) {
      var i = {},
          r = t === He;

      function s(a) {
        var c;
        return i[a] = !0, C.each(t[a] || [], function (t, a) {
          var l = a(e, n, o);
          return "string" != typeof l || r || i[l] ? r ? !(c = l) : void 0 : (e.dataTypes.unshift(l), s(l), !1);
        }), c;
      }

      return s(e.dataTypes[0]) || !i["*"] && s("*");
    }

    function Re(t, e) {
      var n,
          o,
          i = C.ajaxSettings.flatOptions || {};

      for (n in e) {
        void 0 !== e[n] && ((i[n] ? t : o || (o = {}))[n] = e[n]);
      }

      return o && C.extend(!0, t, o), t;
    }

    Ie.href = xe.href, C.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: xe.href,
        type: "GET",
        isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(xe.protocol),
        global: !0,
        processData: !0,
        async: !0,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        accepts: {
          "*": qe,
          text: "text/plain",
          html: "text/html",
          xml: "application/xml, text/xml",
          json: "application/json, text/javascript"
        },
        contents: {
          xml: /\bxml\b/,
          html: /\bhtml/,
          json: /\bjson\b/
        },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
          json: "responseJSON"
        },
        converters: {
          "* text": String,
          "text html": !0,
          "text json": JSON.parse,
          "text xml": C.parseXML
        },
        flatOptions: {
          url: !0,
          context: !0
        }
      },
      ajaxSetup: function ajaxSetup(t, e) {
        return e ? Re(Re(t, C.ajaxSettings), e) : Re(C.ajaxSettings, t);
      },
      ajaxPrefilter: Fe(Ne),
      ajaxTransport: Fe(He),
      ajax: function ajax(t, e) {
        "object" == _typeof(t) && (e = t, t = void 0), e = e || {};
        var o,
            i,
            r,
            a,
            c,
            l,
            u,
            f,
            d,
            p,
            h = C.ajaxSetup({}, e),
            g = h.context || h,
            m = h.context && (g.nodeType || g.jquery) ? C(g) : C.event,
            v = C.Deferred(),
            y = C.Callbacks("once memory"),
            b = h.statusCode || {},
            x = {},
            w = {},
            T = "canceled",
            S = {
          readyState: 0,
          getResponseHeader: function getResponseHeader(t) {
            var e;

            if (u) {
              if (!a) for (a = {}; e = De.exec(r);) {
                a[e[1].toLowerCase()] = e[2];
              }
              e = a[t.toLowerCase()];
            }

            return null == e ? null : e;
          },
          getAllResponseHeaders: function getAllResponseHeaders() {
            return u ? r : null;
          },
          setRequestHeader: function setRequestHeader(t, e) {
            return null == u && (t = w[t.toLowerCase()] = w[t.toLowerCase()] || t, x[t] = e), this;
          },
          overrideMimeType: function overrideMimeType(t) {
            return null == u && (h.mimeType = t), this;
          },
          statusCode: function statusCode(t) {
            var e;
            if (t) if (u) S.always(t[S.status]);else for (e in t) {
              b[e] = [b[e], t[e]];
            }
            return this;
          },
          abort: function abort(t) {
            var e = t || T;
            return o && o.abort(e), $(0, e), this;
          }
        };

        if (v.promise(S), h.url = ((t || h.url || xe.href) + "").replace(Me, xe.protocol + "//"), h.type = e.method || e.type || h.method || h.type, h.dataTypes = (h.dataType || "*").toLowerCase().match(F) || [""], null == h.crossDomain) {
          l = s.createElement("a");

          try {
            l.href = h.url, l.href = l.href, h.crossDomain = Ie.protocol + "//" + Ie.host != l.protocol + "//" + l.host;
          } catch (t) {
            h.crossDomain = !0;
          }
        }

        if (h.data && h.processData && "string" != typeof h.data && (h.data = C.param(h.data, h.traditional)), Oe(Ne, h, e, S), u) return S;

        for (d in (f = C.event && h.global) && 0 == C.active++ && C.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !je.test(h.type), i = h.url.replace(Ae, ""), h.hasContent ? h.data && h.processData && 0 === (h.contentType || "").indexOf("application/x-www-form-urlencoded") && (h.data = h.data.replace(ke, "+")) : (p = h.url.slice(i.length), h.data && (h.processData || "string" == typeof h.data) && (i += (Te.test(i) ? "&" : "?") + h.data, delete h.data), !1 === h.cache && (i = i.replace(Le, "$1"), p = (Te.test(i) ? "&" : "?") + "_=" + we++ + p), h.url = i + p), h.ifModified && (C.lastModified[i] && S.setRequestHeader("If-Modified-Since", C.lastModified[i]), C.etag[i] && S.setRequestHeader("If-None-Match", C.etag[i])), (h.data && h.hasContent && !1 !== h.contentType || e.contentType) && S.setRequestHeader("Content-Type", h.contentType), S.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + qe + "; q=0.01" : "") : h.accepts["*"]), h.headers) {
          S.setRequestHeader(d, h.headers[d]);
        }

        if (h.beforeSend && (!1 === h.beforeSend.call(g, S, h) || u)) return S.abort();

        if (T = "abort", y.add(h.complete), S.done(h.success), S.fail(h.error), o = Oe(He, h, e, S)) {
          if (S.readyState = 1, f && m.trigger("ajaxSend", [S, h]), u) return S;
          h.async && h.timeout > 0 && (c = n.setTimeout(function () {
            S.abort("timeout");
          }, h.timeout));

          try {
            u = !1, o.send(x, $);
          } catch (t) {
            if (u) throw t;
            $(-1, t);
          }
        } else $(-1, "No Transport");

        function $(t, e, s, a) {
          var l,
              d,
              p,
              x,
              w,
              T = e;
          u || (u = !0, c && n.clearTimeout(c), o = void 0, r = a || "", S.readyState = t > 0 ? 4 : 0, l = t >= 200 && t < 300 || 304 === t, s && (x = function (t, e, n) {
            for (var o, i, r, s, a = t.contents, c = t.dataTypes; "*" === c[0];) {
              c.shift(), void 0 === o && (o = t.mimeType || e.getResponseHeader("Content-Type"));
            }

            if (o) for (i in a) {
              if (a[i] && a[i].test(o)) {
                c.unshift(i);
                break;
              }
            }
            if (c[0] in n) r = c[0];else {
              for (i in n) {
                if (!c[0] || t.converters[i + " " + c[0]]) {
                  r = i;
                  break;
                }

                s || (s = i);
              }

              r = r || s;
            }
            if (r) return r !== c[0] && c.unshift(r), n[r];
          }(h, S, s)), x = function (t, e, n, o) {
            var i,
                r,
                s,
                a,
                c,
                l = {},
                u = t.dataTypes.slice();
            if (u[1]) for (s in t.converters) {
              l[s.toLowerCase()] = t.converters[s];
            }

            for (r = u.shift(); r;) {
              if (t.responseFields[r] && (n[t.responseFields[r]] = e), !c && o && t.dataFilter && (e = t.dataFilter(e, t.dataType)), c = r, r = u.shift()) if ("*" === r) r = c;else if ("*" !== c && c !== r) {
                if (!(s = l[c + " " + r] || l["* " + r])) for (i in l) {
                  if ((a = i.split(" "))[1] === r && (s = l[c + " " + a[0]] || l["* " + a[0]])) {
                    !0 === s ? s = l[i] : !0 !== l[i] && (r = a[0], u.unshift(a[1]));
                    break;
                  }
                }
                if (!0 !== s) if (s && t.throws) e = s(e);else try {
                  e = s(e);
                } catch (t) {
                  return {
                    state: "parsererror",
                    error: s ? t : "No conversion from " + c + " to " + r
                  };
                }
              }
            }

            return {
              state: "success",
              data: e
            };
          }(h, x, S, l), l ? (h.ifModified && ((w = S.getResponseHeader("Last-Modified")) && (C.lastModified[i] = w), (w = S.getResponseHeader("etag")) && (C.etag[i] = w)), 204 === t || "HEAD" === h.type ? T = "nocontent" : 304 === t ? T = "notmodified" : (T = x.state, d = x.data, l = !(p = x.error))) : (p = T, !t && T || (T = "error", t < 0 && (t = 0))), S.status = t, S.statusText = (e || T) + "", l ? v.resolveWith(g, [d, T, S]) : v.rejectWith(g, [S, T, p]), S.statusCode(b), b = void 0, f && m.trigger(l ? "ajaxSuccess" : "ajaxError", [S, h, l ? d : p]), y.fireWith(g, [S, T]), f && (m.trigger("ajaxComplete", [S, h]), --C.active || C.event.trigger("ajaxStop")));
        }

        return S;
      },
      getJSON: function getJSON(t, e, n) {
        return C.get(t, e, n, "json");
      },
      getScript: function getScript(t, e) {
        return C.get(t, void 0, e, "script");
      }
    }), C.each(["get", "post"], function (t, e) {
      C[e] = function (t, n, o, i) {
        return y(n) && (i = i || o, o = n, n = void 0), C.ajax(C.extend({
          url: t,
          type: e,
          dataType: i,
          data: n,
          success: o
        }, C.isPlainObject(t) && t));
      };
    }), C._evalUrl = function (t) {
      return C.ajax({
        url: t,
        type: "GET",
        dataType: "script",
        cache: !0,
        async: !1,
        global: !1,
        throws: !0
      });
    }, C.fn.extend({
      wrapAll: function wrapAll(t) {
        var e;
        return this[0] && (y(t) && (t = t.call(this[0])), e = C(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
          for (var t = this; t.firstElementChild;) {
            t = t.firstElementChild;
          }

          return t;
        }).append(this)), this;
      },
      wrapInner: function wrapInner(t) {
        return y(t) ? this.each(function (e) {
          C(this).wrapInner(t.call(this, e));
        }) : this.each(function () {
          var e = C(this),
              n = e.contents();
          n.length ? n.wrapAll(t) : e.append(t);
        });
      },
      wrap: function wrap(t) {
        var e = y(t);
        return this.each(function (n) {
          C(this).wrapAll(e ? t.call(this, n) : t);
        });
      },
      unwrap: function unwrap(t) {
        return this.parent(t).not("body").each(function () {
          C(this).replaceWith(this.childNodes);
        }), this;
      }
    }), C.expr.pseudos.hidden = function (t) {
      return !C.expr.pseudos.visible(t);
    }, C.expr.pseudos.visible = function (t) {
      return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length);
    }, C.ajaxSettings.xhr = function () {
      try {
        return new n.XMLHttpRequest();
      } catch (t) {}
    };
    var ze = {
      0: 200,
      1223: 204
    },
        Be = C.ajaxSettings.xhr();
    v.cors = !!Be && "withCredentials" in Be, v.ajax = Be = !!Be, C.ajaxTransport(function (t) {
      var _e3, o;

      if (v.cors || Be && !t.crossDomain) return {
        send: function send(i, r) {
          var s,
              a = t.xhr();
          if (a.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields) for (s in t.xhrFields) {
            a[s] = t.xhrFields[s];
          }

          for (s in t.mimeType && a.overrideMimeType && a.overrideMimeType(t.mimeType), t.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest"), i) {
            a.setRequestHeader(s, i[s]);
          }

          _e3 = function e(t) {
            return function () {
              _e3 && (_e3 = o = a.onload = a.onerror = a.onabort = a.ontimeout = a.onreadystatechange = null, "abort" === t ? a.abort() : "error" === t ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(ze[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                binary: a.response
              } : {
                text: a.responseText
              }, a.getAllResponseHeaders()));
            };
          }, a.onload = _e3(), o = a.onerror = a.ontimeout = _e3("error"), void 0 !== a.onabort ? a.onabort = o : a.onreadystatechange = function () {
            4 === a.readyState && n.setTimeout(function () {
              _e3 && o();
            });
          }, _e3 = _e3("abort");

          try {
            a.send(t.hasContent && t.data || null);
          } catch (t) {
            if (_e3) throw t;
          }
        },
        abort: function abort() {
          _e3 && _e3();
        }
      };
    }), C.ajaxPrefilter(function (t) {
      t.crossDomain && (t.contents.script = !1);
    }), C.ajaxSetup({
      accepts: {
        script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
      },
      contents: {
        script: /\b(?:java|ecma)script\b/
      },
      converters: {
        "text script": function textScript(t) {
          return C.globalEval(t), t;
        }
      }
    }), C.ajaxPrefilter("script", function (t) {
      void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET");
    }), C.ajaxTransport("script", function (t) {
      var e, _n;

      if (t.crossDomain) return {
        send: function send(o, i) {
          e = C("<script>").prop({
            charset: t.scriptCharset,
            src: t.url
          }).on("load error", _n = function n(t) {
            e.remove(), _n = null, t && i("error" === t.type ? 404 : 200, t.type);
          }), s.head.appendChild(e[0]);
        },
        abort: function abort() {
          _n && _n();
        }
      };
    });
    var _e = [],
        We = /(=)\?(?=&|$)|\?\?/;
    C.ajaxSetup({
      jsonp: "callback",
      jsonpCallback: function jsonpCallback() {
        var t = _e.pop() || C.expando + "_" + we++;
        return this[t] = !0, t;
      }
    }), C.ajaxPrefilter("json jsonp", function (t, e, o) {
      var i,
          r,
          s,
          a = !1 !== t.jsonp && (We.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && We.test(t.data) && "data");
      if (a || "jsonp" === t.dataTypes[0]) return i = t.jsonpCallback = y(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(We, "$1" + i) : !1 !== t.jsonp && (t.url += (Te.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function () {
        return s || C.error(i + " was not called"), s[0];
      }, t.dataTypes[0] = "json", r = n[i], n[i] = function () {
        s = arguments;
      }, o.always(function () {
        void 0 === r ? C(n).removeProp(i) : n[i] = r, t[i] && (t.jsonpCallback = e.jsonpCallback, _e.push(i)), s && y(r) && r(s[0]), s = r = void 0;
      }), "script";
    }), v.createHTMLDocument = function () {
      var t = s.implementation.createHTMLDocument("").body;
      return t.innerHTML = "<form></form><form></form>", 2 === t.childNodes.length;
    }(), C.parseHTML = function (t, e, n) {
      return "string" != typeof t ? [] : ("boolean" == typeof e && (n = e, e = !1), e || (v.createHTMLDocument ? ((o = (e = s.implementation.createHTMLDocument("")).createElement("base")).href = s.location.href, e.head.appendChild(o)) : e = s), i = D.exec(t), r = !n && [], i ? [e.createElement(i[1])] : (i = bt([t], e, r), r && r.length && C(r).remove(), C.merge([], i.childNodes)));
      var o, i, r;
    }, C.fn.load = function (t, e, n) {
      var o,
          i,
          r,
          s = this,
          a = t.indexOf(" ");
      return a > -1 && (o = he(t.slice(a)), t = t.slice(0, a)), y(e) ? (n = e, e = void 0) : e && "object" == _typeof(e) && (i = "POST"), s.length > 0 && C.ajax({
        url: t,
        type: i || "GET",
        dataType: "html",
        data: e
      }).done(function (t) {
        r = arguments, s.html(o ? C("<div>").append(C.parseHTML(t)).find(o) : t);
      }).always(n && function (t, e) {
        s.each(function () {
          n.apply(this, r || [t.responseText, e, t]);
        });
      }), this;
    }, C.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
      C.fn[e] = function (t) {
        return this.on(e, t);
      };
    }), C.expr.pseudos.animated = function (t) {
      return C.grep(C.timers, function (e) {
        return t === e.elem;
      }).length;
    }, C.offset = {
      setOffset: function setOffset(t, e, n) {
        var o,
            i,
            r,
            s,
            a,
            c,
            l = C.css(t, "position"),
            u = C(t),
            f = {};
        "static" === l && (t.style.position = "relative"), a = u.offset(), r = C.css(t, "top"), c = C.css(t, "left"), ("absolute" === l || "fixed" === l) && (r + c).indexOf("auto") > -1 ? (s = (o = u.position()).top, i = o.left) : (s = parseFloat(r) || 0, i = parseFloat(c) || 0), y(e) && (e = e.call(t, n, C.extend({}, a))), null != e.top && (f.top = e.top - a.top + s), null != e.left && (f.left = e.left - a.left + i), "using" in e ? e.using.call(t, f) : u.css(f);
      }
    }, C.fn.extend({
      offset: function offset(t) {
        if (arguments.length) return void 0 === t ? this : this.each(function (e) {
          C.offset.setOffset(this, t, e);
        });
        var e,
            n,
            o = this[0];
        return o ? o.getClientRects().length ? (e = o.getBoundingClientRect(), n = o.ownerDocument.defaultView, {
          top: e.top + n.pageYOffset,
          left: e.left + n.pageXOffset
        }) : {
          top: 0,
          left: 0
        } : void 0;
      },
      position: function position() {
        if (this[0]) {
          var t,
              e,
              n,
              o = this[0],
              i = {
            top: 0,
            left: 0
          };
          if ("fixed" === C.css(o, "position")) e = o.getBoundingClientRect();else {
            for (e = this.offset(), n = o.ownerDocument, t = o.offsetParent || n.documentElement; t && (t === n.body || t === n.documentElement) && "static" === C.css(t, "position");) {
              t = t.parentNode;
            }

            t && t !== o && 1 === t.nodeType && ((i = C(t).offset()).top += C.css(t, "borderTopWidth", !0), i.left += C.css(t, "borderLeftWidth", !0));
          }
          return {
            top: e.top - i.top - C.css(o, "marginTop", !0),
            left: e.left - i.left - C.css(o, "marginLeft", !0)
          };
        }
      },
      offsetParent: function offsetParent() {
        return this.map(function () {
          for (var t = this.offsetParent; t && "static" === C.css(t, "position");) {
            t = t.offsetParent;
          }

          return t || xt;
        });
      }
    }), C.each({
      scrollLeft: "pageXOffset",
      scrollTop: "pageYOffset"
    }, function (t, e) {
      var n = "pageYOffset" === e;

      C.fn[t] = function (o) {
        return X(this, function (t, o, i) {
          var r;
          if (b(t) ? r = t : 9 === t.nodeType && (r = t.defaultView), void 0 === i) return r ? r[e] : t[o];
          r ? r.scrollTo(n ? r.pageXOffset : i, n ? i : r.pageYOffset) : t[o] = i;
        }, t, o, arguments.length);
      };
    }), C.each(["top", "left"], function (t, e) {
      C.cssHooks[e] = _t(v.pixelPosition, function (t, n) {
        if (n) return n = Bt(t, e), Ot.test(n) ? C(t).position()[e] + "px" : n;
      });
    }), C.each({
      Height: "height",
      Width: "width"
    }, function (t, e) {
      C.each({
        padding: "inner" + t,
        content: e,
        "": "outer" + t
      }, function (n, o) {
        C.fn[o] = function (i, r) {
          var s = arguments.length && (n || "boolean" != typeof i),
              a = n || (!0 === i || !0 === r ? "margin" : "border");
          return X(this, function (e, n, i) {
            var r;
            return b(e) ? 0 === o.indexOf("outer") ? e["inner" + t] : e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === i ? C.css(e, n, a) : C.style(e, n, i, a);
          }, e, s ? i : void 0, s);
        };
      });
    }), C.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (t, e) {
      C.fn[e] = function (t, n) {
        return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e);
      };
    }), C.fn.extend({
      hover: function hover(t, e) {
        return this.mouseenter(t).mouseleave(e || t);
      }
    }), C.fn.extend({
      bind: function bind(t, e, n) {
        return this.on(t, null, e, n);
      },
      unbind: function unbind(t, e) {
        return this.off(t, null, e);
      },
      delegate: function delegate(t, e, n, o) {
        return this.on(e, t, n, o);
      },
      undelegate: function undelegate(t, e, n) {
        return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n);
      }
    }), C.proxy = function (t, e) {
      var n, o, i;
      if ("string" == typeof e && (n = t[e], e = t, t = n), y(t)) return o = c.call(arguments, 2), (i = function i() {
        return t.apply(e || this, o.concat(c.call(arguments)));
      }).guid = t.guid = t.guid || C.guid++, i;
    }, C.holdReady = function (t) {
      t ? C.readyWait++ : C.ready(!0);
    }, C.isArray = Array.isArray, C.parseJSON = JSON.parse, C.nodeName = L, C.isFunction = y, C.isWindow = b, C.camelCase = Z, C.type = T, C.now = Date.now, C.isNumeric = function (t) {
      var e = C.type(t);
      return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t));
    }, void 0 === (o = function () {
      return C;
    }.apply(e, [])) || (t.exports = o);
    var Xe = n.jQuery,
        Ye = n.$;
    return C.noConflict = function (t) {
      return n.$ === C && (n.$ = Ye), t && n.jQuery === C && (n.jQuery = Xe), C;
    }, i || (n.jQuery = n.$ = C), C;
  });
},, function (t, e, n) {
  "use strict";

  n.r(e);
  var o = n(0),
      i = n.n(o);
  n(3);
  i()(function () {
    function t() {
      var t = document.getElementById("header-inner"),
          e = document.getElementById("catalog-nav"),
          n = t.offsetTop + t.clientHeight;
      window.pageYOffset > n ? e.classList.add("fixed") : e.classList.remove("fixed");
    }

    i()(".mobile-menu-tgl, .mobile-nav-close .close-tgl").click(function (t) {
      i()(".catalog-list").toggleClass("on");
    }), i()(".category-group").click(function (t) {
      var e = i()(t.target);
      i()(".categories-list").not(e.next()).hide(), e.next().toggle();
    }), t(), i()(window).scroll(function (e) {
      t();
    });
  });
}, function (t, e, n) {
  (function (t) {
    !function (t, e, n, o) {
      "use strict";

      if (t.console = t.console || {
        info: function info(t) {}
      }, n) if (n.fn.fancybox) console.info("fancyBox already initialized");else {
        var i = {
          closeExisting: !1,
          loop: !1,
          gutter: 50,
          keyboard: !0,
          preventCaptionOverlap: !0,
          arrows: !0,
          infobar: !0,
          smallBtn: "auto",
          toolbar: "auto",
          buttons: ["zoom", "slideShow", "thumbs", "close"],
          idleTime: 3,
          protect: !1,
          modal: !1,
          image: {
            preload: !1
          },
          ajax: {
            settings: {
              data: {
                fancybox: !0
              }
            }
          },
          iframe: {
            tpl: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src=""></iframe>',
            preload: !0,
            css: {},
            attr: {
              scrolling: "auto"
            }
          },
          video: {
            tpl: '<video class="fancybox-video" controls controlsList="nodownload" poster="{{poster}}"><source src="{{src}}" type="{{format}}" />Sorry, your browser doesn\'t support embedded videos, <a href="{{src}}">download</a> and watch with your favorite video player!</video>',
            format: "",
            autoStart: !0
          },
          defaultType: "image",
          animationEffect: "zoom",
          animationDuration: 366,
          zoomOpacity: "auto",
          transitionEffect: "fade",
          transitionDuration: 366,
          slideClass: "",
          baseClass: "",
          baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><div class="fancybox-toolbar">{{buttons}}</div><div class="fancybox-navigation">{{arrows}}</div><div class="fancybox-stage"></div><div class="fancybox-caption"><div class="fancybox-caption__body"></div></div></div></div>',
          spinnerTpl: '<div class="fancybox-loading"></div>',
          errorTpl: '<div class="fancybox-error"><p>{{ERROR}}</p></div>',
          btnTpl: {
            download: '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.62 17.09V19H5.38v-1.91zm-2.97-6.96L17 11.45l-5 4.87-5-4.87 1.36-1.32 2.68 2.64V5h1.92v7.77z"/></svg></a>',
            zoom: '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.7 17.3l-3-3a5.9 5.9 0 0 0-.6-7.6 5.9 5.9 0 0 0-8.4 0 5.9 5.9 0 0 0 0 8.4 5.9 5.9 0 0 0 7.7.7l3 3a1 1 0 0 0 1.3 0c.4-.5.4-1 0-1.5zM8.1 13.8a4 4 0 0 1 0-5.7 4 4 0 0 1 5.7 0 4 4 0 0 1 0 5.7 4 4 0 0 1-5.7 0z"/></svg></button>',
            close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"/></svg></button>',
            arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.28 15.7l-1.34 1.37L5 12l4.94-5.07 1.34 1.38-2.68 2.72H19v1.94H8.6z"/></svg></div></button>',
            arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.4 12.97l-2.68 2.72 1.34 1.38L19 12l-4.94-5.07-1.34 1.38 2.68 2.72H5v1.94z"/></svg></div></button>',
            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg></button>'
          },
          parentEl: "body",
          hideScrollbar: !0,
          autoFocus: !0,
          backFocus: !0,
          trapFocus: !0,
          fullScreen: {
            autoStart: !1
          },
          touch: {
            vertical: !0,
            momentum: !0
          },
          hash: null,
          media: {},
          slideShow: {
            autoStart: !1,
            speed: 3e3
          },
          thumbs: {
            autoStart: !1,
            hideOnClose: !0,
            parentEl: ".fancybox-container",
            axis: "y"
          },
          wheel: "auto",
          onInit: n.noop,
          beforeLoad: n.noop,
          afterLoad: n.noop,
          beforeShow: n.noop,
          afterShow: n.noop,
          beforeClose: n.noop,
          afterClose: n.noop,
          onActivate: n.noop,
          onDeactivate: n.noop,
          clickContent: function clickContent(t, e) {
            return "image" === t.type && "zoom";
          },
          clickSlide: "close",
          clickOutside: "close",
          dblclickContent: !1,
          dblclickSlide: !1,
          dblclickOutside: !1,
          mobile: {
            preventCaptionOverlap: !1,
            idleTime: !1,
            clickContent: function clickContent(t, e) {
              return "image" === t.type && "toggleControls";
            },
            clickSlide: function clickSlide(t, e) {
              return "image" === t.type ? "toggleControls" : "close";
            },
            dblclickContent: function dblclickContent(t, e) {
              return "image" === t.type && "zoom";
            },
            dblclickSlide: function dblclickSlide(t, e) {
              return "image" === t.type && "zoom";
            }
          },
          lang: "en",
          i18n: {
            en: {
              CLOSE: "Close",
              NEXT: "Next",
              PREV: "Previous",
              ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
              PLAY_START: "Start slideshow",
              PLAY_STOP: "Pause slideshow",
              FULL_SCREEN: "Full screen",
              THUMBS: "Thumbnails",
              DOWNLOAD: "Download",
              SHARE: "Share",
              ZOOM: "Zoom"
            },
            de: {
              CLOSE: "Schlie&szlig;en",
              NEXT: "Weiter",
              PREV: "Zur&uuml;ck",
              ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es sp&auml;ter nochmal.",
              PLAY_START: "Diaschau starten",
              PLAY_STOP: "Diaschau beenden",
              FULL_SCREEN: "Vollbild",
              THUMBS: "Vorschaubilder",
              DOWNLOAD: "Herunterladen",
              SHARE: "Teilen",
              ZOOM: "Vergr&ouml;&szlig;ern"
            }
          }
        },
            r = n(t),
            s = n(e),
            a = 0,
            c = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || function (e) {
          return t.setTimeout(e, 1e3 / 60);
        },
            l = t.cancelAnimationFrame || t.webkitCancelAnimationFrame || t.mozCancelAnimationFrame || t.oCancelAnimationFrame || function (e) {
          t.clearTimeout(e);
        },
            u = function () {
          var t,
              n = e.createElement("fakeelement"),
              o = {
            transition: "transitionend",
            OTransition: "oTransitionEnd",
            MozTransition: "transitionend",
            WebkitTransition: "webkitTransitionEnd"
          };

          for (t in o) {
            if (void 0 !== n.style[t]) return o[t];
          }

          return "transitionend";
        }(),
            f = function f(t) {
          return t && t.length && t[0].offsetHeight;
        },
            d = function d(t, e) {
          var o = n.extend(!0, {}, t, e);
          return n.each(e, function (t, e) {
            n.isArray(e) && (o[t] = e);
          }), o;
        },
            p = function p(t, e, o) {
          this.opts = d({
            index: o
          }, n.fancybox.defaults), n.isPlainObject(e) && (this.opts = d(this.opts, e)), n.fancybox.isMobile && (this.opts = d(this.opts, this.opts.mobile)), this.id = this.opts.id || ++a, this.currIndex = parseInt(this.opts.index, 10) || 0, this.prevIndex = null, this.prevPos = null, this.currPos = 0, this.firstRun = !0, this.group = [], this.slides = {}, this.addContent(t), this.group.length && this.init();
        };

        n.extend(p.prototype, {
          init: function init() {
            var o,
                i,
                r = this,
                s = r.group[r.currIndex].opts;
            s.closeExisting && n.fancybox.close(!0), n("body").addClass("fancybox-active"), !n.fancybox.getInstance() && !1 !== s.hideScrollbar && !n.fancybox.isMobile && e.body.scrollHeight > t.innerHeight && (n("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar{margin-right:' + (t.innerWidth - e.documentElement.clientWidth) + "px;}</style>"), n("body").addClass("compensate-for-scrollbar")), i = "", n.each(s.buttons, function (t, e) {
              i += s.btnTpl[e] || "";
            }), o = n(r.translate(r, s.baseTpl.replace("{{buttons}}", i).replace("{{arrows}}", s.btnTpl.arrowLeft + s.btnTpl.arrowRight))).attr("id", "fancybox-container-" + r.id).addClass(s.baseClass).data("FancyBox", r).appendTo(s.parentEl), r.$refs = {
              container: o
            }, ["bg", "inner", "infobar", "toolbar", "stage", "caption", "navigation"].forEach(function (t) {
              r.$refs[t] = o.find(".fancybox-" + t);
            }), r.trigger("onInit"), r.activate(), r.jumpTo(r.currIndex);
          },
          translate: function translate(t, e) {
            var n = t.opts.i18n[t.opts.lang] || t.opts.i18n.en;
            return e.replace(/\{\{(\w+)\}\}/g, function (t, e) {
              return void 0 === n[e] ? t : n[e];
            });
          },
          addContent: function addContent(t) {
            var e,
                o = this,
                i = n.makeArray(t);
            n.each(i, function (t, e) {
              var i,
                  r,
                  s,
                  a,
                  c,
                  l = {},
                  u = {};
              n.isPlainObject(e) ? (l = e, u = e.opts || e) : "object" === n.type(e) && n(e).length ? (u = (i = n(e)).data() || {}, (u = n.extend(!0, {}, u, u.options)).$orig = i, l.src = o.opts.src || u.src || i.attr("href"), l.type || l.src || (l.type = "inline", l.src = e)) : l = {
                type: "html",
                src: e + ""
              }, l.opts = n.extend(!0, {}, o.opts, u), n.isArray(u.buttons) && (l.opts.buttons = u.buttons), n.fancybox.isMobile && l.opts.mobile && (l.opts = d(l.opts, l.opts.mobile)), r = l.type || l.opts.type, a = l.src || "", !r && a && ((s = a.match(/\.(mp4|mov|ogv|webm)((\?|#).*)?$/i)) ? (r = "video", l.opts.video.format || (l.opts.video.format = "video/" + ("ogv" === s[1] ? "ogg" : s[1]))) : a.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i) ? r = "image" : a.match(/\.(pdf)((\?|#).*)?$/i) ? (r = "iframe", l = n.extend(!0, l, {
                contentType: "pdf",
                opts: {
                  iframe: {
                    preload: !1
                  }
                }
              })) : "#" === a.charAt(0) && (r = "inline")), r ? l.type = r : o.trigger("objectNeedsType", l), l.contentType || (l.contentType = n.inArray(l.type, ["html", "inline", "ajax"]) > -1 ? "html" : l.type), l.index = o.group.length, "auto" == l.opts.smallBtn && (l.opts.smallBtn = n.inArray(l.type, ["html", "inline", "ajax"]) > -1), "auto" === l.opts.toolbar && (l.opts.toolbar = !l.opts.smallBtn), l.$thumb = l.opts.$thumb || null, l.opts.$trigger && l.index === o.opts.index && (l.$thumb = l.opts.$trigger.find("img:first"), l.$thumb.length && (l.opts.$orig = l.opts.$trigger)), l.$thumb && l.$thumb.length || !l.opts.$orig || (l.$thumb = l.opts.$orig.find("img:first")), l.$thumb && !l.$thumb.length && (l.$thumb = null), l.thumb = l.opts.thumb || (l.$thumb ? l.$thumb[0].src : null), "function" === n.type(l.opts.caption) && (l.opts.caption = l.opts.caption.apply(e, [o, l])), "function" === n.type(o.opts.caption) && (l.opts.caption = o.opts.caption.apply(e, [o, l])), l.opts.caption instanceof n || (l.opts.caption = void 0 === l.opts.caption ? "" : l.opts.caption + ""), "ajax" === l.type && (c = a.split(/\s+/, 2)).length > 1 && (l.src = c.shift(), l.opts.filter = c.shift()), l.opts.modal && (l.opts = n.extend(!0, l.opts, {
                trapFocus: !0,
                infobar: 0,
                toolbar: 0,
                smallBtn: 0,
                keyboard: 0,
                slideShow: 0,
                fullScreen: 0,
                thumbs: 0,
                touch: 0,
                clickContent: !1,
                clickSlide: !1,
                clickOutside: !1,
                dblclickContent: !1,
                dblclickSlide: !1,
                dblclickOutside: !1
              })), o.group.push(l);
            }), Object.keys(o.slides).length && (o.updateControls(), (e = o.Thumbs) && e.isActive && (e.create(), e.focus()));
          },
          addEvents: function addEvents() {
            var e = this;
            e.removeEvents(), e.$refs.container.on("click.fb-close", "[data-fancybox-close]", function (t) {
              t.stopPropagation(), t.preventDefault(), e.close(t);
            }).on("touchstart.fb-prev click.fb-prev", "[data-fancybox-prev]", function (t) {
              t.stopPropagation(), t.preventDefault(), e.previous();
            }).on("touchstart.fb-next click.fb-next", "[data-fancybox-next]", function (t) {
              t.stopPropagation(), t.preventDefault(), e.next();
            }).on("click.fb", "[data-fancybox-zoom]", function (t) {
              e[e.isScaledDown() ? "scaleToActual" : "scaleToFit"]();
            }), r.on("orientationchange.fb resize.fb", function (t) {
              t && t.originalEvent && "resize" === t.originalEvent.type ? (e.requestId && l(e.requestId), e.requestId = c(function () {
                e.update(t);
              })) : (e.current && "iframe" === e.current.type && e.$refs.stage.hide(), setTimeout(function () {
                e.$refs.stage.show(), e.update(t);
              }, n.fancybox.isMobile ? 600 : 250));
            }), s.on("keydown.fb", function (t) {
              var o = (n.fancybox ? n.fancybox.getInstance() : null).current,
                  i = t.keyCode || t.which;

              if (9 != i) {
                if (!(!o.opts.keyboard || t.ctrlKey || t.altKey || t.shiftKey || n(t.target).is("input,textarea,video,audio"))) return 8 === i || 27 === i ? (t.preventDefault(), void e.close(t)) : 37 === i || 38 === i ? (t.preventDefault(), void e.previous()) : 39 === i || 40 === i ? (t.preventDefault(), void e.next()) : void e.trigger("afterKeydown", t, i);
              } else o.opts.trapFocus && e.focus(t);
            }), e.group[e.currIndex].opts.idleTime && (e.idleSecondsCounter = 0, s.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle", function (t) {
              e.idleSecondsCounter = 0, e.isIdle && e.showControls(), e.isIdle = !1;
            }), e.idleInterval = t.setInterval(function () {
              e.idleSecondsCounter++, e.idleSecondsCounter >= e.group[e.currIndex].opts.idleTime && !e.isDragging && (e.isIdle = !0, e.idleSecondsCounter = 0, e.hideControls());
            }, 1e3));
          },
          removeEvents: function removeEvents() {
            r.off("orientationchange.fb resize.fb"), s.off("keydown.fb .fb-idle"), this.$refs.container.off(".fb-close .fb-prev .fb-next"), this.idleInterval && (t.clearInterval(this.idleInterval), this.idleInterval = null);
          },
          previous: function previous(t) {
            return this.jumpTo(this.currPos - 1, t);
          },
          next: function next(t) {
            return this.jumpTo(this.currPos + 1, t);
          },
          jumpTo: function jumpTo(t, e) {
            var o,
                i,
                r,
                s,
                a,
                c,
                l,
                u,
                d,
                p = this,
                h = p.group.length;

            if (!(p.isDragging || p.isClosing || p.isAnimating && p.firstRun)) {
              if (t = parseInt(t, 10), !(r = p.current ? p.current.opts.loop : p.opts.loop) && (t < 0 || t >= h)) return !1;
              if (o = p.firstRun = !Object.keys(p.slides).length, a = p.current, p.prevIndex = p.currIndex, p.prevPos = p.currPos, s = p.createSlide(t), h > 1 && ((r || s.index < h - 1) && p.createSlide(t + 1), (r || s.index > 0) && p.createSlide(t - 1)), p.current = s, p.currIndex = s.index, p.currPos = s.pos, p.trigger("beforeShow", o), p.updateControls(), s.forcedDuration = void 0, n.isNumeric(e) ? s.forcedDuration = e : e = s.opts[o ? "animationDuration" : "transitionDuration"], e = parseInt(e, 10), i = p.isMoved(s), s.$slide.addClass("fancybox-slide--current"), o) return s.opts.animationEffect && e && p.$refs.container.css("transition-duration", e + "ms"), p.$refs.container.addClass("fancybox-is-open").trigger("focus"), p.loadSlide(s), void p.preload("image");
              c = n.fancybox.getTranslate(a.$slide), l = n.fancybox.getTranslate(p.$refs.stage), n.each(p.slides, function (t, e) {
                n.fancybox.stop(e.$slide, !0);
              }), a.pos !== s.pos && (a.isComplete = !1), a.$slide.removeClass("fancybox-slide--complete fancybox-slide--current"), i ? (d = c.left - (a.pos * c.width + a.pos * a.opts.gutter), n.each(p.slides, function (t, o) {
                o.$slide.removeClass("fancybox-animated").removeClass(function (t, e) {
                  return (e.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ");
                });
                var i = o.pos * c.width + o.pos * o.opts.gutter;
                n.fancybox.setTranslate(o.$slide, {
                  top: 0,
                  left: i - l.left + d
                }), o.pos !== s.pos && o.$slide.addClass("fancybox-slide--" + (o.pos > s.pos ? "next" : "previous")), f(o.$slide), n.fancybox.animate(o.$slide, {
                  top: 0,
                  left: (o.pos - s.pos) * c.width + (o.pos - s.pos) * o.opts.gutter
                }, e, function () {
                  o.$slide.css({
                    transform: "",
                    opacity: ""
                  }).removeClass("fancybox-slide--next fancybox-slide--previous"), o.pos === p.currPos && p.complete();
                });
              })) : e && s.opts.transitionEffect && (u = "fancybox-animated fancybox-fx-" + s.opts.transitionEffect, a.$slide.addClass("fancybox-slide--" + (a.pos > s.pos ? "next" : "previous")), n.fancybox.animate(a.$slide, u, e, function () {
                a.$slide.removeClass(u).removeClass("fancybox-slide--next fancybox-slide--previous");
              }, !1)), s.isLoaded ? p.revealContent(s) : p.loadSlide(s), p.preload("image");
            }
          },
          createSlide: function createSlide(t) {
            var e, o;
            return o = (o = t % this.group.length) < 0 ? this.group.length + o : o, !this.slides[t] && this.group[o] && (e = n('<div class="fancybox-slide"></div>').appendTo(this.$refs.stage), this.slides[t] = n.extend(!0, {}, this.group[o], {
              pos: t,
              $slide: e,
              isLoaded: !1
            }), this.updateSlide(this.slides[t])), this.slides[t];
          },
          scaleToActual: function scaleToActual(t, e, o) {
            var i,
                r,
                s,
                a,
                c,
                l = this,
                u = l.current,
                f = u.$content,
                d = n.fancybox.getTranslate(u.$slide).width,
                p = n.fancybox.getTranslate(u.$slide).height,
                h = u.width,
                g = u.height;
            l.isAnimating || l.isMoved() || !f || "image" != u.type || !u.isLoaded || u.hasError || (l.isAnimating = !0, n.fancybox.stop(f), t = void 0 === t ? .5 * d : t, e = void 0 === e ? .5 * p : e, (i = n.fancybox.getTranslate(f)).top -= n.fancybox.getTranslate(u.$slide).top, i.left -= n.fancybox.getTranslate(u.$slide).left, a = h / i.width, c = g / i.height, r = .5 * d - .5 * h, s = .5 * p - .5 * g, h > d && ((r = i.left * a - (t * a - t)) > 0 && (r = 0), r < d - h && (r = d - h)), g > p && ((s = i.top * c - (e * c - e)) > 0 && (s = 0), s < p - g && (s = p - g)), l.updateCursor(h, g), n.fancybox.animate(f, {
              top: s,
              left: r,
              scaleX: a,
              scaleY: c
            }, o || 366, function () {
              l.isAnimating = !1;
            }), l.SlideShow && l.SlideShow.isActive && l.SlideShow.stop());
          },
          scaleToFit: function scaleToFit(t) {
            var e,
                o = this,
                i = o.current,
                r = i.$content;
            o.isAnimating || o.isMoved() || !r || "image" != i.type || !i.isLoaded || i.hasError || (o.isAnimating = !0, n.fancybox.stop(r), e = o.getFitPos(i), o.updateCursor(e.width, e.height), n.fancybox.animate(r, {
              top: e.top,
              left: e.left,
              scaleX: e.width / r.width(),
              scaleY: e.height / r.height()
            }, t || 366, function () {
              o.isAnimating = !1;
            }));
          },
          getFitPos: function getFitPos(t) {
            var e,
                o,
                i,
                r,
                s = t.$content,
                a = t.$slide,
                c = t.width || t.opts.width,
                l = t.height || t.opts.height,
                u = {};
            return !!(t.isLoaded && s && s.length) && (e = n.fancybox.getTranslate(this.$refs.stage).width, o = n.fancybox.getTranslate(this.$refs.stage).height, e -= parseFloat(a.css("paddingLeft")) + parseFloat(a.css("paddingRight")) + parseFloat(s.css("marginLeft")) + parseFloat(s.css("marginRight")), o -= parseFloat(a.css("paddingTop")) + parseFloat(a.css("paddingBottom")) + parseFloat(s.css("marginTop")) + parseFloat(s.css("marginBottom")), c && l || (c = e, l = o), l *= i = Math.min(1, e / c, o / l), (c *= i) > e - .5 && (c = e), l > o - .5 && (l = o), "image" === t.type ? (u.top = Math.floor(.5 * (o - l)) + parseFloat(a.css("paddingTop")), u.left = Math.floor(.5 * (e - c)) + parseFloat(a.css("paddingLeft"))) : "video" === t.contentType && (l > c / (r = t.opts.width && t.opts.height ? c / l : t.opts.ratio || 16 / 9) ? l = c / r : c > l * r && (c = l * r)), u.width = c, u.height = l, u);
          },
          update: function update(t) {
            var e = this;
            n.each(e.slides, function (n, o) {
              e.updateSlide(o, t);
            });
          },
          updateSlide: function updateSlide(t, e) {
            var o = t && t.$content,
                i = t.width || t.opts.width,
                r = t.height || t.opts.height,
                s = t.$slide;
            this.adjustCaption(t), o && (i || r || "video" === t.contentType) && !t.hasError && (n.fancybox.stop(o), n.fancybox.setTranslate(o, this.getFitPos(t)), t.pos === this.currPos && (this.isAnimating = !1, this.updateCursor())), this.adjustLayout(t), s.length && (s.trigger("refresh"), t.pos === this.currPos && this.$refs.toolbar.add(this.$refs.navigation.find(".fancybox-button--arrow_right")).toggleClass("compensate-for-scrollbar", s.get(0).scrollHeight > s.get(0).clientHeight)), this.trigger("onUpdate", t, e);
          },
          centerSlide: function centerSlide(t) {
            var e = this,
                o = e.current,
                i = o.$slide;
            !e.isClosing && o && (i.siblings().css({
              transform: "",
              opacity: ""
            }), i.parent().children().removeClass("fancybox-slide--previous fancybox-slide--next"), n.fancybox.animate(i, {
              top: 0,
              left: 0,
              opacity: 1
            }, void 0 === t ? 0 : t, function () {
              i.css({
                transform: "",
                opacity: ""
              }), o.isComplete || e.complete();
            }, !1));
          },
          isMoved: function isMoved(t) {
            var e,
                o,
                i = t || this.current;
            return !!i && (o = n.fancybox.getTranslate(this.$refs.stage), e = n.fancybox.getTranslate(i.$slide), !i.$slide.hasClass("fancybox-animated") && (Math.abs(e.top - o.top) > .5 || Math.abs(e.left - o.left) > .5));
          },
          updateCursor: function updateCursor(t, e) {
            var o,
                i,
                r = this.current,
                s = this.$refs.container;
            r && !this.isClosing && this.Guestures && (s.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-zoomOut fancybox-can-swipe fancybox-can-pan"), i = !!(o = this.canPan(t, e)) || this.isZoomable(), s.toggleClass("fancybox-is-zoomable", i), n("[data-fancybox-zoom]").prop("disabled", !i), o ? s.addClass("fancybox-can-pan") : i && ("zoom" === r.opts.clickContent || n.isFunction(r.opts.clickContent) && "zoom" == r.opts.clickContent(r)) ? s.addClass("fancybox-can-zoomIn") : r.opts.touch && (r.opts.touch.vertical || this.group.length > 1) && "video" !== r.contentType && s.addClass("fancybox-can-swipe"));
          },
          isZoomable: function isZoomable() {
            var t,
                e = this.current;

            if (e && !this.isClosing && "image" === e.type && !e.hasError) {
              if (!e.isLoaded) return !0;
              if ((t = this.getFitPos(e)) && (e.width > t.width || e.height > t.height)) return !0;
            }

            return !1;
          },
          isScaledDown: function isScaledDown(t, e) {
            var o = !1,
                i = this.current,
                r = i.$content;
            return void 0 !== t && void 0 !== e ? o = t < i.width && e < i.height : r && (o = (o = n.fancybox.getTranslate(r)).width < i.width && o.height < i.height), o;
          },
          canPan: function canPan(t, e) {
            var o = this.current,
                i = null,
                r = !1;
            return "image" === o.type && (o.isComplete || t && e) && !o.hasError && (r = this.getFitPos(o), void 0 !== t && void 0 !== e ? i = {
              width: t,
              height: e
            } : o.isComplete && (i = n.fancybox.getTranslate(o.$content)), i && r && (r = Math.abs(i.width - r.width) > 1.5 || Math.abs(i.height - r.height) > 1.5)), r;
          },
          loadSlide: function loadSlide(t) {
            var e,
                o,
                i,
                r = this;

            if (!t.isLoading && !t.isLoaded) {
              if (t.isLoading = !0, !1 === r.trigger("beforeLoad", t)) return t.isLoading = !1, !1;

              switch (e = t.type, (o = t.$slide).off("refresh").trigger("onReset").addClass(t.opts.slideClass), e) {
                case "image":
                  r.setImage(t);
                  break;

                case "iframe":
                  r.setIframe(t);
                  break;

                case "html":
                  r.setContent(t, t.src || t.content);
                  break;

                case "video":
                  r.setContent(t, t.opts.video.tpl.replace(/\{\{src\}\}/gi, t.src).replace("{{format}}", t.opts.videoFormat || t.opts.video.format || "").replace("{{poster}}", t.thumb || ""));
                  break;

                case "inline":
                  n(t.src).length ? r.setContent(t, n(t.src)) : r.setError(t);
                  break;

                case "ajax":
                  r.showLoading(t), i = n.ajax(n.extend({}, t.opts.ajax.settings, {
                    url: t.src,
                    success: function success(e, n) {
                      "success" === n && r.setContent(t, e);
                    },
                    error: function error(e, n) {
                      e && "abort" !== n && r.setError(t);
                    }
                  })), o.one("onReset", function () {
                    i.abort();
                  });
                  break;

                default:
                  r.setError(t);
              }

              return !0;
            }
          },
          setImage: function setImage(t) {
            var o,
                i = this;
            setTimeout(function () {
              var e = t.$image;
              i.isClosing || !t.isLoading || e && e.length && e[0].complete || t.hasError || i.showLoading(t);
            }, 50), i.checkSrcset(t), t.$content = n('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(t.$slide.addClass("fancybox-slide--image")), !1 !== t.opts.preload && t.opts.width && t.opts.height && t.thumb && (t.width = t.opts.width, t.height = t.opts.height, (o = e.createElement("img")).onerror = function () {
              n(this).remove(), t.$ghost = null;
            }, o.onload = function () {
              i.afterLoad(t);
            }, t.$ghost = n(o).addClass("fancybox-image").appendTo(t.$content).attr("src", t.thumb)), i.setBigImage(t);
          },
          checkSrcset: function checkSrcset(e) {
            var n,
                o,
                i,
                r,
                s = e.opts.srcset || e.opts.image.srcset;

            if (s) {
              i = t.devicePixelRatio || 1, r = t.innerWidth * i, (o = s.split(",").map(function (t) {
                var e = {};
                return t.trim().split(/\s+/).forEach(function (t, n) {
                  var o = parseInt(t.substring(0, t.length - 1), 10);
                  if (0 === n) return e.url = t;
                  o && (e.value = o, e.postfix = t[t.length - 1]);
                }), e;
              })).sort(function (t, e) {
                return t.value - e.value;
              });

              for (var a = 0; a < o.length; a++) {
                var c = o[a];

                if ("w" === c.postfix && c.value >= r || "x" === c.postfix && c.value >= i) {
                  n = c;
                  break;
                }
              }

              !n && o.length && (n = o[o.length - 1]), n && (e.src = n.url, e.width && e.height && "w" == n.postfix && (e.height = e.width / e.height * n.value, e.width = n.value), e.opts.srcset = s);
            }
          },
          setBigImage: function setBigImage(t) {
            var o = this,
                i = e.createElement("img"),
                s = n(i);
            t.$image = s.one("error", function () {
              o.setError(t);
            }).one("load", function () {
              var e;
              t.$ghost || (o.resolveImageSlideSize(t, this.naturalWidth, this.naturalHeight), o.afterLoad(t)), o.isClosing || (t.opts.srcset && ((e = t.opts.sizes) && "auto" !== e || (e = (t.width / t.height > 1 && r.width() / r.height() > 1 ? "100" : Math.round(t.width / t.height * 100)) + "vw"), s.attr("sizes", e).attr("srcset", t.opts.srcset)), t.$ghost && setTimeout(function () {
                t.$ghost && !o.isClosing && t.$ghost.hide();
              }, Math.min(300, Math.max(1e3, t.height / 1600))), o.hideLoading(t));
            }).addClass("fancybox-image").attr("src", t.src).appendTo(t.$content), (i.complete || "complete" == i.readyState) && s.naturalWidth && s.naturalHeight ? s.trigger("load") : i.error && s.trigger("error");
          },
          resolveImageSlideSize: function resolveImageSlideSize(t, e, n) {
            var o = parseInt(t.opts.width, 10),
                i = parseInt(t.opts.height, 10);
            t.width = e, t.height = n, o > 0 && (t.width = o, t.height = Math.floor(o * n / e)), i > 0 && (t.width = Math.floor(i * e / n), t.height = i);
          },
          setIframe: function setIframe(t) {
            var e,
                o = this,
                i = t.opts.iframe,
                r = t.$slide;
            t.$content = n('<div class="fancybox-content' + (i.preload ? " fancybox-is-hidden" : "") + '"></div>').css(i.css).appendTo(r), r.addClass("fancybox-slide--" + t.contentType), t.$iframe = e = n(i.tpl.replace(/\{rnd\}/g, new Date().getTime())).attr(i.attr).appendTo(t.$content), i.preload ? (o.showLoading(t), e.on("load.fb error.fb", function (e) {
              this.isReady = 1, t.$slide.trigger("refresh"), o.afterLoad(t);
            }), r.on("refresh.fb", function () {
              var n,
                  o = t.$content,
                  s = i.css.width,
                  a = i.css.height;

              if (1 === e[0].isReady) {
                try {
                  n = e.contents().find("body");
                } catch (t) {}

                n && n.length && n.children().length && (r.css("overflow", "visible"), o.css({
                  width: "100%",
                  "max-width": "100%",
                  height: "9999px"
                }), void 0 === s && (s = Math.ceil(Math.max(n[0].clientWidth, n.outerWidth(!0)))), o.css("width", s || "").css("max-width", ""), void 0 === a && (a = Math.ceil(Math.max(n[0].clientHeight, n.outerHeight(!0)))), o.css("height", a || ""), r.css("overflow", "auto")), o.removeClass("fancybox-is-hidden");
              }
            })) : o.afterLoad(t), e.attr("src", t.src), r.one("onReset", function () {
              try {
                n(this).find("iframe").hide().unbind().attr("src", "//about:blank");
              } catch (t) {}

              n(this).off("refresh.fb").empty(), t.isLoaded = !1, t.isRevealed = !1;
            });
          },
          setContent: function setContent(t, e) {
            this.isClosing || (this.hideLoading(t), t.$content && n.fancybox.stop(t.$content), t.$slide.empty(), function (t) {
              return t && t.hasOwnProperty && t instanceof n;
            }(e) && e.parent().length ? ((e.hasClass("fancybox-content") || e.parent().hasClass("fancybox-content")) && e.parents(".fancybox-slide").trigger("onReset"), t.$placeholder = n("<div>").hide().insertAfter(e), e.css("display", "inline-block")) : t.hasError || ("string" === n.type(e) && (e = n("<div>").append(n.trim(e)).contents()), t.opts.filter && (e = n("<div>").html(e).find(t.opts.filter))), t.$slide.one("onReset", function () {
              n(this).find("video,audio").trigger("pause"), t.$placeholder && (t.$placeholder.after(e.removeClass("fancybox-content").hide()).remove(), t.$placeholder = null), t.$smallBtn && (t.$smallBtn.remove(), t.$smallBtn = null), t.hasError || (n(this).empty(), t.isLoaded = !1, t.isRevealed = !1);
            }), n(e).appendTo(t.$slide), n(e).is("video,audio") && (n(e).addClass("fancybox-video"), n(e).wrap("<div></div>"), t.contentType = "video", t.opts.width = t.opts.width || n(e).attr("width"), t.opts.height = t.opts.height || n(e).attr("height")), t.$content = t.$slide.children().filter("div,form,main,video,audio,article,.fancybox-content").first(), t.$content.siblings().hide(), t.$content.length || (t.$content = t.$slide.wrapInner("<div></div>").children().first()), t.$content.addClass("fancybox-content"), t.$slide.addClass("fancybox-slide--" + t.contentType), this.afterLoad(t));
          },
          setError: function setError(t) {
            t.hasError = !0, t.$slide.trigger("onReset").removeClass("fancybox-slide--" + t.contentType).addClass("fancybox-slide--error"), t.contentType = "html", this.setContent(t, this.translate(t, t.opts.errorTpl)), t.pos === this.currPos && (this.isAnimating = !1);
          },
          showLoading: function showLoading(t) {
            (t = t || this.current) && !t.$spinner && (t.$spinner = n(this.translate(this, this.opts.spinnerTpl)).appendTo(t.$slide).hide().fadeIn("fast"));
          },
          hideLoading: function hideLoading(t) {
            (t = t || this.current) && t.$spinner && (t.$spinner.stop().remove(), delete t.$spinner);
          },
          afterLoad: function afterLoad(t) {
            this.isClosing || (t.isLoading = !1, t.isLoaded = !0, this.trigger("afterLoad", t), this.hideLoading(t), !t.opts.smallBtn || t.$smallBtn && t.$smallBtn.length || (t.$smallBtn = n(this.translate(t, t.opts.btnTpl.smallBtn)).appendTo(t.$content)), t.opts.protect && t.$content && !t.hasError && (t.$content.on("contextmenu.fb", function (t) {
              return 2 == t.button && t.preventDefault(), !0;
            }), "image" === t.type && n('<div class="fancybox-spaceball"></div>').appendTo(t.$content)), this.adjustCaption(t), this.adjustLayout(t), t.pos === this.currPos && this.updateCursor(), this.revealContent(t));
          },
          adjustCaption: function adjustCaption(t) {
            var e,
                n = t || this.current,
                o = n.opts.caption,
                i = n.opts.preventCaptionOverlap,
                r = this.$refs.caption,
                s = !1;
            r.toggleClass("fancybox-caption--separate", i), i && o && o.length && (n.pos !== this.currPos ? ((e = r.clone().appendTo(r.parent())).children().eq(0).empty().html(o), s = e.outerHeight(!0), e.empty().remove()) : this.$caption && (s = this.$caption.outerHeight(!0)), n.$slide.css("padding-bottom", s || ""));
          },
          adjustLayout: function adjustLayout(t) {
            var e,
                n,
                o,
                i,
                r = t || this.current;
            r.isLoaded && !0 !== r.opts.disableLayoutFix && (r.$content.css("margin-bottom", ""), r.$content.outerHeight() > r.$slide.height() + .5 && (o = r.$slide[0].style["padding-bottom"], i = r.$slide.css("padding-bottom"), parseFloat(i) > 0 && (e = r.$slide[0].scrollHeight, r.$slide.css("padding-bottom", 0), Math.abs(e - r.$slide[0].scrollHeight) < 1 && (n = i), r.$slide.css("padding-bottom", o))), r.$content.css("margin-bottom", n));
          },
          revealContent: function revealContent(t) {
            var e,
                o,
                i,
                r,
                s = this,
                a = t.$slide,
                c = !1,
                l = !1,
                u = s.isMoved(t),
                d = t.isRevealed;
            return t.isRevealed = !0, e = t.opts[s.firstRun ? "animationEffect" : "transitionEffect"], i = t.opts[s.firstRun ? "animationDuration" : "transitionDuration"], i = parseInt(void 0 === t.forcedDuration ? i : t.forcedDuration, 10), !u && t.pos === s.currPos && i || (e = !1), "zoom" === e && (t.pos === s.currPos && i && "image" === t.type && !t.hasError && (l = s.getThumbPos(t)) ? c = s.getFitPos(t) : e = "fade"), "zoom" === e ? (s.isAnimating = !0, c.scaleX = c.width / l.width, c.scaleY = c.height / l.height, "auto" == (r = t.opts.zoomOpacity) && (r = Math.abs(t.width / t.height - l.width / l.height) > .1), r && (l.opacity = .1, c.opacity = 1), n.fancybox.setTranslate(t.$content.removeClass("fancybox-is-hidden"), l), f(t.$content), void n.fancybox.animate(t.$content, c, i, function () {
              s.isAnimating = !1, s.complete();
            })) : (s.updateSlide(t), e ? (n.fancybox.stop(a), o = "fancybox-slide--" + (t.pos >= s.prevPos ? "next" : "previous") + " fancybox-animated fancybox-fx-" + e, a.addClass(o).removeClass("fancybox-slide--current"), t.$content.removeClass("fancybox-is-hidden"), f(a), "image" !== t.type && t.$content.hide().show(0), void n.fancybox.animate(a, "fancybox-slide--current", i, function () {
              a.removeClass(o).css({
                transform: "",
                opacity: ""
              }), t.pos === s.currPos && s.complete();
            }, !0)) : (t.$content.removeClass("fancybox-is-hidden"), d || !u || "image" !== t.type || t.hasError || t.$content.hide().fadeIn("fast"), void (t.pos === s.currPos && s.complete())));
          },
          getThumbPos: function getThumbPos(t) {
            var o,
                i,
                r,
                s,
                a,
                c,
                l = t.$thumb;
            return !(!l || !function (t) {
              var o, i;
              return !(!t || t.ownerDocument !== e) && (n(".fancybox-container").css("pointer-events", "none"), o = {
                x: t.getBoundingClientRect().left + t.offsetWidth / 2,
                y: t.getBoundingClientRect().top + t.offsetHeight / 2
              }, i = e.elementFromPoint(o.x, o.y) === t, n(".fancybox-container").css("pointer-events", ""), i);
            }(l[0])) && (i = n.fancybox.getTranslate(l), r = parseFloat(l.css("border-top-width") || 0), s = parseFloat(l.css("border-right-width") || 0), a = parseFloat(l.css("border-bottom-width") || 0), c = parseFloat(l.css("border-left-width") || 0), o = {
              top: i.top + r,
              left: i.left + c,
              width: i.width - s - c,
              height: i.height - r - a,
              scaleX: 1,
              scaleY: 1
            }, i.width > 0 && i.height > 0 && o);
          },
          complete: function complete() {
            var t,
                e = this,
                o = e.current,
                i = {};
            !e.isMoved() && o.isLoaded && (o.isComplete || (o.isComplete = !0, o.$slide.siblings().trigger("onReset"), e.preload("inline"), f(o.$slide), o.$slide.addClass("fancybox-slide--complete"), n.each(e.slides, function (t, o) {
              o.pos >= e.currPos - 1 && o.pos <= e.currPos + 1 ? i[o.pos] = o : o && (n.fancybox.stop(o.$slide), o.$slide.off().remove());
            }), e.slides = i), e.isAnimating = !1, e.updateCursor(), e.trigger("afterShow"), o.opts.video.autoStart && o.$slide.find("video,audio").filter(":visible:first").trigger("play").one("ended", function () {
              this.webkitExitFullscreen && this.webkitExitFullscreen(), e.next();
            }), o.opts.autoFocus && "html" === o.contentType && ((t = o.$content.find("input[autofocus]:enabled:visible:first")).length ? t.trigger("focus") : e.focus(null, !0)), o.$slide.scrollTop(0).scrollLeft(0));
          },
          preload: function preload(t) {
            var e, n;
            this.group.length < 2 || (n = this.slides[this.currPos + 1], (e = this.slides[this.currPos - 1]) && e.type === t && this.loadSlide(e), n && n.type === t && this.loadSlide(n));
          },
          focus: function focus(t, o) {
            var i,
                r,
                s = ["a[href]", "area[href]", 'input:not([disabled]):not([type="hidden"]):not([aria-hidden])', "select:not([disabled]):not([aria-hidden])", "textarea:not([disabled]):not([aria-hidden])", "button:not([disabled]):not([aria-hidden])", "iframe", "object", "embed", "video", "audio", "[contenteditable]", '[tabindex]:not([tabindex^="-"])'].join(",");
            this.isClosing || ((i = (i = !t && this.current && this.current.isComplete ? this.current.$slide.find("*:visible" + (o ? ":not(.fancybox-close-small)" : "")) : this.$refs.container.find("*:visible")).filter(s).filter(function () {
              return "hidden" !== n(this).css("visibility") && !n(this).hasClass("disabled");
            })).length ? (r = i.index(e.activeElement), t && t.shiftKey ? (r < 0 || 0 == r) && (t.preventDefault(), i.eq(i.length - 1).trigger("focus")) : (r < 0 || r == i.length - 1) && (t && t.preventDefault(), i.eq(0).trigger("focus"))) : this.$refs.container.trigger("focus"));
          },
          activate: function activate() {
            var t = this;
            n(".fancybox-container").each(function () {
              var e = n(this).data("FancyBox");
              e && e.id !== t.id && !e.isClosing && (e.trigger("onDeactivate"), e.removeEvents(), e.isVisible = !1);
            }), t.isVisible = !0, (t.current || t.isIdle) && (t.update(), t.updateControls()), t.trigger("onActivate"), t.addEvents();
          },
          close: function close(t, e) {
            var o,
                i,
                r,
                s,
                a,
                l,
                u,
                d = this,
                p = d.current,
                h = function h() {
              d.cleanUp(t);
            };

            return !d.isClosing && (d.isClosing = !0, !1 === d.trigger("beforeClose", t) ? (d.isClosing = !1, c(function () {
              d.update();
            }), !1) : (d.removeEvents(), r = p.$content, o = p.opts.animationEffect, i = n.isNumeric(e) ? e : o ? p.opts.animationDuration : 0, p.$slide.removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"), !0 !== t ? n.fancybox.stop(p.$slide) : o = !1, p.$slide.siblings().trigger("onReset").remove(), i && d.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing").css("transition-duration", i + "ms"), d.hideLoading(p), d.hideControls(!0), d.updateCursor(), "zoom" !== o || r && i && "image" === p.type && !d.isMoved() && !p.hasError && (u = d.getThumbPos(p)) || (o = "fade"), "zoom" === o ? (n.fancybox.stop(r), l = {
              top: (s = n.fancybox.getTranslate(r)).top,
              left: s.left,
              scaleX: s.width / u.width,
              scaleY: s.height / u.height,
              width: u.width,
              height: u.height
            }, "auto" == (a = p.opts.zoomOpacity) && (a = Math.abs(p.width / p.height - u.width / u.height) > .1), a && (u.opacity = 0), n.fancybox.setTranslate(r, l), f(r), n.fancybox.animate(r, u, i, h), !0) : (o && i ? n.fancybox.animate(p.$slide.addClass("fancybox-slide--previous").removeClass("fancybox-slide--current"), "fancybox-animated fancybox-fx-" + o, i, h) : !0 === t ? setTimeout(h, i) : h(), !0)));
          },
          cleanUp: function cleanUp(e) {
            var o,
                i,
                r,
                s = this.current.opts.$orig;
            this.current.$slide.trigger("onReset"), this.$refs.container.empty().remove(), this.trigger("afterClose", e), this.current.opts.backFocus && (s && s.length && s.is(":visible") || (s = this.$trigger), s && s.length && (i = t.scrollX, r = t.scrollY, s.trigger("focus"), n("html, body").scrollTop(r).scrollLeft(i))), this.current = null, (o = n.fancybox.getInstance()) ? o.activate() : (n("body").removeClass("fancybox-active compensate-for-scrollbar"), n("#fancybox-style-noscroll").remove());
          },
          trigger: function trigger(t, e) {
            var o,
                i = Array.prototype.slice.call(arguments, 1),
                r = e && e.opts ? e : this.current;
            if (r ? i.unshift(r) : r = this, i.unshift(this), n.isFunction(r.opts[t]) && (o = r.opts[t].apply(r, i)), !1 === o) return o;
            "afterClose" !== t && this.$refs ? this.$refs.container.trigger(t + ".fb", i) : s.trigger(t + ".fb", i);
          },
          updateControls: function updateControls() {
            var t = this.current,
                o = t.index,
                i = this.$refs.container,
                r = this.$refs.caption,
                s = t.opts.caption;
            t.$slide.trigger("refresh"), s && s.length ? (this.$caption = r, r.children().eq(0).html(s)) : this.$caption = null, this.hasHiddenControls || this.isIdle || this.showControls(), i.find("[data-fancybox-count]").html(this.group.length), i.find("[data-fancybox-index]").html(o + 1), i.find("[data-fancybox-prev]").prop("disabled", !t.opts.loop && o <= 0), i.find("[data-fancybox-next]").prop("disabled", !t.opts.loop && o >= this.group.length - 1), "image" === t.type ? i.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href", t.opts.image.src || t.src).show() : t.opts.toolbar && i.find("[data-fancybox-download],[data-fancybox-zoom]").hide(), n(e.activeElement).is(":hidden,[disabled]") && this.$refs.container.trigger("focus");
          },
          hideControls: function hideControls(t) {
            var e = ["infobar", "toolbar", "nav"];
            !t && this.current.opts.preventCaptionOverlap || e.push("caption"), this.$refs.container.removeClass(e.map(function (t) {
              return "fancybox-show-" + t;
            }).join(" ")), this.hasHiddenControls = !0;
          },
          showControls: function showControls() {
            var t = this.current ? this.current.opts : this.opts,
                e = this.$refs.container;
            this.hasHiddenControls = !1, this.idleSecondsCounter = 0, e.toggleClass("fancybox-show-toolbar", !(!t.toolbar || !t.buttons)).toggleClass("fancybox-show-infobar", !!(t.infobar && this.group.length > 1)).toggleClass("fancybox-show-caption", !!this.$caption).toggleClass("fancybox-show-nav", !!(t.arrows && this.group.length > 1)).toggleClass("fancybox-is-modal", !!t.modal);
          },
          toggleControls: function toggleControls() {
            this.hasHiddenControls ? this.showControls() : this.hideControls();
          }
        }), n.fancybox = {
          version: "3.5.6",
          defaults: i,
          getInstance: function getInstance(t) {
            var e = n('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),
                o = Array.prototype.slice.call(arguments, 1);
            return e instanceof p && ("string" === n.type(t) ? e[t].apply(e, o) : "function" === n.type(t) && t.apply(e, o), e);
          },
          open: function open(t, e, n) {
            return new p(t, e, n);
          },
          close: function close(t) {
            var e = this.getInstance();
            e && (e.close(), !0 === t && this.close(t));
          },
          destroy: function destroy() {
            this.close(!0), s.add("body").off("click.fb-start", "**");
          },
          isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
          use3d: function () {
            var n = e.createElement("div");
            return t.getComputedStyle && t.getComputedStyle(n) && t.getComputedStyle(n).getPropertyValue("transform") && !(e.documentMode && e.documentMode < 11);
          }(),
          getTranslate: function getTranslate(t) {
            var e;
            return !(!t || !t.length) && {
              top: (e = t[0].getBoundingClientRect()).top || 0,
              left: e.left || 0,
              width: e.width,
              height: e.height,
              opacity: parseFloat(t.css("opacity"))
            };
          },
          setTranslate: function setTranslate(t, e) {
            var n = "",
                o = {};
            if (t && e) return void 0 === e.left && void 0 === e.top || (n = (void 0 === e.left ? t.position().left : e.left) + "px, " + (void 0 === e.top ? t.position().top : e.top) + "px", n = this.use3d ? "translate3d(" + n + ", 0px)" : "translate(" + n + ")"), void 0 !== e.scaleX && void 0 !== e.scaleY ? n += " scale(" + e.scaleX + ", " + e.scaleY + ")" : void 0 !== e.scaleX && (n += " scaleX(" + e.scaleX + ")"), n.length && (o.transform = n), void 0 !== e.opacity && (o.opacity = e.opacity), void 0 !== e.width && (o.width = e.width), void 0 !== e.height && (o.height = e.height), t.css(o);
          },
          animate: function animate(t, e, o, i, r) {
            var s,
                a = this;
            n.isFunction(o) && (i = o, o = null), a.stop(t), s = a.getTranslate(t), t.on(u, function (c) {
              (!c || !c.originalEvent || t.is(c.originalEvent.target) && "z-index" != c.originalEvent.propertyName) && (a.stop(t), n.isNumeric(o) && t.css("transition-duration", ""), n.isPlainObject(e) ? void 0 !== e.scaleX && void 0 !== e.scaleY && a.setTranslate(t, {
                top: e.top,
                left: e.left,
                width: s.width * e.scaleX,
                height: s.height * e.scaleY,
                scaleX: 1,
                scaleY: 1
              }) : !0 !== r && t.removeClass(e), n.isFunction(i) && i(c));
            }), n.isNumeric(o) && t.css("transition-duration", o + "ms"), n.isPlainObject(e) ? (void 0 !== e.scaleX && void 0 !== e.scaleY && (delete e.width, delete e.height, t.parent().hasClass("fancybox-slide--image") && t.parent().addClass("fancybox-is-scaling")), n.fancybox.setTranslate(t, e)) : t.addClass(e), t.data("timer", setTimeout(function () {
              t.trigger(u);
            }, o + 33));
          },
          stop: function stop(t, e) {
            t && t.length && (clearTimeout(t.data("timer")), e && t.trigger(u), t.off(u).css("transition-duration", ""), t.parent().removeClass("fancybox-is-scaling"));
          }
        }, n.fn.fancybox = function (t) {
          var e;
          return (e = (t = t || {}).selector || !1) ? n("body").off("click.fb-start", e).on("click.fb-start", e, {
            options: t
          }, h) : this.off("click.fb-start").on("click.fb-start", {
            items: this,
            options: t
          }, h), this;
        }, s.on("click.fb-start", "[data-fancybox]", h), s.on("click.fb-start", "[data-fancybox-trigger]", function (t) {
          n('[data-fancybox="' + n(this).attr("data-fancybox-trigger") + '"]').eq(n(this).attr("data-fancybox-index") || 0).trigger("click.fb-start", {
            $trigger: n(this)
          });
        }), function () {
          var t = null;
          s.on("mousedown mouseup focus blur", ".fancybox-button", function (e) {
            switch (e.type) {
              case "mousedown":
                t = n(this);
                break;

              case "mouseup":
                t = null;
                break;

              case "focusin":
                n(".fancybox-button").removeClass("fancybox-focus"), n(this).is(t) || n(this).is("[disabled]") || n(this).addClass("fancybox-focus");
                break;

              case "focusout":
                n(".fancybox-button").removeClass("fancybox-focus");
            }
          });
        }();
      }

      function h(t, e) {
        var o,
            i,
            r,
            s = [],
            a = 0;
        t && t.isDefaultPrevented() || (t.preventDefault(), e = e || {}, t && t.data && (e = d(t.data.options, e)), o = e.$target || n(t.currentTarget).trigger("blur"), (r = n.fancybox.getInstance()) && r.$trigger && r.$trigger.is(o) || (s = e.selector ? n(e.selector) : (i = o.attr("data-fancybox") || "") ? (s = t.data ? t.data.items : []).length ? s.filter('[data-fancybox="' + i + '"]') : n('[data-fancybox="' + i + '"]') : [o], (a = n(s).index(o)) < 0 && (a = 0), (r = n.fancybox.open(s, e, a)).$trigger = o));
      }
    }(window, document, t), function (t) {
      "use strict";

      var e = {
        youtube: {
          matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
          params: {
            autoplay: 1,
            autohide: 1,
            fs: 1,
            rel: 0,
            hd: 1,
            wmode: "transparent",
            enablejsapi: 1,
            html5: 1
          },
          paramPlace: 8,
          type: "iframe",
          url: "https://www.youtube-nocookie.com/embed/$4",
          thumb: "https://img.youtube.com/vi/$4/hqdefault.jpg"
        },
        vimeo: {
          matcher: /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
          params: {
            autoplay: 1,
            hd: 1,
            show_title: 1,
            show_byline: 1,
            show_portrait: 0,
            fullscreen: 1
          },
          paramPlace: 3,
          type: "iframe",
          url: "//player.vimeo.com/video/$2"
        },
        instagram: {
          matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
          type: "image",
          url: "//$1/p/$2/media/?size=l"
        },
        gmap_place: {
          matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
          type: "iframe",
          url: function url(t) {
            return "//maps.google." + t[2] + "/?ll=" + (t[9] ? t[9] + "&z=" + Math.floor(t[10]) + (t[12] ? t[12].replace(/^\//, "&") : "") : t[12] + "").replace(/\?/, "&") + "&output=" + (t[12] && t[12].indexOf("layer=c") > 0 ? "svembed" : "embed");
          }
        },
        gmap_search: {
          matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,
          type: "iframe",
          url: function url(t) {
            return "//maps.google." + t[2] + "/maps?q=" + t[5].replace("query=", "q=").replace("api=1", "") + "&output=embed";
          }
        }
      },
          n = function n(e, _n2, o) {
        if (e) return o = o || "", "object" === t.type(o) && (o = t.param(o, !0)), t.each(_n2, function (t, n) {
          e = e.replace("$" + t, n || "");
        }), o.length && (e += (e.indexOf("?") > 0 ? "&" : "?") + o), e;
      };

      t(document).on("objectNeedsType.fb", function (o, i, r) {
        var s,
            a,
            c,
            l,
            u,
            f,
            d,
            p = r.src || "",
            h = !1;
        s = t.extend(!0, {}, e, r.opts.media), t.each(s, function (e, o) {
          if (c = p.match(o.matcher)) {
            if (h = o.type, d = e, f = {}, o.paramPlace && c[o.paramPlace]) {
              "?" == (u = c[o.paramPlace])[0] && (u = u.substring(1)), u = u.split("&");

              for (var i = 0; i < u.length; ++i) {
                var s = u[i].split("=", 2);
                2 == s.length && (f[s[0]] = decodeURIComponent(s[1].replace(/\+/g, " ")));
              }
            }

            return l = t.extend(!0, {}, o.params, r.opts[e], f), p = "function" === t.type(o.url) ? o.url.call(this, c, l, r) : n(o.url, c, l), a = "function" === t.type(o.thumb) ? o.thumb.call(this, c, l, r) : n(o.thumb, c), "youtube" === e ? p = p.replace(/&t=((\d+)m)?(\d+)s/, function (t, e, n, o) {
              return "&start=" + ((n ? 60 * parseInt(n, 10) : 0) + parseInt(o, 10));
            }) : "vimeo" === e && (p = p.replace("&%23", "#")), !1;
          }
        }), h ? (r.opts.thumb || r.opts.$thumb && r.opts.$thumb.length || (r.opts.thumb = a), "iframe" === h && (r.opts = t.extend(!0, r.opts, {
          iframe: {
            preload: !1,
            attr: {
              scrolling: "no"
            }
          }
        })), t.extend(r, {
          type: h,
          src: p,
          origSrc: r.src,
          contentSource: d,
          contentType: "image" === h ? "image" : "gmap_place" == d || "gmap_search" == d ? "map" : "video"
        })) : p && (r.type = r.opts.defaultType);
      });
      var o = {
        youtube: {
          src: "https://www.youtube.com/iframe_api",
          class: "YT",
          loading: !1,
          loaded: !1
        },
        vimeo: {
          src: "https://player.vimeo.com/api/player.js",
          class: "Vimeo",
          loading: !1,
          loaded: !1
        },
        load: function load(t) {
          var e,
              n = this;
          this[t].loaded ? setTimeout(function () {
            n.done(t);
          }) : this[t].loading || (this[t].loading = !0, (e = document.createElement("script")).type = "text/javascript", e.src = this[t].src, "youtube" === t ? window.onYouTubeIframeAPIReady = function () {
            n[t].loaded = !0, n.done(t);
          } : e.onload = function () {
            n[t].loaded = !0, n.done(t);
          }, document.body.appendChild(e));
        },
        done: function done(e) {
          var n, o;
          "youtube" === e && delete window.onYouTubeIframeAPIReady, (n = t.fancybox.getInstance()) && (o = n.current.$content.find("iframe"), "youtube" === e && void 0 !== YT && YT ? new YT.Player(o.attr("id"), {
            events: {
              onStateChange: function onStateChange(t) {
                0 == t.data && n.next();
              }
            }
          }) : "vimeo" === e && void 0 !== Vimeo && Vimeo && new Vimeo.Player(o).on("ended", function () {
            n.next();
          }));
        }
      };
      t(document).on({
        "afterShow.fb": function afterShowFb(t, e, n) {
          e.group.length > 1 && ("youtube" === n.contentSource || "vimeo" === n.contentSource) && o.load(n.contentSource);
        }
      });
    }(t), function (t, e, n) {
      "use strict";

      var o = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || function (e) {
        return t.setTimeout(e, 1e3 / 60);
      },
          i = t.cancelAnimationFrame || t.webkitCancelAnimationFrame || t.mozCancelAnimationFrame || t.oCancelAnimationFrame || function (e) {
        t.clearTimeout(e);
      },
          r = function r(e) {
        var n = [];

        for (var o in e = (e = e.originalEvent || e || t.e).touches && e.touches.length ? e.touches : e.changedTouches && e.changedTouches.length ? e.changedTouches : [e]) {
          e[o].pageX ? n.push({
            x: e[o].pageX,
            y: e[o].pageY
          }) : e[o].clientX && n.push({
            x: e[o].clientX,
            y: e[o].clientY
          });
        }

        return n;
      },
          s = function s(t, e, n) {
        return e && t ? "x" === n ? t.x - e.x : "y" === n ? t.y - e.y : Math.sqrt(Math.pow(t.x - e.x, 2) + Math.pow(t.y - e.y, 2)) : 0;
      },
          a = function a(t) {
        if (t.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio,iframe') || n.isFunction(t.get(0).onclick) || t.data("selectable")) return !0;

        for (var e = 0, o = t[0].attributes, i = o.length; e < i; e++) {
          if ("data-fancybox-" === o[e].nodeName.substr(0, 14)) return !0;
        }

        return !1;
      },
          c = function c(e) {
        var n = t.getComputedStyle(e)["overflow-y"],
            o = t.getComputedStyle(e)["overflow-x"],
            i = ("scroll" === n || "auto" === n) && e.scrollHeight > e.clientHeight,
            r = ("scroll" === o || "auto" === o) && e.scrollWidth > e.clientWidth;
        return i || r;
      },
          l = function l(t) {
        for (var e = !1; !(e = c(t.get(0))) && (t = t.parent()).length && !t.hasClass("fancybox-stage") && !t.is("body");) {
          ;
        }

        return e;
      },
          u = function u(t) {
        this.instance = t, this.$bg = t.$refs.bg, this.$stage = t.$refs.stage, this.$container = t.$refs.container, this.destroy(), this.$container.on("touchstart.fb.touch mousedown.fb.touch", n.proxy(this, "ontouchstart"));
      };

      u.prototype.destroy = function () {
        this.$container.off(".fb.touch"), n(e).off(".fb.touch"), this.requestId && (i(this.requestId), this.requestId = null), this.tapped && (clearTimeout(this.tapped), this.tapped = null);
      }, u.prototype.ontouchstart = function (o) {
        var i = n(o.target),
            c = this.instance,
            u = c.current,
            f = u.$slide,
            d = u.$content,
            p = "touchstart" == o.type;

        if (p && this.$container.off("mousedown.fb.touch"), (!o.originalEvent || 2 != o.originalEvent.button) && f.length && i.length && !a(i) && !a(i.parent()) && (i.is("img") || !(o.originalEvent.clientX > i[0].clientWidth + i.offset().left))) {
          if (!u || c.isAnimating || u.$slide.hasClass("fancybox-animated")) return o.stopPropagation(), void o.preventDefault();
          this.realPoints = this.startPoints = r(o), this.startPoints.length && (u.touch && o.stopPropagation(), this.startEvent = o, this.canTap = !0, this.$target = i, this.$content = d, this.opts = u.opts.touch, this.isPanning = !1, this.isSwiping = !1, this.isZooming = !1, this.isScrolling = !1, this.canPan = c.canPan(), this.startTime = new Date().getTime(), this.distanceX = this.distanceY = this.distance = 0, this.canvasWidth = Math.round(f[0].clientWidth), this.canvasHeight = Math.round(f[0].clientHeight), this.contentLastPos = null, this.contentStartPos = n.fancybox.getTranslate(this.$content) || {
            top: 0,
            left: 0
          }, this.sliderStartPos = n.fancybox.getTranslate(f), this.stagePos = n.fancybox.getTranslate(c.$refs.stage), this.sliderStartPos.top -= this.stagePos.top, this.sliderStartPos.left -= this.stagePos.left, this.contentStartPos.top -= this.stagePos.top, this.contentStartPos.left -= this.stagePos.left, n(e).off(".fb.touch").on(p ? "touchend.fb.touch touchcancel.fb.touch" : "mouseup.fb.touch mouseleave.fb.touch", n.proxy(this, "ontouchend")).on(p ? "touchmove.fb.touch" : "mousemove.fb.touch", n.proxy(this, "ontouchmove")), n.fancybox.isMobile && e.addEventListener("scroll", this.onscroll, !0), ((this.opts || this.canPan) && (i.is(this.$stage) || this.$stage.find(i).length) || (i.is(".fancybox-image") && o.preventDefault(), n.fancybox.isMobile && i.parents(".fancybox-caption").length)) && (this.isScrollable = l(i) || l(i.parent()), n.fancybox.isMobile && this.isScrollable || o.preventDefault(), (1 === this.startPoints.length || u.hasError) && (this.canPan ? (n.fancybox.stop(this.$content), this.isPanning = !0) : this.isSwiping = !0, this.$container.addClass("fancybox-is-grabbing")), 2 === this.startPoints.length && "image" === u.type && (u.isLoaded || u.$ghost) && (this.canTap = !1, this.isSwiping = !1, this.isPanning = !1, this.isZooming = !0, n.fancybox.stop(this.$content), this.centerPointStartX = .5 * (this.startPoints[0].x + this.startPoints[1].x) - n(t).scrollLeft(), this.centerPointStartY = .5 * (this.startPoints[0].y + this.startPoints[1].y) - n(t).scrollTop(), this.percentageOfImageAtPinchPointX = (this.centerPointStartX - this.contentStartPos.left) / this.contentStartPos.width, this.percentageOfImageAtPinchPointY = (this.centerPointStartY - this.contentStartPos.top) / this.contentStartPos.height, this.startDistanceBetweenFingers = s(this.startPoints[0], this.startPoints[1]))));
        }
      }, u.prototype.onscroll = function (t) {
        this.isScrolling = !0, e.removeEventListener("scroll", this.onscroll, !0);
      }, u.prototype.ontouchmove = function (t) {
        void 0 === t.originalEvent.buttons || 0 !== t.originalEvent.buttons ? this.isScrolling ? this.canTap = !1 : (this.newPoints = r(t), (this.opts || this.canPan) && this.newPoints.length && this.newPoints.length && (this.isSwiping && !0 === this.isSwiping || t.preventDefault(), this.distanceX = s(this.newPoints[0], this.startPoints[0], "x"), this.distanceY = s(this.newPoints[0], this.startPoints[0], "y"), this.distance = s(this.newPoints[0], this.startPoints[0]), this.distance > 0 && (this.isSwiping ? this.onSwipe(t) : this.isPanning ? this.onPan() : this.isZooming && this.onZoom()))) : this.ontouchend(t);
      }, u.prototype.onSwipe = function (e) {
        var r,
            s = this,
            a = s.instance,
            c = s.isSwiping,
            l = s.sliderStartPos.left || 0;
        if (!0 !== c) "x" == c && (s.distanceX > 0 && (s.instance.group.length < 2 || 0 === s.instance.current.index && !s.instance.current.opts.loop) ? l += Math.pow(s.distanceX, .8) : s.distanceX < 0 && (s.instance.group.length < 2 || s.instance.current.index === s.instance.group.length - 1 && !s.instance.current.opts.loop) ? l -= Math.pow(-s.distanceX, .8) : l += s.distanceX), s.sliderLastPos = {
          top: "x" == c ? 0 : s.sliderStartPos.top + s.distanceY,
          left: l
        }, s.requestId && (i(s.requestId), s.requestId = null), s.requestId = o(function () {
          s.sliderLastPos && (n.each(s.instance.slides, function (t, e) {
            var o = e.pos - s.instance.currPos;
            n.fancybox.setTranslate(e.$slide, {
              top: s.sliderLastPos.top,
              left: s.sliderLastPos.left + o * s.canvasWidth + o * e.opts.gutter
            });
          }), s.$container.addClass("fancybox-is-sliding"));
        });else if (Math.abs(s.distance) > 10) {
          if (s.canTap = !1, a.group.length < 2 && s.opts.vertical ? s.isSwiping = "y" : a.isDragging || !1 === s.opts.vertical || "auto" === s.opts.vertical && n(t).width() > 800 ? s.isSwiping = "x" : (r = Math.abs(180 * Math.atan2(s.distanceY, s.distanceX) / Math.PI), s.isSwiping = r > 45 && r < 135 ? "y" : "x"), "y" === s.isSwiping && n.fancybox.isMobile && s.isScrollable) return void (s.isScrolling = !0);
          a.isDragging = s.isSwiping, s.startPoints = s.newPoints, n.each(a.slides, function (t, e) {
            var o, i;
            n.fancybox.stop(e.$slide), o = n.fancybox.getTranslate(e.$slide), i = n.fancybox.getTranslate(a.$refs.stage), e.$slide.css({
              transform: "",
              opacity: "",
              "transition-duration": ""
            }).removeClass("fancybox-animated").removeClass(function (t, e) {
              return (e.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ");
            }), e.pos === a.current.pos && (s.sliderStartPos.top = o.top - i.top, s.sliderStartPos.left = o.left - i.left), n.fancybox.setTranslate(e.$slide, {
              top: o.top - i.top,
              left: o.left - i.left
            });
          }), a.SlideShow && a.SlideShow.isActive && a.SlideShow.stop();
        }
      }, u.prototype.onPan = function () {
        var t = this;
        s(t.newPoints[0], t.realPoints[0]) < (n.fancybox.isMobile ? 10 : 5) ? t.startPoints = t.newPoints : (t.canTap = !1, t.contentLastPos = t.limitMovement(), t.requestId && i(t.requestId), t.requestId = o(function () {
          n.fancybox.setTranslate(t.$content, t.contentLastPos);
        }));
      }, u.prototype.limitMovement = function () {
        var t,
            e,
            n,
            o,
            i,
            r,
            s = this.canvasWidth,
            a = this.canvasHeight,
            c = this.distanceX,
            l = this.distanceY,
            u = this.contentStartPos,
            f = u.left,
            d = u.top,
            p = u.width,
            h = u.height;
        return i = p > s ? f + c : f, r = d + l, t = Math.max(0, .5 * s - .5 * p), e = Math.max(0, .5 * a - .5 * h), n = Math.min(s - p, .5 * s - .5 * p), o = Math.min(a - h, .5 * a - .5 * h), c > 0 && i > t && (i = t - 1 + Math.pow(-t + f + c, .8) || 0), c < 0 && i < n && (i = n + 1 - Math.pow(n - f - c, .8) || 0), l > 0 && r > e && (r = e - 1 + Math.pow(-e + d + l, .8) || 0), l < 0 && r < o && (r = o + 1 - Math.pow(o - d - l, .8) || 0), {
          top: r,
          left: i
        };
      }, u.prototype.limitPosition = function (t, e, n, o) {
        var i = this.canvasWidth,
            r = this.canvasHeight;
        return t = n > i ? (t = t > 0 ? 0 : t) < i - n ? i - n : t : Math.max(0, i / 2 - n / 2), {
          top: e = o > r ? (e = e > 0 ? 0 : e) < r - o ? r - o : e : Math.max(0, r / 2 - o / 2),
          left: t
        };
      }, u.prototype.onZoom = function () {
        var e = this,
            r = e.contentStartPos,
            a = r.width,
            c = r.height,
            l = r.left,
            u = r.top,
            f = s(e.newPoints[0], e.newPoints[1]) / e.startDistanceBetweenFingers,
            d = Math.floor(a * f),
            p = Math.floor(c * f),
            h = (a - d) * e.percentageOfImageAtPinchPointX,
            g = (c - p) * e.percentageOfImageAtPinchPointY,
            m = (e.newPoints[0].x + e.newPoints[1].x) / 2 - n(t).scrollLeft(),
            v = (e.newPoints[0].y + e.newPoints[1].y) / 2 - n(t).scrollTop(),
            y = m - e.centerPointStartX,
            b = {
          top: u + (g + (v - e.centerPointStartY)),
          left: l + (h + y),
          scaleX: f,
          scaleY: f
        };
        e.canTap = !1, e.newWidth = d, e.newHeight = p, e.contentLastPos = b, e.requestId && i(e.requestId), e.requestId = o(function () {
          n.fancybox.setTranslate(e.$content, e.contentLastPos);
        });
      }, u.prototype.ontouchend = function (t) {
        var o = this.isSwiping,
            s = this.isPanning,
            a = this.isZooming,
            c = this.isScrolling;
        if (this.endPoints = r(t), this.dMs = Math.max(new Date().getTime() - this.startTime, 1), this.$container.removeClass("fancybox-is-grabbing"), n(e).off(".fb.touch"), e.removeEventListener("scroll", this.onscroll, !0), this.requestId && (i(this.requestId), this.requestId = null), this.isSwiping = !1, this.isPanning = !1, this.isZooming = !1, this.isScrolling = !1, this.instance.isDragging = !1, this.canTap) return this.onTap(t);
        this.speed = 100, this.velocityX = this.distanceX / this.dMs * .5, this.velocityY = this.distanceY / this.dMs * .5, s ? this.endPanning() : a ? this.endZooming() : this.endSwiping(o, c);
      }, u.prototype.endSwiping = function (t, e) {
        var o = !1,
            i = this.instance.group.length,
            r = Math.abs(this.distanceX),
            s = "x" == t && i > 1 && (this.dMs > 130 && r > 10 || r > 50);
        this.sliderLastPos = null, "y" == t && !e && Math.abs(this.distanceY) > 50 ? (n.fancybox.animate(this.instance.current.$slide, {
          top: this.sliderStartPos.top + this.distanceY + 150 * this.velocityY,
          opacity: 0
        }, 200), o = this.instance.close(!0, 250)) : s && this.distanceX > 0 ? o = this.instance.previous(300) : s && this.distanceX < 0 && (o = this.instance.next(300)), !1 !== o || "x" != t && "y" != t || this.instance.centerSlide(200), this.$container.removeClass("fancybox-is-sliding");
      }, u.prototype.endPanning = function () {
        var t, e, o;
        this.contentLastPos && (!1 === this.opts.momentum || this.dMs > 350 ? (t = this.contentLastPos.left, e = this.contentLastPos.top) : (t = this.contentLastPos.left + 500 * this.velocityX, e = this.contentLastPos.top + 500 * this.velocityY), (o = this.limitPosition(t, e, this.contentStartPos.width, this.contentStartPos.height)).width = this.contentStartPos.width, o.height = this.contentStartPos.height, n.fancybox.animate(this.$content, o, 366));
      }, u.prototype.endZooming = function () {
        var t,
            e,
            o,
            i,
            r = this.instance.current,
            s = this.newWidth,
            a = this.newHeight;
        this.contentLastPos && (t = this.contentLastPos.left, i = {
          top: e = this.contentLastPos.top,
          left: t,
          width: s,
          height: a,
          scaleX: 1,
          scaleY: 1
        }, n.fancybox.setTranslate(this.$content, i), s < this.canvasWidth && a < this.canvasHeight ? this.instance.scaleToFit(150) : s > r.width || a > r.height ? this.instance.scaleToActual(this.centerPointStartX, this.centerPointStartY, 150) : (o = this.limitPosition(t, e, s, a), n.fancybox.animate(this.$content, o, 150)));
      }, u.prototype.onTap = function (e) {
        var o,
            i = this,
            s = n(e.target),
            a = i.instance,
            c = a.current,
            l = e && r(e) || i.startPoints,
            u = l[0] ? l[0].x - n(t).scrollLeft() - i.stagePos.left : 0,
            f = l[0] ? l[0].y - n(t).scrollTop() - i.stagePos.top : 0,
            d = function d(t) {
          var o = c.opts[t];
          if (n.isFunction(o) && (o = o.apply(a, [c, e])), o) switch (o) {
            case "close":
              a.close(i.startEvent);
              break;

            case "toggleControls":
              a.toggleControls();
              break;

            case "next":
              a.next();
              break;

            case "nextOrClose":
              a.group.length > 1 ? a.next() : a.close(i.startEvent);
              break;

            case "zoom":
              "image" == c.type && (c.isLoaded || c.$ghost) && (a.canPan() ? a.scaleToFit() : a.isScaledDown() ? a.scaleToActual(u, f) : a.group.length < 2 && a.close(i.startEvent));
          }
        };

        if ((!e.originalEvent || 2 != e.originalEvent.button) && (s.is("img") || !(u > s[0].clientWidth + s.offset().left))) {
          if (s.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container")) o = "Outside";else if (s.is(".fancybox-slide")) o = "Slide";else {
            if (!a.current.$content || !a.current.$content.find(s).addBack().filter(s).length) return;
            o = "Content";
          }

          if (i.tapped) {
            if (clearTimeout(i.tapped), i.tapped = null, Math.abs(u - i.tapX) > 50 || Math.abs(f - i.tapY) > 50) return this;
            d("dblclick" + o);
          } else i.tapX = u, i.tapY = f, c.opts["dblclick" + o] && c.opts["dblclick" + o] !== c.opts["click" + o] ? i.tapped = setTimeout(function () {
            i.tapped = null, a.isAnimating || d("click" + o);
          }, 500) : d("click" + o);

          return this;
        }
      }, n(e).on("onActivate.fb", function (t, e) {
        e && !e.Guestures && (e.Guestures = new u(e));
      }).on("beforeClose.fb", function (t, e) {
        e && e.Guestures && e.Guestures.destroy();
      });
    }(window, document, t), function (t, e) {
      "use strict";

      e.extend(!0, e.fancybox.defaults, {
        btnTpl: {
          slideShow: '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 5.4v13.2l11-6.6z"/></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M8.33 5.75h2.2v12.5h-2.2V5.75zm5.15 0h2.2v12.5h-2.2V5.75z"/></svg></button>'
        },
        slideShow: {
          autoStart: !1,
          speed: 3e3,
          progress: !0
        }
      });

      var n = function n(t) {
        this.instance = t, this.init();
      };

      e.extend(n.prototype, {
        timer: null,
        isActive: !1,
        $button: null,
        init: function init() {
          var t = this,
              n = t.instance,
              o = n.group[n.currIndex].opts.slideShow;
          t.$button = n.$refs.toolbar.find("[data-fancybox-play]").on("click", function () {
            t.toggle();
          }), n.group.length < 2 || !o ? t.$button.hide() : o.progress && (t.$progress = e('<div class="fancybox-progress"></div>').appendTo(n.$refs.inner));
        },
        set: function set(t) {
          var n = this.instance,
              o = n.current;
          o && (!0 === t || o.opts.loop || n.currIndex < n.group.length - 1) ? this.isActive && "video" !== o.contentType && (this.$progress && e.fancybox.animate(this.$progress.show(), {
            scaleX: 1
          }, o.opts.slideShow.speed), this.timer = setTimeout(function () {
            n.current.opts.loop || n.current.index != n.group.length - 1 ? n.next() : n.jumpTo(0);
          }, o.opts.slideShow.speed)) : (this.stop(), n.idleSecondsCounter = 0, n.showControls());
        },
        clear: function clear() {
          clearTimeout(this.timer), this.timer = null, this.$progress && this.$progress.removeAttr("style").hide();
        },
        start: function start() {
          var t = this.instance.current;
          t && (this.$button.attr("title", (t.opts.i18n[t.opts.lang] || t.opts.i18n.en).PLAY_STOP).removeClass("fancybox-button--play").addClass("fancybox-button--pause"), this.isActive = !0, t.isComplete && this.set(!0), this.instance.trigger("onSlideShowChange", !0));
        },
        stop: function stop() {
          var t = this.instance.current;
          this.clear(), this.$button.attr("title", (t.opts.i18n[t.opts.lang] || t.opts.i18n.en).PLAY_START).removeClass("fancybox-button--pause").addClass("fancybox-button--play"), this.isActive = !1, this.instance.trigger("onSlideShowChange", !1), this.$progress && this.$progress.removeAttr("style").hide();
        },
        toggle: function toggle() {
          this.isActive ? this.stop() : this.start();
        }
      }), e(t).on({
        "onInit.fb": function onInitFb(t, e) {
          e && !e.SlideShow && (e.SlideShow = new n(e));
        },
        "beforeShow.fb": function beforeShowFb(t, e, n, o) {
          var i = e && e.SlideShow;
          o ? i && n.opts.slideShow.autoStart && i.start() : i && i.isActive && i.clear();
        },
        "afterShow.fb": function afterShowFb(t, e, n) {
          var o = e && e.SlideShow;
          o && o.isActive && o.set();
        },
        "afterKeydown.fb": function afterKeydownFb(n, o, i, r, s) {
          var a = o && o.SlideShow;
          !a || !i.opts.slideShow || 80 !== s && 32 !== s || e(t.activeElement).is("button,a,input") || (r.preventDefault(), a.toggle());
        },
        "beforeClose.fb onDeactivate.fb": function beforeCloseFbOnDeactivateFb(t, e) {
          var n = e && e.SlideShow;
          n && n.stop();
        }
      }), e(t).on("visibilitychange", function () {
        var n = e.fancybox.getInstance(),
            o = n && n.SlideShow;
        o && o.isActive && (t.hidden ? o.clear() : o.set());
      });
    }(document, t), function (t, e) {
      "use strict";

      var n = function () {
        for (var e = [["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"], ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"], ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"], ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"], ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]], n = {}, o = 0; o < e.length; o++) {
          var i = e[o];

          if (i && i[1] in t) {
            for (var r = 0; r < i.length; r++) {
              n[e[0][r]] = i[r];
            }

            return n;
          }
        }

        return !1;
      }();

      if (n) {
        var o = {
          request: function request(e) {
            (e = e || t.documentElement)[n.requestFullscreen](e.ALLOW_KEYBOARD_INPUT);
          },
          exit: function exit() {
            t[n.exitFullscreen]();
          },
          toggle: function toggle(e) {
            e = e || t.documentElement, this.isFullscreen() ? this.exit() : this.request(e);
          },
          isFullscreen: function isFullscreen() {
            return Boolean(t[n.fullscreenElement]);
          },
          enabled: function enabled() {
            return Boolean(t[n.fullscreenEnabled]);
          }
        };
        e.extend(!0, e.fancybox.defaults, {
          btnTpl: {
            fullScreen: '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fsenter" title="{{FULL_SCREEN}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 16h3v3h2v-5H5zm3-8H5v2h5V5H8zm6 11h2v-3h3v-2h-5zm2-11V5h-2v5h5V8z"/></svg></button>'
          },
          fullScreen: {
            autoStart: !1
          }
        }), e(t).on(n.fullscreenchange, function () {
          var t = o.isFullscreen(),
              n = e.fancybox.getInstance();
          n && (n.current && "image" === n.current.type && n.isAnimating && (n.isAnimating = !1, n.update(!0, !0, 0), n.isComplete || n.complete()), n.trigger("onFullscreenChange", t), n.$refs.container.toggleClass("fancybox-is-fullscreen", t), n.$refs.toolbar.find("[data-fancybox-fullscreen]").toggleClass("fancybox-button--fsenter", !t).toggleClass("fancybox-button--fsexit", t));
        });
      }

      e(t).on({
        "onInit.fb": function onInitFb(t, e) {
          n ? e && e.group[e.currIndex].opts.fullScreen ? (e.$refs.container.on("click.fb-fullscreen", "[data-fancybox-fullscreen]", function (t) {
            t.stopPropagation(), t.preventDefault(), o.toggle();
          }), e.opts.fullScreen && !0 === e.opts.fullScreen.autoStart && o.request(), e.FullScreen = o) : e && e.$refs.toolbar.find("[data-fancybox-fullscreen]").hide() : e.$refs.toolbar.find("[data-fancybox-fullscreen]").remove();
        },
        "afterKeydown.fb": function afterKeydownFb(t, e, n, o, i) {
          e && e.FullScreen && 70 === i && (o.preventDefault(), e.FullScreen.toggle());
        },
        "beforeClose.fb": function beforeCloseFb(t, e) {
          e && e.FullScreen && e.$refs.container.hasClass("fancybox-is-fullscreen") && o.exit();
        }
      });
    }(document, t), function (t, e) {
      "use strict";

      var n = "fancybox-thumbs";
      e.fancybox.defaults = e.extend(!0, {
        btnTpl: {
          thumbs: '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14.59 14.59h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76H5.65V5.65z"/></svg></button>'
        },
        thumbs: {
          autoStart: !1,
          hideOnClose: !0,
          parentEl: ".fancybox-container",
          axis: "y"
        }
      }, e.fancybox.defaults);

      var o = function o(t) {
        this.init(t);
      };

      e.extend(o.prototype, {
        $button: null,
        $grid: null,
        $list: null,
        isVisible: !1,
        isActive: !1,
        init: function init(t) {
          var e = this,
              n = t.group,
              o = 0;
          e.instance = t, e.opts = n[t.currIndex].opts.thumbs, t.Thumbs = e, e.$button = t.$refs.toolbar.find("[data-fancybox-thumbs]");

          for (var i = 0, r = n.length; i < r && (n[i].thumb && o++, !(o > 1)); i++) {
            ;
          }

          o > 1 && e.opts ? (e.$button.removeAttr("style").on("click", function () {
            e.toggle();
          }), e.isActive = !0) : e.$button.hide();
        },
        create: function create() {
          var t,
              o = this.instance,
              i = this.opts.parentEl,
              r = [];
          this.$grid || (this.$grid = e('<div class="' + n + " " + n + "-" + this.opts.axis + '"></div>').appendTo(o.$refs.container.find(i).addBack().filter(i)), this.$grid.on("click", "a", function () {
            o.jumpTo(e(this).attr("data-index"));
          })), this.$list || (this.$list = e('<div class="' + n + '__list">').appendTo(this.$grid)), e.each(o.group, function (e, n) {
            (t = n.thumb) || "image" !== n.type || (t = n.src), r.push('<a href="javascript:;" tabindex="0" data-index="' + e + '"' + (t && t.length ? ' style="background-image:url(' + t + ')"' : 'class="fancybox-thumbs-missing"') + "></a>");
          }), this.$list[0].innerHTML = r.join(""), "x" === this.opts.axis && this.$list.width(parseInt(this.$grid.css("padding-right"), 10) + o.group.length * this.$list.children().eq(0).outerWidth(!0));
        },
        focus: function focus(t) {
          var e,
              n,
              o = this.$list,
              i = this.$grid;
          this.instance.current && (n = (e = o.children().removeClass("fancybox-thumbs-active").filter('[data-index="' + this.instance.current.index + '"]').addClass("fancybox-thumbs-active")).position(), "y" === this.opts.axis && (n.top < 0 || n.top > o.height() - e.outerHeight()) ? o.stop().animate({
            scrollTop: o.scrollTop() + n.top
          }, t) : "x" === this.opts.axis && (n.left < i.scrollLeft() || n.left > i.scrollLeft() + (i.width() - e.outerWidth())) && o.parent().stop().animate({
            scrollLeft: n.left
          }, t));
        },
        update: function update() {
          this.instance.$refs.container.toggleClass("fancybox-show-thumbs", this.isVisible), this.isVisible ? (this.$grid || this.create(), this.instance.trigger("onThumbsShow"), this.focus(0)) : this.$grid && this.instance.trigger("onThumbsHide"), this.instance.update();
        },
        hide: function hide() {
          this.isVisible = !1, this.update();
        },
        show: function show() {
          this.isVisible = !0, this.update();
        },
        toggle: function toggle() {
          this.isVisible = !this.isVisible, this.update();
        }
      }), e(t).on({
        "onInit.fb": function onInitFb(t, e) {
          var n;
          e && !e.Thumbs && (n = new o(e)).isActive && !0 === n.opts.autoStart && n.show();
        },
        "beforeShow.fb": function beforeShowFb(t, e, n, o) {
          var i = e && e.Thumbs;
          i && i.isVisible && i.focus(o ? 0 : 250);
        },
        "afterKeydown.fb": function afterKeydownFb(t, e, n, o, i) {
          var r = e && e.Thumbs;
          r && r.isActive && 71 === i && (o.preventDefault(), r.toggle());
        },
        "beforeClose.fb": function beforeCloseFb(t, e) {
          var n = e && e.Thumbs;
          n && n.isVisible && !1 !== n.opts.hideOnClose && n.$grid.hide();
        }
      });
    }(document, t), function (t, e) {
      "use strict";

      e.extend(!0, e.fancybox.defaults, {
        btnTpl: {
          share: '<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M2.55 19c1.4-8.4 9.1-9.8 11.9-9.8V5l7 7-7 6.3v-3.5c-2.8 0-10.5 2.1-11.9 4.2z"/></svg></button>'
        },
        share: {
          url: function url(t, e) {
            return !t.currentHash && "inline" !== e.type && "html" !== e.type && (e.origSrc || e.src) || window.location;
          },
          tpl: '<div class="fancybox-share"><h1>{{SHARE}}</h1><p><a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg><span>Facebook</span></a><a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg><span>Twitter</span></a><a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg><span>Pinterest</span></a></p><p><input class="fancybox-share__input" type="text" value="{{url_raw}}" onclick="select()" /></p></div>'
        }
      }), e(t).on("click", "[data-fancybox-share]", function () {
        var t,
            n,
            o = e.fancybox.getInstance(),
            i = o.current || null;
        i && ("function" === e.type(i.opts.share.url) && (t = i.opts.share.url.apply(i, [o, i])), n = i.opts.share.tpl.replace(/\{\{media\}\}/g, "image" === i.type ? encodeURIComponent(i.src) : "").replace(/\{\{url\}\}/g, encodeURIComponent(t)).replace(/\{\{url_raw\}\}/g, function (t) {
          var e = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#39;",
            "/": "&#x2F;",
            "`": "&#x60;",
            "=": "&#x3D;"
          };
          return String(t).replace(/[&<>"'`=\/]/g, function (t) {
            return e[t];
          });
        }(t)).replace(/\{\{descr\}\}/g, o.$caption ? encodeURIComponent(o.$caption.text()) : ""), e.fancybox.open({
          src: o.translate(o, n),
          type: "html",
          opts: {
            touch: !1,
            animationEffect: !1,
            afterLoad: function afterLoad(t, e) {
              o.$refs.container.one("beforeClose.fb", function () {
                t.close(null, 0);
              }), e.$content.find(".fancybox-share__button").click(function () {
                return window.open(this.href, "Share", "width=550, height=450"), !1;
              });
            },
            mobile: {
              autoFocus: !1
            }
          }
        }));
      });
    }(document, t), function (t, e, n) {
      "use strict";

      function o() {
        var e = t.location.hash.substr(1),
            n = e.split("-"),
            o = n.length > 1 && /^\+?\d+$/.test(n[n.length - 1]) && parseInt(n.pop(-1), 10) || 1,
            i = n.join("-");
        return {
          hash: e,
          index: o < 1 ? 1 : o,
          gallery: i
        };
      }

      function i(t) {
        "" !== t.gallery && n("[data-fancybox='" + n.escapeSelector(t.gallery) + "']").eq(t.index - 1).focus().trigger("click.fb-start");
      }

      function r(t) {
        var e, n;
        return !!t && "" !== (n = (e = t.current ? t.current.opts : t.opts).hash || (e.$orig ? e.$orig.data("fancybox") || e.$orig.data("fancybox-trigger") : "")) && n;
      }

      n.escapeSelector || (n.escapeSelector = function (t) {
        return (t + "").replace(/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g, function (t, e) {
          return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t;
        });
      }), n(function () {
        !1 !== n.fancybox.defaults.hash && (n(e).on({
          "onInit.fb": function onInitFb(t, e) {
            var n, i;
            !1 !== e.group[e.currIndex].opts.hash && (n = o(), (i = r(e)) && n.gallery && i == n.gallery && (e.currIndex = n.index - 1));
          },
          "beforeShow.fb": function beforeShowFb(n, o, i, s) {
            var a;
            i && !1 !== i.opts.hash && (a = r(o)) && (o.currentHash = a + (o.group.length > 1 ? "-" + (i.index + 1) : ""), t.location.hash !== "#" + o.currentHash && (s && !o.origHash && (o.origHash = t.location.hash), o.hashTimer && clearTimeout(o.hashTimer), o.hashTimer = setTimeout(function () {
              "replaceState" in t.history ? (t.history[s ? "pushState" : "replaceState"]({}, e.title, t.location.pathname + t.location.search + "#" + o.currentHash), s && (o.hasCreatedHistory = !0)) : t.location.hash = o.currentHash, o.hashTimer = null;
            }, 300)));
          },
          "beforeClose.fb": function beforeCloseFb(n, o, i) {
            i && !1 !== i.opts.hash && (clearTimeout(o.hashTimer), o.currentHash && o.hasCreatedHistory ? t.history.back() : o.currentHash && ("replaceState" in t.history ? t.history.replaceState({}, e.title, t.location.pathname + t.location.search + (o.origHash || "")) : t.location.hash = o.origHash), o.currentHash = null);
          }
        }), n(t).on("hashchange.fb", function () {
          var t = o(),
              e = null;
          n.each(n(".fancybox-container").get().reverse(), function (t, o) {
            var i = n(o).data("FancyBox");
            if (i && i.currentHash) return e = i, !1;
          }), e ? e.currentHash === t.gallery + "-" + t.index || 1 === t.index && e.currentHash == t.gallery || (e.currentHash = null, e.close()) : "" !== t.gallery && i(t);
        }), setTimeout(function () {
          n.fancybox.getInstance() || i(o());
        }, 50));
      });
    }(window, document, t), function (t, e) {
      "use strict";

      var n = new Date().getTime();
      e(t).on({
        "onInit.fb": function onInitFb(t, e, o) {
          e.$refs.stage.on("mousewheel DOMMouseScroll wheel MozMousePixelScroll", function (t) {
            var o = e.current,
                i = new Date().getTime();
            e.group.length < 2 || !1 === o.opts.wheel || "auto" === o.opts.wheel && "image" !== o.type || (t.preventDefault(), t.stopPropagation(), o.$slide.hasClass("fancybox-animated") || (t = t.originalEvent || t, i - n < 250 || (n = i, e[(-t.deltaY || -t.deltaX || t.wheelDelta || -t.detail) < 0 ? "next" : "previous"]())));
          });
        }
      });
    }(document, t);
  }).call(this, n(0));
}]);

/***/ }),

/***/ 1:
/*!**************************************!*\
  !*** multi ./resources/js/common.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/kalbekovnurpeyis/workspace/webserver/project/resources/js/common.js */"./resources/js/common.js");


/***/ })

/******/ });