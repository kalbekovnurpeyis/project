/*--------------  coin_sales1 start ------------*/
if ($('#coin_sales1').length) {
    var ctx = document.getElementById("coin_sales1").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "Sales",
                backgroundColor: "rgba(117, 19, 246, 0.1)",
                borderColor: '#0b76b6',
                data: [18, 41, 86, 49, 20, 35, 20, 50, 49, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
}
/*--------------  coin_sales1 End ------------*/

/*--------------  coin_sales2 start ------------*/
if ($('#coin_sales2').length) {
    var ctx = document.getElementById("coin_sales2").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "Sales",
                backgroundColor: "rgba(240, 180, 26, 0.1)",
                borderColor: '#F0B41A',
                data: [18, 41, 86, 49, 20, 65, 64, 50, 49, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
}
/*--------------  coin_sales2 End ------------*/

/*--------------  coin_sales3 start ------------*/
if ($('#coin_sales3').length) {
    var ctx = document.getElementById("coin_sales3").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "Sales",
                backgroundColor: "rgba(247, 163, 58, 0.1)",
                borderColor: '#fd9d24',
                fill: true,
                data: [18, 41, 50, 49, 20, 65, 50, 86, 20, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
}

/*--------------  coin_sales3 End ------------*/

/*--------------  overview-chart start ------------*/
if ($('#verview-shart').length) {
    var myConfig = {
        "type": "line",

        "scale-x": { //X-Axis
            "labels": ["0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100"],
            "label": {
                "font-size": 14,
                "offset-x": 0,
            },
            "item": { //Scale Items (scale values or labels)
                "font-size": 10,
            },
            "guide": { //Guides
                "visible": false,
                "line-style": "solid", //"solid", "dotted", "dashed", "dashdot"
                "alpha": 1
            }
        },
        "plot": { "aspect": "spline" },
        "series": [{
                "values": [20, 25, 30, 35, 45, 40, 40, 35, 25, 17, 40, 50],
                "line-color": "#F0B41A",
                /* "dotted" | "dashed" */
                "line-width": 5 /* in pixels */ ,
                "marker": { /* Marker object */
                    "background-color": "#D79D3B",
                    /* hexadecimal or RGB value */
                    "size": 5,
                    /* in pixels */
                    "border-color": "#D79D3B",
                    /* hexadecimal or RBG value */
                }
            },
            {
                "values": [40, 45, 30, 20, 30, 35, 45, 55, 40, 30, 55, 30],
                "line-color": "#0884D9",
                /* "dotted" | "dashed" */
                "line-width": 5 /* in pixels */ ,
                "marker": { /* Marker object */
                    "background-color": "#067dce",
                    /* hexadecimal or RGB value */
                    "size": 5,
                    /* in pixels */
                    "border-color": "#067dce",
                    /* hexadecimal or RBG value */
                }
            }
        ]
    };

    zingchart.render({
        id: 'verview-shart',
        data: myConfig,
        height: "100%",
        width: "100%"
    });
}

/*--------------  overview-chart END ------------*/

/*--------------  market status chart start ------------*/

if ($('#mvaluechart').length) {
    var ctx = document.getElementById('mvaluechart').getContext('2d');
    var myLineChart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "Market Value",
                backgroundColor: 'transparent',
                borderColor: '#6e00ff',
                borderWidth: 2,
                data: [0, 15, 30, 10, 25, 0, 30],
                pointBorderColor: "transparent",
                pointBorderWidth: 10
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            },
            scales: {
                yAxes: [{
                    display: !1
                }],
                xAxes: [{
                    display: !1
                }]
            }
        }
    });
}

if ($('#mvaluechart2').length) {
    var ctx = document.getElementById('mvaluechart2').getContext('2d');
    var myLineChart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "Market Value",
                backgroundColor: 'transparent',
                borderColor: '#6e00ff',
                borderWidth: 2,
                data: [0, 15, 30, 10, 25, 0, 50],
                pointBorderColor: "transparent",
                pointBorderWidth: 10
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            },
            scales: {
                yAxes: [{
                    display: !1
                }],
                xAxes: [{
                    display: !1
                }]
            }
        }
    });
}

if ($('#mvaluechart3').length) {
    var ctx = document.getElementById('mvaluechart3').getContext('2d');
    var myLineChart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "Agut"],
            datasets: [{
                label: "Market Value",
                backgroundColor: 'transparent',
                borderColor: '#6e00ff',
                borderWidth: 2,
                data: [0, 15, 40, 10, 25, 0, 30, 20],
                pointBorderColor: "transparent",
                pointBorderWidth: 10
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            },
            scales: {
                yAxes: [{
                    display: !1
                }],
                xAxes: [{
                    display: !1
                }]
            }
        }
    });
}

if ($('#mvaluechart4').length) {
    var ctx = document.getElementById('mvaluechart4').getContext('2d');
    var myLineChart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "Market Value",
                backgroundColor: 'transparent',
                borderColor: '#6e00ff',
                borderWidth: 2,
                data: [0, 30, 30, 10, 25, 0, 30],
                pointBorderColor: "transparent",
                pointBorderWidth: 10
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            },
            scales: {
                yAxes: [{
                    display: !1
                }],
                xAxes: [{
                    display: !1
                }]
            }
        }
    });
}

/*--------------  market status chart END ------------*/

/*--------------  visitor graph line chart start ------------*/
if ($('#visitor_graph').length) {

    Highcharts.chart('visitor_graph', {
        chart: {
            type: 'areaspline'
        },
        title: false,
        yAxis: {
            title: false,
            gridLineColor: '#fbf7f7',
            gridLineWidth: 1
        },
        xAxis: {
            gridLineColor: '#fbf7f7',
            gridLineWidth: 1
        },
        series: [{
                name: 'USD',
                data: [400, 470, 520, 500, 420, 350, 320, 400, 550, 600, 500, 420, 400],
                fillColor: 'rgba(76, 57, 249, 0.5)',
                lineColor: 'transparent'
            },
            {
                name: 'BTC',
                data: [450, 520, 550, 400, 450, 500, 400, 450, 500, 450, 400, 500, 450],
                fillColor: 'rgba(103, 13, 251, 0.5)',
                lineColor: 'transparent'
            }
        ]
    });
}
/*--------------  END visitor graph line chart start ------------*/

/*-------------- 1 line chart amchart start ------------*/
if ($('#amlinechart1').length) {
    var chart = AmCharts.makeChart("amlinechart1", {
        "type": "serial",
        "theme": "light",
        "marginRight": 20,
        "autoMarginOffset": 20,
        "dataDateFormat": "YYYY-MM-DD HH:NN",
        "dataProvider": [{
            "date": "2012-01-01",
            "value": 8
        }, {
            "date": "2012-01-02",
            "color": "#6e00ff",
            "value": 10
        }, {
            "date": "2012-01-03",
            "value": 12
        }, {
            "date": "2012-01-04",
            "value": 14
        }, {
            "date": "2012-01-05",
            "value": 11
        }, {
            "date": "2012-01-06",
            "value": 6
        }, {
            "date": "2012-01-07",
            "value": 7
        }, {
            "date": "2012-01-08",
            "value": 9
        }, {
            "date": "2012-01-09",
            "value": 13
        }, {
            "date": "2012-01-10",
            "value": 15
        }, {
            "date": "2012-01-11",
            "color": "#6e00ff",
            "value": 19
        }, {
            "date": "2012-01-12",
            "value": 21
        }, {
            "date": "2012-01-13",
            "value": 22
        }, {
            "date": "2012-01-14",
            "value": 20
        }, {
            "date": "2012-01-15",
            "value": 18
        }, {
            "date": "2012-01-16",
            "value": 14
        }, {
            "date": "2012-01-17",
            "color": "#6e00ff",
            "value": 16
        }, {
            "date": "2012-01-18",
            "value": 18
        }, {
            "date": "2012-01-19",
            "value": 17
        }, {
            "date": "2012-01-20",
            "value": 15
        }, {
            "date": "2012-01-21",
            "value": 12
        }, {
            "date": "2012-01-22",
            "color": "#6e00ff",
            "value": 10
        }, {
            "date": "2012-01-23",
            "value": 8
        }],
        "valueAxes": [{
            "axisAlpha": 0,
            "guides": [{
                "fillAlpha": 0.1,
                "fillColor": "#6e00ff",
                "lineAlpha": 0,
                "toValue": 16,
                "value": 10
            }],
            "position": "left",
            "tickLength": 0
        }],
        "graphs": [{
            "balloonText": "[[categories]]<br><b><span style='font-size:14px;'>value:[[value]]</span></b>",
            "bullet": "round",
            "dashLength": 3,
            "colorField": "color",
            "valueField": "value"
        }],
        "trendLines": [{
            "finalDate": "2012-01-11 12",
            "finalValue": 19,
            "initialDate": "2012-01-02 12",
            "initialValue": 10,
            "lineColor": "#6e00ff"
        }, {
            "finalDate": "2012-01-22 12",
            "finalValue": 10,
            "initialDate": "2012-01-17 12",
            "initialValue": 16,
            "lineColor": "#6e00ff"
        }],
        "chartScrollbar": {
            "scrollbarHeight": 2,
            "offset": -1,
            "backgroundAlpha": 0.2,
            "backgroundColor": "#8816FD",
            "selectedBackgroundColor": "#815FF5",
            "selectedBackgroundAlpha": 1
        },
        "chartCursor": {
            "fullWidth": true,
            "valueLineEabled": true,
            "valueLineBalloonEnabled": true,
            "valueLineAlpha": 0.5,
            "cursorAlpha": 0
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "minorGridAlpha": 0.1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": false
        }
    });

    chart.addListener("dataUpdated", zoomChart);

    function zoomChart() {
        chart.zoomToDates(new Date(2012, 0, 2), new Date(2012, 0, 13));
    }
}
/*-------------- 1 line chart amchart end ------------*/

/*-------------- 2 line chart amchart start ------------*/
if ($('#amlinechart2').length) {
    var chart = AmCharts.makeChart("amlinechart2", {
        "type": "serial",
        "theme": "light",
        "marginTop": 0,
        "marginRight": 20,
        "dataProvider": [{
            "year": "1950",
            "value": -0.307
        }, {
            "year": "1951",
            "value": -0.168
        }, {
            "year": "1952",
            "value": -0.073
        }, {
            "year": "1953",
            "value": -0.027
        }, {
            "year": "1954",
            "value": -0.251
        }, {
            "year": "1955",
            "value": -0.281
        }, {
            "year": "1956",
            "value": -0.348
        }, {
            "year": "1957",
            "value": -0.074
        }, {
            "year": "1958",
            "value": -0.011
        }, {
            "year": "1959",
            "value": -0.074
        }, {
            "year": "1960",
            "value": -0.124
        }, {
            "year": "1961",
            "value": -0.024
        }, {
            "year": "1962",
            "value": -0.022
        }, {
            "year": "1963",
            "value": 0
        }, {
            "year": "1964",
            "value": -0.296
        }, {
            "year": "1965",
            "value": -0.217
        }, {
            "year": "1966",
            "value": -0.147
        }, {
            "year": "1967",
            "value": -0.15
        }, {
            "year": "1968",
            "value": -0.16
        }, {
            "year": "1969",
            "value": -0.011
        }, {
            "year": "1970",
            "value": -0.068
        }, {
            "year": "1971",
            "value": -0.19
        }, {
            "year": "1972",
            "value": -0.056
        }, {
            "year": "1973",
            "value": 0.077
        }, {
            "year": "1974",
            "value": -0.213
        }, {
            "year": "1975",
            "value": -0.17
        }, {
            "year": "1976",
            "value": -0.254
        }, {
            "year": "1977",
            "value": 0.019
        }, {
            "year": "1978",
            "value": -0.063
        }, {
            "year": "1979",
            "value": 0.05
        }, {
            "year": "1980",
            "value": 0.077
        }, {
            "year": "1981",
            "value": 0.12
        }, {
            "year": "1982",
            "value": 0.011
        }, {
            "year": "1983",
            "value": 0.177
        }, {
            "year": "1984",
            "value": -0.021
        }, {
            "year": "1985",
            "value": -0.037
        }, {
            "year": "1986",
            "value": 0.03
        }, {
            "year": "1987",
            "value": 0.179
        }, {
            "year": "1988",
            "value": 0.18
        }, {
            "year": "1989",
            "value": 0.104
        }, {
            "year": "1990",
            "value": 0.255
        }, {
            "year": "1991",
            "value": 0.21
        }, {
            "year": "1992",
            "value": 0.065
        }, {
            "year": "1993",
            "value": 0.11
        }, {
            "year": "1994",
            "value": 0.172
        }, {
            "year": "1995",
            "value": 0.269
        }, {
            "year": "1996",
            "value": 0.141
        }, {
            "year": "1997",
            "value": 0.353
        }, {
            "year": "1998",
            "value": 0.548
        }, {
            "year": "1999",
            "value": 0.298
        }, {
            "year": "2000",
            "value": 0.267
        }, {
            "year": "2001",
            "value": 0.411
        }, {
            "year": "2002",
            "value": 0.462
        }, {
            "year": "2003",
            "value": 0.47
        }, {
            "year": "2004",
            "value": 0.445
        }, {
            "year": "2005",
            "value": 0.47
        }],
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left"
        }],
        "graphs": [{
            "id": "g1",
            "balloonText": "[[categories]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#9656e7",
            "lineThickness": 2,
            "negativeLineColor": "#c69cfd",
            "type": "smoothedLine",
            "valueField": "value"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "gridAlpha": 0,
            "color": "#8816FD",
            "scrollbarHeight": 55,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#8816FD",
            "graphFillAlpha": 0,
            "autoGridCount": true,
            "selectedGraphFillAlpha": 0,
            "graphLineAlpha": 0.2,
            "graphLineColor": "#c2c2c2",
            "selectedGraphLineColor": "#9f46fc",
            "selectedGraphLineAlpha": 1

        },
        "chartCursor": {
            "categoryBalloonDateFormat": "YYYY",
            "cursorAlpha": 0,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "valueLineAlpha": 0.5,
            "fullWidth": true
        },
        "dataDateFormat": "YYYY",
        "categoryField": "year",
        "categoryAxis": {
            "minPeriod": "YYYY",
            "parseDates": true,
            "minorGridAlpha": 0.1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": false
        }
    });

    chart.addListener("rendered", zoomChart);
    if (chart.zoomChart) {
        chart.zoomChart();
    }

    function zoomChart() {
        chart.zoomToIndexes(Math.round(chart.dataProvider.length * 0.4), Math.round(chart.dataProvider.length * 0.55));
    }
}
/*-------------- 2 line chart amchart end ------------*/

/*-------------- 3 line chart amchart start ------------*/
if ($('#amlinechart3').length) {
    var chartData = generateChartData();
    var chart = AmCharts.makeChart("amlinechart3", {
        "type": "serial",
        "theme": "light",
        "marginRight": 20,
        "autoMarginOffset": 20,
        "marginTop": 7,
        "dataProvider": chartData,
        "valueAxes": [{
            "axisAlpha": 0.2,
            "dashLength": 1,
            "position": "left"
        }],
        "mouseWheelZoomEnabled": true,
        "graphs": [{
            "id": "g1",
            "balloonText": "[[value]]",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 50,
            "title": "red line",
            "valueField": "visits",
            "useLineColorForBulletBorder": true,
            "balloon": {
                "drop": true
            }
        }],
        "chartScrollbar": {
            "autoGridCount": true,
            "graph": "g1",
            "scrollbarHeight": 40,
            "color": "#fff",
            "selectedBackgroundAlpha": 1,
            "selectedBackgroundColor": "#815BF6",
            "selectedGraphFillAlpha": 0,
            "selectedGraphFillColor": "#8918FE",
            "graphLineAlpha": 0.2,
            "graphLineColor": "#c2c2c2",
            "selectedGraphLineColor": "#fff",
            "selectedGraphLineAlpha": 1
        },
        "chartCursor": {
            "limitToGraph": "g1"
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "axisColor": "#DADADA",
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": false
        }
    });

    chart.addListener("rendered", zoomChart);
    zoomChart();

    // this method is called when chart is first inited as we listen for "rendered" event
    function zoomChart() {
        // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
        chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
    }


    // generate some random data, quite different range

    // generate some random data, quite different range
    function generateChartData() {
        var chartData = [];
        var firstDate = new Date();
        firstDate.setDate(firstDate.getDate() - 5);
        var visits = 1200;
        for (var i = 0; i < 1000; i++) {
            // we create date objects here. In your data, you can have date strings
            // and then set format of your dates using chart.dataDateFormat property,
            // however when possible, use date objects, as this will speed up chart rendering.
            var newDate = new Date(firstDate);
            newDate.setDate(newDate.getDate() + i);

            visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

            chartData.push({
                date: newDate,
                visits: visits
            });
        }
        return chartData;
    }
}
/*-------------- 3 line chart amchart end ------------*/

/*-------------- 4 line chart amchart start ------------*/
if ($('#amlinechart4').length) {
    var chart = AmCharts.makeChart("amlinechart4", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "useGraphSettings": true
        },
        "dataProvider": [{
            "year": 1930,
            "italy": 1,
            "germany": 5,
            "uk": 3
        }, {
            "year": 1934,
            "italy": 1,
            "germany": 2,
            "uk": 6
        }, {
            "year": 1938,
            "italy": 2,
            "germany": 3,
            "uk": 1
        }, {
            "year": 1950,
            "italy": 3,
            "germany": 4,
            "uk": 1
        }, {
            "year": 1954,
            "italy": 5,
            "germany": 1,
            "uk": 2
        }, {
            "year": 1958,
            "italy": 3,
            "germany": 2,
            "uk": 1
        }, {
            "year": 1962,
            "italy": 1,
            "germany": 2,
            "uk": 3
        }, {
            "year": 1966,
            "italy": 2,
            "germany": 1,
            "uk": 5
        }, {
            "year": 1970,
            "italy": 3,
            "germany": 5,
            "uk": 2
        }, {
            "year": 1974,
            "italy": 4,
            "germany": 3,
            "uk": 6
        }, {
            "year": 1978,
            "italy": 1,
            "germany": 2,
            "uk": 4
        }],
        "startDuration": 0.5,
        "graphs": [{
            "balloonText": "place taken by Italy in [[categories]]: [[value]]",
            "bullet": "round",
            "hidden": true,
            "title": "Italy",
            "valueField": "italy",
            "fillAlphas": 0,
            "lineColor": "#31ef98",
            "lineThickness": 2,
            "negativeLineColor": "#17e285",
        }, {
            "balloonText": "place taken by Germany in [[categories]]: [[value]]",
            "bullet": "round",
            "title": "Germany",
            "valueField": "germany",
            "fillAlphas": 0,
            "lineColor": "#9656e7",
            "lineThickness": 2,
            "negativeLineColor": "#c69cfd"
        }, {
            "balloonText": "place taken by UK in [[categories]]: [[value]]",
            "bullet": "round",
            "title": "United Kingdom",
            "valueField": "uk",
            "fillAlphas": 0,
            "lineColor": "#31aeef",
            "lineThickness": 2,
            "negativeLineColor": "#31aeef",
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "year",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "fillAlpha": 0.05,
            "fillColor": "#000000",
            "gridAlpha": 0,
            "position": "top"
        },
        "export": {
            "enabled": false
        }
    });
}
/*-------------- 4 line chart amchart end ------------*/

/*-------------- 5 line chart amchart start ------------*/
if ($('#amlinechart5').length) {
    var chart = AmCharts.makeChart("amlinechart5", {
        "type": "serial",
        "theme": "light",
        "marginRight": 20,
        "marginTop": 17,
        "autoMarginOffset": 20,
        "dataProvider": [{
            "date": "2012-03-01",
            "price": 20
        }, {
            "date": "2012-03-02",
            "price": 75
        }, {
            "date": "2012-03-03",
            "price": 15
        }, {
            "date": "2012-03-04",
            "price": 75
        }, {
            "date": "2012-03-05",
            "price": 158
        }, {
            "date": "2012-03-06",
            "price": 57
        }, {
            "date": "2012-03-07",
            "price": 107
        }, {
            "date": "2012-03-08",
            "price": 89
        }, {
            "date": "2012-03-09",
            "price": 75
        }, {
            "date": "2012-03-10",
            "price": 132
        }, {
            "date": "2012-03-11",
            "price": 158
        }, {
            "date": "2012-03-12",
            "price": 56
        }, {
            "date": "2012-03-13",
            "price": 169
        }, {
            "date": "2012-03-14",
            "price": 24
        }, {
            "date": "2012-03-15",
            "price": 147
        }],
        "valueAxes": [{
            "logarithmic": true,
            "dashLength": 1,
            "guides": [{
                "dashLength": 6,
                "inside": true,
                "label": "average",
                "lineAlpha": 1,
                "value": 90.4
            }],
            "position": "left"
        }],
        "graphs": [{
            "bullet": "round",
            "id": "g1",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 7,
            "lineThickness": 2,
            "title": "Price",
            "type": "smoothedLine",
            "useLineColorForBulletBorder": true,
            "valueField": "price"
        }],
        "chartScrollbar": {},
        "chartCursor": {
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "valueLineAlpha": 0.5,
            "fullWidth": true,
            "cursorAlpha": 0.05
        },
        "dataDateFormat": "YYYY-MM-DD",
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true
        },
        "export": {
            "enabled": false
        }
    });

    chart.addListener("dataUpdated", zoomChart);

    function zoomChart() {
        chart.zoomToDates(new Date(2012, 2, 2), new Date(2012, 2, 10));
    }
}
/*-------------- 5 line chart amchart end ------------*/

/*-------------- 6 line chart chartjs start ------------*/
if ($('#seolinechart1').length) {
    var ctx = document.getElementById("seolinechart1").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "Likes",
                backgroundColor: "rgba(104, 124, 247, 0.6)",
                borderColor: '#8596fe',
                data: [18, 41, 86, 49, 20, 35, 20, 50, 49, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            }
        }
    });
}
/*-------------- 6 line chart chartjs end ------------*/

/*-------------- 7 line chart chartjs start ------------*/
if ($('#seolinechart2').length) {
    var ctx = document.getElementById("seolinechart2").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "Share",
                backgroundColor: "rgba(96, 241, 205, 0.2)",
                borderColor: '#3de5bb',
                data: [18, 41, 86, 49, 20, 35, 20, 50, 49, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            }
        }
    });
}
/*-------------- 7 line chart chartjs end ------------*/

/*-------------- 8 line chart chartjs start ------------*/
if ($('#seolinechart3').length) {
    var ctx = document.getElementById("seolinechart3").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "Share",
                backgroundColor: "rgba(96, 241, 205, 0)",
                borderColor: '#fff',
                data: [18, 41, 86, 49, 20, 35, 20, 50, 49, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            }
        }
    });
}
/*-------------- 8 line chart chartjs end ------------*/

/*-------------- 9 line chart chartjs start ------------*/
if ($('#seolinechart4').length) {
    var ctx = document.getElementById("seolinechart4").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "January", "February", "March", "April", "May"],
            datasets: [{
                label: "New user",
                backgroundColor: "rgba(96, 241, 205, 0)",
                borderColor: '#fff',
                data: [18, 41, 86, 49, 20, 35, 20, 50, 49, 30, 45, 25],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: !0,
                        maxTicksLimit: 5,
                        padding: 0
                    },
                    gridLines: {
                        drawTicks: !1,
                        display: !1
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 0,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            }
        }
    });
}
/*-------------- 9 line chart chartjs end ------------*/

/*-------------- 10 line chart amchart start ------------*/
if ($('#user-statistics').length) {
    var chart = AmCharts.makeChart("user-statistics", {
        "type": "serial",
        "theme": "light",
        "marginRight": 0,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff",
                "type": "smoothedLine"
            },
            "fillAlphas": 0.2,
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "red line",
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
        "chartCursor": {
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 0,
            "zoomable": false,
            "valueZoomable": true,
            "valueLineAlpha": 0.5
        },
        "valueScrollbar": {
            "autoGridCount": true,
            "color": "#5E72F3",
            "scrollbarHeight": 30
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": false
        },
        "dataProvider": [{
            "date": "2012-07-27",
            "value": 13
        }, {
            "date": "2012-07-28",
            "value": 11
        }, {
            "date": "2012-07-29",
            "value": 15
        }, {
            "date": "2012-07-30",
            "value": 16
        }, {
            "date": "2012-07-31",
            "value": 18
        }, {
            "date": "2012-08-01",
            "value": 13
        }, {
            "date": "2012-08-02",
            "value": 22
        }, {
            "date": "2012-08-03",
            "value": 23
        }, {
            "date": "2012-08-04",
            "value": 20
        }, {
            "date": "2012-08-05",
            "value": 17
        }, {
            "date": "2012-08-06",
            "value": 16
        }, {
            "date": "2012-08-07",
            "value": 18
        }, {
            "date": "2012-08-08",
            "value": 21
        }, {
            "date": "2012-08-09",
            "value": 26
        }, {
            "date": "2012-08-10",
            "value": 24
        }, {
            "date": "2012-08-11",
            "value": 29
        }, {
            "date": "2012-08-12",
            "value": 32
        }, {
            "date": "2012-08-13",
            "value": 18
        }, {
            "date": "2012-08-14",
            "value": 24
        }, {
            "date": "2012-08-15",
            "value": 22
        }, {
            "date": "2012-08-16",
            "value": 18
        }, {
            "date": "2012-08-17",
            "value": 19
        }, {
            "date": "2012-08-18",
            "value": 14
        }, {
            "date": "2012-08-19",
            "value": 15
        }, {
            "date": "2012-08-20",
            "value": 12
        }, {
            "date": "2012-08-21",
            "value": 8
        }, {
            "date": "2012-08-22",
            "value": 9
        }, {
            "date": "2012-08-23",
            "value": 8
        }, {
            "date": "2012-08-24",
            "value": 7
        }, {
            "date": "2012-08-25",
            "value": 5
        }, {
            "date": "2012-08-26",
            "value": 11
        }, {
            "date": "2012-08-27",
            "value": 13
        }, {
            "date": "2012-08-28",
            "value": 18
        }, {
            "date": "2012-08-29",
            "value": 20
        }, {
            "date": "2012-08-30",
            "value": 29
        }, {
            "date": "2012-08-31",
            "value": 33
        }, {
            "date": "2012-09-01",
            "value": 42
        }, {
            "date": "2012-09-02",
            "value": 35
        }, {
            "date": "2012-09-03",
            "value": 31
        }, {
            "date": "2012-09-04",
            "value": 47
        }, {
            "date": "2012-09-05",
            "value": 52
        }, {
            "date": "2012-09-06",
            "value": 46
        }, {
            "date": "2012-09-07",
            "value": 41
        }, {
            "date": "2012-09-08",
            "value": 43
        }, {
            "date": "2012-09-09",
            "value": 40
        }, {
            "date": "2012-09-10",
            "value": 39
        }, {
            "date": "2012-09-11",
            "value": 34
        }, {
            "date": "2012-09-12",
            "value": 29
        }, {
            "date": "2012-09-13",
            "value": 34
        }, {
            "date": "2012-09-14",
            "value": 37
        }, {
            "date": "2012-09-15",
            "value": 42
        }, {
            "date": "2012-09-16",
            "value": 49
        }, {
            "date": "2012-09-17",
            "value": 46
        }, {
            "date": "2012-09-18",
            "value": 47
        }, {
            "date": "2012-09-19",
            "value": 55
        }, {
            "date": "2012-09-20",
            "value": 59
        }, {
            "date": "2012-09-21",
            "value": 58
        }, {
            "date": "2012-09-22",
            "value": 57
        }, {
            "date": "2012-09-23",
            "value": 61
        }, {
            "date": "2012-09-24",
            "value": 59
        }, {
            "date": "2012-09-25",
            "value": 67
        }, {
            "date": "2012-09-26",
            "value": 65
        }, {
            "date": "2012-09-27",
            "value": 61
        }, {
            "date": "2012-09-28",
            "value": 66
        }, {
            "date": "2012-09-29",
            "value": 69
        }, {
            "date": "2012-09-30",
            "value": 71
        }, {
            "date": "2012-10-01",
            "value": 67
        }, {
            "date": "2012-10-02",
            "value": 63
        }, {
            "date": "2012-10-03",
            "value": 46
        }, {
            "date": "2012-10-04",
            "value": 32
        }, {
            "date": "2012-10-05",
            "value": 21
        }, {
            "date": "2012-10-06",
            "value": 18
        }, {
            "date": "2012-10-07",
            "value": 21
        }, {
            "date": "2012-10-08",
            "value": 28
        }, {
            "date": "2012-10-09",
            "value": 27
        }, {
            "date": "2012-10-10",
            "value": 36
        }, {
            "date": "2012-10-11",
            "value": 33
        }, {
            "date": "2012-10-12",
            "value": 31
        }, {
            "date": "2012-10-13",
            "value": 30
        }, {
            "date": "2012-10-14",
            "value": 34
        }, {
            "date": "2012-10-15",
            "value": 38
        }, {
            "date": "2012-10-16",
            "value": 37
        }, {
            "date": "2012-10-17",
            "value": 44
        }, {
            "date": "2012-10-18",
            "value": 49
        }, {
            "date": "2012-10-19",
            "value": 53
        }, {
            "date": "2012-10-20",
            "value": 57
        }, {
            "date": "2012-10-21",
            "value": 60
        }, {
            "date": "2012-10-22",
            "value": 61
        }, {
            "date": "2012-10-23",
            "value": 69
        }, {
            "date": "2012-10-24",
            "value": 67
        }, {
            "date": "2012-10-25",
            "value": 72
        }, {
            "date": "2012-10-26",
            "value": 77
        }, {
            "date": "2012-10-27",
            "value": 75
        }, {
            "date": "2012-10-28",
            "value": 70
        }, {
            "date": "2012-10-29",
            "value": 72
        }, {
            "date": "2012-10-30",
            "value": 70
        }, {
            "date": "2012-10-31",
            "value": 72
        }, {
            "date": "2012-11-01",
            "value": 73
        }, {
            "date": "2012-11-02",
            "value": 67
        }, {
            "date": "2012-11-03",
            "value": 68
        }, {
            "date": "2012-11-04",
            "value": 65
        }, {
            "date": "2012-11-05",
            "value": 71
        }, {
            "date": "2012-11-06",
            "value": 75
        }, {
            "date": "2012-11-07",
            "value": 74
        }, {
            "date": "2012-11-08",
            "value": 71
        }, {
            "date": "2012-11-09",
            "value": 76
        }, {
            "date": "2012-11-10",
            "value": 77
        }, {
            "date": "2012-11-11",
            "value": 81
        }, {
            "date": "2012-11-12",
            "value": 83
        }, {
            "date": "2012-11-13",
            "value": 80
        }, {
            "date": "2012-11-14",
            "value": 81
        }, {
            "date": "2012-11-15",
            "value": 87
        }, {
            "date": "2012-11-16",
            "value": 82
        }, {
            "date": "2012-11-17",
            "value": 86
        }, {
            "date": "2012-11-18",
            "value": 80
        }, {
            "date": "2012-11-19",
            "value": 87
        }, {
            "date": "2012-11-20",
            "value": 83
        }, {
            "date": "2012-11-21",
            "value": 85
        }, {
            "date": "2012-11-22",
            "value": 84
        }, {
            "date": "2012-11-23",
            "value": 82
        }, {
            "date": "2012-11-24",
            "value": 73
        }, {
            "date": "2012-11-25",
            "value": 71
        }, {
            "date": "2012-11-26",
            "value": 75
        }, {
            "date": "2012-11-27",
            "value": 79
        }, {
            "date": "2012-11-28",
            "value": 70
        }, {
            "date": "2012-11-29",
            "value": 73
        }, {
            "date": "2012-11-30",
            "value": 61
        }, {
            "date": "2012-12-01",
            "value": 62
        }, {
            "date": "2012-12-02",
            "value": 66
        }, {
            "date": "2012-12-03",
            "value": 65
        }, {
            "date": "2012-12-04",
            "value": 73
        }, {
            "date": "2012-12-05",
            "value": 79
        }, {
            "date": "2012-12-06",
            "value": 78
        }, {
            "date": "2012-12-07",
            "value": 78
        }, {
            "date": "2012-12-08",
            "value": 78
        }, {
            "date": "2012-12-09",
            "value": 74
        }, {
            "date": "2012-12-10",
            "value": 73
        }, {
            "date": "2012-12-11",
            "value": 75
        }, {
            "date": "2012-12-12",
            "value": 70
        }, {
            "date": "2012-12-13",
            "value": 77
        }, {
            "date": "2012-12-14",
            "value": 67
        }, {
            "date": "2012-12-15",
            "value": 62
        }, {
            "date": "2012-12-16",
            "value": 64
        }, {
            "date": "2012-12-17",
            "value": 61
        }, {
            "date": "2012-12-18",
            "value": 59
        }, {
            "date": "2012-12-19",
            "value": 53
        }, {
            "date": "2012-12-20",
            "value": 54
        }, {
            "date": "2012-12-21",
            "value": 56
        }, {
            "date": "2012-12-22",
            "value": 59
        }, {
            "date": "2012-12-23",
            "value": 58
        }, {
            "date": "2012-12-24",
            "value": 55
        }, {
            "date": "2012-12-25",
            "value": 52
        }, {
            "date": "2012-12-26",
            "value": 54
        }, {
            "date": "2012-12-27",
            "value": 50
        }, {
            "date": "2012-12-28",
            "value": 50
        }, {
            "date": "2012-12-29",
            "value": 51
        }, {
            "date": "2012-12-30",
            "value": 52
        }, {
            "date": "2012-12-31",
            "value": 58
        }, {
            "date": "2013-01-01",
            "value": 60
        }, {
            "date": "2013-01-02",
            "value": 67
        }, {
            "date": "2013-01-03",
            "value": 64
        }, {
            "date": "2013-01-04",
            "value": 66
        }, {
            "date": "2013-01-05",
            "value": 60
        }, {
            "date": "2013-01-06",
            "value": 63
        }, {
            "date": "2013-01-07",
            "value": 61
        }, {
            "date": "2013-01-08",
            "value": 60
        }, {
            "date": "2013-01-09",
            "value": 65
        }, {
            "date": "2013-01-10",
            "value": 75
        }, {
            "date": "2013-01-11",
            "value": 77
        }, {
            "date": "2013-01-12",
            "value": 78
        }, {
            "date": "2013-01-13",
            "value": 70
        }, {
            "date": "2013-01-14",
            "value": 70
        }, {
            "date": "2013-01-15",
            "value": 73
        }, {
            "date": "2013-01-16",
            "value": 71
        }, {
            "date": "2013-01-17",
            "value": 74
        }, {
            "date": "2013-01-18",
            "value": 78
        }, {
            "date": "2013-01-19",
            "value": 85
        }, {
            "date": "2013-01-20",
            "value": 82
        }, {
            "date": "2013-01-21",
            "value": 83
        }, {
            "date": "2013-01-22",
            "value": 88
        }, {
            "date": "2013-01-23",
            "value": 85
        }, {
            "date": "2013-01-24",
            "value": 85
        }, {
            "date": "2013-01-25",
            "value": 80
        }, {
            "date": "2013-01-26",
            "value": 87
        }, {
            "date": "2013-01-27",
            "value": 84
        }, {
            "date": "2013-01-28",
            "value": 83
        }, {
            "date": "2013-01-29",
            "value": 84
        }, {
            "date": "2013-01-30",
            "value": 81
        }]
    });
}

/*-------------- 10 line chart amchart end ------------*/

/*-------------- 11 line chart amchart start ------------*/
if ($('#salesanalytic').length) {

    var chart = AmCharts.makeChart("salesanalytic", {
        "type": "serial",
        "theme": "light",
        "dataDateFormat": "YYYY-MM-DD",
        "precision": 2,
        "valueAxes": [{
            "id": "v1",
            "title": "Sales",
            "position": "left",
            "autoGridCount": false,
            "labelFunction": function(value) {
                return "$" + Math.round(value) + "M";
            }
        }, {
            "id": "v2",
            "title": "Duration",
            "gridAlpha": 0,
            "position": "right",
            "autoGridCount": false
        }],
        "graphs": [{
            "id": "g3",
            "valueAxis": "v1",
            "lineColor": "#F3F8FB",
            "fillColors": "#F3F8FB",
            "fillAlphas": 1,
            "type": "column",
            "title": "Actual Sales",
            "valueField": "sales2",
            "clustered": false,
            "columnWidth": 0.5,
            "legendValueText": "$[[value]]M",
            "balloonText": "[[title]]<br /><small style='font-size: 130%'>$[[value]]M</small>"
        }, {
            "id": "g4",
            "valueAxis": "v1",
            "lineColor": "#5C6DF4",
            "fillColors": "#5C6DF4",
            "fillAlphas": 1,
            "type": "column",
            "title": "Target Sales",
            "valueField": "sales1",
            "clustered": false,
            "columnWidth": 0.3,
            "legendValueText": "$[[value]]M",
            "balloonText": "[[title]]<br /><small style='font-size: 130%'>$[[value]]M</small>"
        }, {
            "id": "g1",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "lineColor": "#815FF6",
            "type": "smoothedLine",
            "title": "Duration",
            "useLineColorForBulletBorder": true,
            "valueField": "market1",
            "balloonText": "[[title]]<br /><small style='font-size: 130%'>[[value]]</small>"
        }, {
            "id": "g2",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "lineColor": "#ffe598",
            "type": "smoothedLine",
            "dashLength": 5,
            "title": "Market Days ALL",
            "useLineColorForBulletBorder": true,
            "valueField": "market2",
            "balloonText": "[[title]]<br /><small style='font-size: 130%'>[[value]]</small>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis": false,
            "offset": 50,
            "scrollbarHeight": 45,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.5,
            "selectedBackgroundColor": "#f9f9f9",
            "graphFillAlpha": 0.1,
            "graphLineAlpha": 0.4,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#95a1f9"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 0,
            "valueLineAlpha": 0.2
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true,
            "color": "#5C6DF4"
        },
        "legend": {
            "useGraphSettings": true,
            "position": "top"
        },
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "export": {
            "enabled": false
        },
        "dataProvider": [{
            "date": "2013-01-16",
            "market1": 51,
            "market2": 55,
            "sales1": 5,
            "sales2": 8
        }, {
            "date": "2013-01-17",
            "market1": 64,
            "market2": 70,
            "sales1": 5,
            "sales2": 6
        }, {
            "date": "2013-01-18",
            "market1": 65,
            "market2": 45,
            "sales1": 8,
            "sales2": 12
        }, {
            "date": "2013-01-19",
            "market1": 73,
            "market2": 75,
            "sales1": 7,
            "sales2": 8
        }, {
            "date": "2013-01-20",
            "market1": 65,
            "market2": 70,
            "sales1": 7,
            "sales2": 10
        }, {
            "date": "2013-01-21",
            "market1": 65,
            "market2": 55,
            "sales1": 9,
            "sales2": 12
        }, {
            "date": "2013-01-22",
            "market1": 68,
            "market2": 62,
            "sales1": 5,
            "sales2": 7
        }, {
            "date": "2013-01-23",
            "market1": 75,
            "market2": 80,
            "sales1": 7,
            "sales2": 10
        }, {
            "date": "2013-01-24",
            "market1": 75,
            "market2": 65,
            "sales1": 7,
            "sales2": 9
        }, {
            "date": "2013-01-25",
            "market1": 75,
            "market2": 69,
            "sales1": 8,
            "sales2": 10
        }, {
            "date": "2013-01-26",
            "market1": 55,
            "market2": 68,
            "sales1": 6,
            "sales2": 7
        }, {
            "date": "2013-01-27",
            "market1": 67,
            "market2": 70,
            "sales1": 3,
            "sales2": 4
        }, {
            "date": "2013-01-28",
            "market1": 62,
            "market2": 59,
            "sales1": 5,
            "sales2": 7
        }, {
            "date": "2013-01-29",
            "market1": 62,
            "market2": 56,
            "sales1": 5,
            "sales2": 8
        }, {
            "date": "2013-01-30",
            "market1": 71,
            "market2": 69,
            "sales1": 4,
            "sales2": 7
        }]
    });
}
//------------echarts2

/*-------------- 11 line chart amchart end ------------*/
/*--------------  coin distrubution chart END ------------*/
if ($('#coin_distribution').length) {

    zingchart.THEME = "classic";

    var myConfig = {
        "globals": {
            "font-family": "Roboto"
        },
        "graphset": [{
                "type": "pie",
                "background-color": "#fff",
                "legend": {
                    "background-color": "none",
                    "border-width": 0,
                    "shadow": false,
                    "layout": "float",
                    "margin": "auto auto 16% auto",
                    "marker": {
                        "border-radius": 3,
                        "border-width": 0
                    },
                    "item": {
                        "color": "%backgroundcolor"
                    }
                },
                "plotarea": {
                    "background-color": "#FFFFFF",
                    "border-color": "#DFE1E3",
                    "margin": "25% 8%"
                },
                "labels": [{
                    "x": "45%",
                    "y": "47%",
                    "width": "10%",
                    "text": "340 Coin",
                    "font-size": 17,
                    "font-weight": 700
                }],
                "plot": {
                    "size": 70,
                    "slice": 90,
                    "margin-right": 0,
                    "border-width": 0,
                    "shadow": 0,
                    "value-box": {
                        "visible": true
                    },
                    "tooltip": {
                        "text": "%v USD",
                        "shadow": false,
                        "border-radius": 2
                    }
                },
                "series": [{
                        "values": [1355460],
                        "text": "Bitcoin",
                        "background-color": "#4cff63"
                    },
                    {
                        "values": [1585218],
                        "text": "LiteCoin",
                        "background-color": "#fd9c21"
                    },
                    {
                        "values": [1064598],
                        "text": "Euthorium",
                        "background-color": "#2c13f8"
                    }
                ]
            }

        ]
    };

    zingchart.render({
        id: 'coin_distribution',
        data: myConfig,
    });
}
/*--------------  coin distrubution chart END ------------*/

/*-------------- 1 Pie chart amchart start ------------*/
if ($('#ampiechart1').length) {
    var chart = AmCharts.makeChart("ampiechart1", {
        "type": "pie",
        "labelRadius": -35,
        "labelText": "[[percents]]%",
        "dataProvider": [{
            "country": "Lithuania",
            "litres": 501.9,
            "backgroundColor": "#815DF6"
        }, {
            "country": "Czech Republic",
            "litres": 301.9,
            "backgroundColor": "#67B7DC"
        }, {
            "country": "Ireland",
            "litres": 201.1,
            "backgroundColor": "#9c82f4"
        }, {
            "country": "The Netherlands",
            "litres": 150,
            "backgroundColor": "#FDD400"
        }],
        "color": "#fff",
        "colorField": "backgroundColor",
        "valueField": "litres",
        "titleField": "country"
    });
}

/*-------------- 1 Pie chart amchart end ------------*/

/*-------------- 2 Pie chart amchart start ------------*/
if ($('#ampiechart2').length) {
    var chart = AmCharts.makeChart("ampiechart2", {
        "type": "pie",
        "theme": "light",
        "labelRadius": -65,
        "labelText": "[[title]]%",
        "dataProvider": [{
            "title": "New",
            "value": 4852
        }, {
            "title": "Returning",
            "value": 9899
        }],
        "titleField": "title",
        "valueField": "value",
        "export": {
            "enabled": false
        },
        "color": "#fff"
    });
}
/*-------------- 2 Pie chart amchart end ------------*/

/*-------------- 3 Pie chart amchart start ------------*/
var chart;
var legend;
var selected;

var types = [{
    type: "Fossil Energy",
    percent: 70,
    color: "#ff9e01",
    subs: [{
        type: "Oil",
        percent: 15
    }, {
        type: "Coal",
        percent: 35
    }, {
        type: "Nuclear",
        percent: 20
    }]
}, {
    type: "Green Energy",
    percent: 30,
    color: "#6E4FD1",
    subs: [{
        type: "Hydro",
        percent: 15
    }, {
        type: "Wind",
        percent: 10
    }, {
        type: "Other",
        percent: 5
    }]
}];

function generateChartData() {
    var chartData = [];
    for (var i = 0; i < types.length; i++) {
        if (i == selected) {
            for (var x = 0; x < types[i].subs.length; x++) {
                chartData.push({
                    type: types[i].subs[x].type,
                    percent: types[i].subs[x].percent,
                    color: types[i].color,
                    pulled: true
                });
            }
        } else {
            chartData.push({
                type: types[i].type,
                percent: types[i].percent,
                color: types[i].color,
                id: i
            });
        }
    }
    return chartData;
}

if ($('#ampiechart3').length) {
    AmCharts.makeChart("ampiechart3", {
        "type": "pie",
        "theme": "light",
        "labelRadius": -35,
        "labelText": "[[percents]]%",
        "dataProvider": generateChartData(),
        "balloonText": "[[title]]: [[value]]",
        "titleField": "type",
        "valueField": "percent",
        "outlineColor": "#FFFFFF",
        "outlineAlpha": 0.8,
        "outlineThickness": 2,
        "colorField": "color",
        "color": "#fff",
        "pulledField": "pulled",
        "titles": [{
            "text": "Click a slice to see the details"
        }],
        "listeners": [{
            "event": "clickSlice",
            "method": function(event) {
                var chart = event.chart;
                if (event.dataItem.dataContext.id != undefined) {
                    selected = event.dataItem.dataContext.id;
                } else {
                    selected = undefined;
                }
                chart.dataProvider = generateChartData();
                chart.validateData();
            }
        }],
        "export": {
            "enabled": false
        }
    });
}

/*-------------- 3 Pie chart amchart end ------------*/

/*-------------- 4 Pie chart highcharts start ------------*/
if ($('#highpiechart4').length) {
    var pieColors = (function() {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());

    // Build the chart
    Highcharts.chart('highpiechart4', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Dollar market Values, 2018'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                colors: pieColors,
                dataLabels: {
                    style: { "color": "contrast", "fontSize": "11px", "fontWeight": "bold", "textOutline": "" },
                    enabled: true,
                    format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                    distance: -50,
                    filter: {
                        property: 'percentage',
                        operator: '>',
                        value: 4
                    }
                }
            }
        },
        series: [{
            name: 'Share',
            data: [
                { name: 'USD', y: 61.41 },
                { name: 'BTC', y: 11.84 },
                { name: 'TCN', y: 10.85 }
            ]
        }]
    });
}
/*-------------- 4 Pie chart highcharts end ------------*/

/*-------------- 5 Pie chart highcharts start ------------*/
if ($('#highpiechart5').length) {
    Highcharts.chart('highpiechart5', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Dollar market Values, 2018'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#444',
                        "textOutline": ""
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'USB',
                y: 61.41,
                sliced: true,
                selected: true
            }, {
                name: 'BTC',
                y: 11.84
            }, {
                name: 'ETC',
                y: 10.85
            }]
        }]
    });
}

/*-------------- 5 Pie chart highcharts end ------------*/

/*-------------- 6 Pie chart highcharts start ------------*/
if ($('#highpiechart6').length) {
    Highcharts.chart('highpiechart6', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: '<br>Values<br>',
            align: 'center',
            verticalAlign: 'middle',
            y: 40
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -30,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textOutline: 0
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '65%']
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            innerSize: '55%',
            data: [
                ['USD', 58.9],
                ['BTC', 13.29],
                ['TCN', 13],
                {
                    name: 'Other',
                    y: 7.61,
                    dataLabels: {
                        enabled: false
                    }
                }
            ]
        }]
    });
}
/*-------------- 6 Pie chart highcharts end ------------*/

/*-------------- 7 Pie chart chartjs start ------------*/
if ($('#seolinechart8').length) {
    var ctx = document.getElementById("seolinechart8").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnut',
        // The data for our dataset
        data: {
            labels: ["FB", "TW", "G+", "INS"],
            datasets: [{
                backgroundColor: [
                    "#8919FE",
                    "#12C498",
                    "#F8CB3F",
                    "#E36D68"
                ],
                borderColor: '#fff',
                data: [810, 410, 260, 150],
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: true
            },
            animation: {
                easing: "easeInOutBack"
            }
        }
    });
}
/*-------------- 7 Pie chart chartjs end ------------*/
/*--------------  coin_sales4 bar chart start ------------*/
if ($('#coin_sales4').length) {
    var ctx = document.getElementById("coin_sales4").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: {
            labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
            datasets: [{
                label: "Sales",
                data: [250, 320, 380, 330, 420, 250, 180, 250, 100, 300],
                backgroundColor: [
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#3a3afb',
                    '#8416fe'
                ]
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "#cccccc",
                        beginAtZero: !0,
                        padding: 0
                    },
                    gridLines: {
                        zeroLineColor: "transparent"
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: !1
                    },
                    ticks: {
                        beginAtZero: !0,
                        padding: 0,
                        fontColor: "#cccccc"
                    }
                }]
            }
        }
    });
}

/*--------------  coin_sales4 bar chart End ------------*/

/*--------------  coin_sales5 bar chart start ------------*/
if ($('#coin_sales5').length) {
    var ctx = document.getElementById("coin_sales5").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: {
            labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
            datasets: [{
                label: "Sales",
                data: [250, 220, 380, 130, 420, 230, 180, 220, 150, 300],
                backgroundColor: [
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#3a3afb',
                    '#8416fe'
                ]
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "#cccccc",
                        beginAtZero: !0,
                        padding: 0
                    },
                    gridLines: {
                        zeroLineColor: "transparent"
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: !1
                    },
                    ticks: {
                        beginAtZero: !0,
                        padding: 0,
                        fontColor: "#cccccc"
                    }
                }]
            }
        }
    });
}

/*--------------  coin_sales5 bar chart End ------------*/

/*--------------  coin_sales6 bar chart start ------------*/
if ($('#coin_sales6').length) {
    var ctx = document.getElementById("coin_sales6").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: {
            labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
            datasets: [{
                label: "Sales",
                data: [250, 320, 380, 120, 420, 530, 180, 250, 80, 250],
                backgroundColor: [
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#3a3afb',
                    '#8416fe'
                ]
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "#cccccc",
                        beginAtZero: !0,
                        padding: 0
                    },
                    gridLines: {
                        zeroLineColor: "transparent"
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: !1
                    },
                    ticks: {
                        beginAtZero: !0,
                        padding: 0,
                        fontColor: "#cccccc"
                    }
                }]
            }
        }
    });
}

/*--------------  coin_sales6 bar chart End ------------*/

/*--------------  coin_sales7 bar chart start ------------*/
if ($('#coin_sales7').length) {
    var ctx = document.getElementById("coin_sales7").getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: {
            labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
            datasets: [{
                label: "Sales",
                data: [100, 300, 350, 350, 420, 150, 300, 250, 250, 300],
                backgroundColor: [
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#8416fe',
                    '#3a3afb',
                    '#3a3afb',
                    '#8416fe'
                ]
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            animation: {
                easing: "easeInOutBack"
            },
            scales: {
                yAxes: [{
                    display: !1,
                    ticks: {
                        fontColor: "#cccccc",
                        beginAtZero: !0,
                        padding: 0
                    },
                    gridLines: {
                        zeroLineColor: "transparent"
                    }
                }],
                xAxes: [{
                    display: !1,
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: !1
                    },
                    ticks: {
                        beginAtZero: !0,
                        padding: 0,
                        fontColor: "#cccccc"
                    }
                }]
            }
        }
    });
}

/*--------------  coin_sales7 bar chart End ------------*/

/*--------------  bar chart 08 amchart start ------------*/
if ($('#ambarchart1').length) {
    var chart = AmCharts.makeChart("ambarchart1", {
        "theme": "light",
        "type": "serial",
        "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 4,
            "color": "#fff"
        },
        "dataProvider": [{
            "country": "USA",
            "year2004": 3.5,
            "year2005": 4.2,
            "color": "#bfbffd",
            "color2": "#7474F0"
        }, {
            "country": "UK",
            "year2004": 1.7,
            "year2005": 3.1,
            "color": "#bfbffd",
            "color2": "#7474F0"
        }, {
            "country": "Canada",
            "year2004": 2.8,
            "year2005": 2.9,
            "color": "#bfbffd",
            "color2": "#7474F0"
        }, {
            "country": "Japan",
            "year2004": 2.6,
            "year2005": 2.3,
            "color": "#bfbffd",
            "color2": "#7474F0"
        }, {
            "country": "France",
            "year2004": 1.4,
            "year2005": 2.1,
            "color": "#bfbffd",
            "color2": "#7474F0"
        }, {
            "country": "Brazil",
            "year2004": 2.6,
            "year2005": 4.9,
            "color": "#bfbffd",
            "color2": "#7474F0"
        }],
        "valueAxes": [{
            "unit": "%",
            "position": "left",
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "GDP grow in [[categories]] (2017): <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "fillColorsField": "color",
            "lineAlpha": 0.2,
            "title": "2017",
            "type": "column",
            "valueField": "year2004"
        }, {
            "balloonText": "GDP grow in [[categories]] (2018): <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "fillColorsField": "color2",
            "lineAlpha": 0.2,
            "title": "2018",
            "type": "column",
            "clustered": false,
            "columnWidth": 0.5,
            "valueField": "year2005"
        }],
        "plotAreaFillAlphas": 0.1,
        "categoryField": "country",
        "categoryAxis": {
            "gridPosition": "start"
        },
        "export": {
            "enabled": false
        }

    });
}

/*--------------  bar chart 08 amchart END ------------*/
/*--------------  bar chart 09 amchart start ------------*/
if ($('#ambarchart2').length) {
    var chart = AmCharts.makeChart("ambarchart2", {
        "type": "serial",
        "addClassNames": true,
        "theme": "light",
        "autoMargins": false,
        "marginLeft": 30,
        "marginRight": 8,
        "marginTop": 10,
        "marginBottom": 26,
        "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
        },

        "dataProvider": [{
            "year": 2013,
            "income": 23.5,
            "expenses": 21.1,
            "color": "#7474f0"
        }, {
            "year": 2014,
            "income": 26.2,
            "expenses": 30.5,
            "color": "#7474f0"
        }, {
            "year": 2015,
            "income": 30.1,
            "expenses": 34.9,
            "color": "#7474f0"
        }, {
            "year": 2016,
            "income": 29.5,
            "expenses": 31.1,
            "color": "#7474f0"
        }, {
            "year": 2017,
            "income": 30.6,
            "expenses": 28.2,
            "dashLengthLine": 5,
            "color": "#7474f0"
        }, {
            "year": 2018,
            "income": 34.1,
            "expenses": 32.9,
            "dashLengthColumn": 5,
            "alpha": 0.2,
            "additional": "(projection)"
        }],
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:12px;'>[[title]] in [[categories]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
            "fillAlphas": 1,
            "fillColorsField": "color",
            "title": "Income",
            "type": "column",
            "valueField": "income",
            "dashLengthField": "dashLengthColumn"
        }, {
            "id": "graph2",
            "balloonText": "<span style='font-size:12px;'>[[title]] in [[categories]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "lineColor": "#AA59FE",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "Expenses",
            "valueField": "expenses",
            "dashLengthField": "dashLengthLine"
        }],
        "categoryField": "year",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 0
        },
        "export": {
            "enabled": false
        }
    });
}

/*--------------  bar chart 09 amchart END ------------*/
/*--------------  bar chart 10 amchart start ------------*/
if ($('#ambarchart3').length) {
    var chart = AmCharts.makeChart("ambarchart3", {
        "type": "serial",
        "theme": "light",
        "categoryField": "year",
        "rotate": true,
        "startDuration": 1,
        "categoryAxis": {
            "gridPosition": "start",
            "position": "left"
        },
        "trendLines": [],
        "graphs": [{
                "balloonText": "Income:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "income",
                "fillColorsField": "color"
            },
            {
                "balloonText": "Expenses:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-2",
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses",
                "fillColorsField": "color2"
            }
        ],
        "guides": [],
        "valueAxes": [{
            "id": "ValueAxis-1",
            "position": "top",
            "axisAlpha": 0
        }],
        "allLabels": [],
        "balloon": {},
        "titles": [],
        "dataProvider": [{
                "year": 2014,
                "income": 23.5,
                "expenses": 18.1,
                "color": "#7474f0",
                "color2": "#C5C5FD"
            },
            {
                "year": 2015,
                "income": 26.2,
                "expenses": 22.8,
                "color": "#7474f0",
                "color2": "#C5C5FD"
            },
            {
                "year": 2016,
                "income": 30.1,
                "expenses": 23.9,
                "color": "#7474f0",
                "color2": "#C5C5FD"
            },
            {
                "year": 2017,
                "income": 29.5,
                "expenses": 25.1,
                "color": "#7474f0",
                "color2": "#C5C5FD"
            },
            {
                "year": 2018,
                "income": 24.6,
                "expenses": 25,
                "color": "#7474f0",
                "color2": "#C5C5FD"
            }
        ],
        "export": {
            "enabled": false
        }

    });
}
/*--------------  bar chart 10 amchart END ------------*/
/*--------------  bar chart 11 amchart start ------------*/
if ($('#ambarchart4').length) {
    var chart = AmCharts.makeChart("ambarchart4", {
        "type": "serial",
        "theme": "light",
        "marginRight": 70,
        "dataProvider": [{
            "country": "USA",
            "visits": 3025,
            "color": "#8918FE"
        }, {
            "country": "China",
            "visits": 1882,
            "color": "#7474F0"
        }, {
            "country": "Japan",
            "visits": 1809,
            "color": "#C5C5FD"
        }, {
            "country": "Germany",
            "visits": 1322,
            "color": "#952FFE"
        }, {
            "country": "UK",
            "visits": 1122,
            "color": "#7474F0"
        }, {
            "country": "France",
            "visits": 1114,
            "color": "#CBCBFD"
        }, {
            "country": "India",
            "visits": 984,
            "color": "#FD9C21"
        }, {
            "country": "Spain",
            "visits": 711,
            "color": "#0D8ECF"
        }, {
            "country": "Netherlands",
            "visits": 665,
            "color": "#0D52D1"
        }, {
            "country": "Russia",
            "visits": 580,
            "color": "#2A0CD0"
        }, {
            "country": "South Korea",
            "visits": 443,
            "color": "#8A0CCF"
        }, {
            "country": "Canada",
            "visits": 441,
            "color": "#9F43FE"
        }],
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "title": false
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<b>[[categories]]: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "visits"
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "country",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45
        },
        "export": {
            "enabled": false
        }

    });
}
/*--------------  bar chart 11 amchart END ------------*/
/*--------------  bar chart 12 amchart start ------------*/
if ($('#ambarchart5').length) {
    var chart = AmCharts.makeChart("ambarchart5", {
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
            "name": "John",
            "points": 35654,
            "color": "#7F8DA9"
        }, {
            "name": "Damon",
            "points": 65456,
            "color": "#FEC514"
        }, {
            "name": "Patrick",
            "points": 45724,
            "color": "#952FFE"
        }, {
            "name": "Mark",
            "points": 23654,
            "color": "#8282F1"
        }, {
            "name": "Natasha",
            "points": 35654,
            "color": "#2599D4"
        }, {
            "name": "Adell",
            "points": 55456,
            "color": "#2563D6"
        }, {
            "name": "Alessandro",
            "points": 13654,
            "color": "#9524D4"
        }],
        "valueAxes": [{
            "maximum": 80000,
            "minimum": 0,
            "axisAlpha": 0,
            "dashLength": 4,
            "position": "left"
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[categories]]: <b>[[value]]</b></span>",
            "bulletOffset": 10,
            "bulletSize": 52,
            "colorField": "color",
            "cornerRadiusTop": 8,
            "customBulletField": "bullet",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "type": "column",
            "valueField": "points"
        }],
        "marginTop": 0,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 0,
        "autoMargins": false,
        "categoryField": "name",
        "categoryAxis": {
            "axisAlpha": 0,
            "gridAlpha": 0,
            "inside": true,
            "tickLength": 10,
            "color": "#fff"
        },
        "export": {
            "enabled": false
        }
    });
}
/*--------------  bar chart 12 amchart END ------------*/
/*--------------  bar chart 13 amchart start ------------*/
if ($('#ambarchart6').length) {
    var chart = AmCharts.makeChart("ambarchart6", {
        "type": "serial",
        "theme": "light",
        "handDrawn": true,
        "handDrawScatter": 3,
        "legend": {
            "useGraphSettings": true,
            "markerSize": 12,
            "valueWidth": 0,
            "verticalGap": 0
        },
        "dataProvider": [{
            "year": 2014,
            "income": 23.5,
            "expenses": 18.1,
            "color": "#952FFE"
        }, {
            "year": 2015,
            "income": 26.2,
            "expenses": 22.8,
            "color": "#5182DE"
        }, {
            "year": 2016,
            "income": 30.1,
            "expenses": 23.9,
            "color": "#8282F1"
        }, {
            "year": 2017,
            "income": 29.5,
            "expenses": 25.1,
            "color": "#B369FE"
        }, {
            "year": 2018,
            "income": 24.6,
            "expenses": 25,
            "color": "#51ADDD"
        }],
        "valueAxes": [{
            "minorGridAlpha": 0.08,
            "minorGridEnabled": true,
            "position": "top",
            "axisAlpha": 0
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[title]] in [[categories]]:<b>[[value]]</b></span>",
            "title": "Income",
            "type": "column",
            "fillAlphas": 1,
            "fillColorsField": "color",
            "valueField": "income"
        }, {
            "balloonText": "<span style='font-size:13px;'>[[title]] in [[categories]]:<b>[[value]]</b></span>",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "lineColor": "#AA59FE",
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "fillAlphas": 0,
            "lineThickness": 2,
            "lineAlpha": 1,
            "bulletSize": 7,
            "title": "Expenses",
            "valueField": "expenses"
        }],
        "rotate": true,
        "categoryField": "year",
        "categoryAxis": {
            "gridPosition": "start"
        },
        "export": {
            "enabled": false
        }

    });
}

/*--------------  bar chart 13 amchart END ------------*/

/*--------------  bar chart 14 highchart start ------------*/
if ($('#socialads').length) {

    Highcharts.chart('socialads', {
        chart: {
            type: 'column'
        },
        title: false,
        xAxis: {
            categories: ['FB', 'TW', 'INS', 'G+', 'LINKD']
        },
        colors: ['#F5CA3F', '#E5726D', '#12C599', '#5F73F2'],
        yAxis: {
            min: 0,
            title: false
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'column'
            }
        },
        series: [{
                name: 'Closed',
                data: [51, 48, 64, 48, 84]
            }, {
                name: 'Hold',
                data: [83, 84, 53, 81, 88]
            }, {
                name: 'Pending',
                data: [93, 84, 53, 53, 48]
            },
            {
                name: 'Active',
                data: [430, 312, 348, 254, 258]
            }
        ]
    });
}
/*--------------  bar chart 14 highchart END ------------*/