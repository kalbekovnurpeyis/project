<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image as ImageInt;

class Page extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = ['title', 'slug', 'content', 'image'];

    /**
     * @return Page[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getPages()
    {
        return static::all();
    }
}
