<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Intervention\Image\Facades\Image as ImageInt;
use Illuminate\Http\UploadedFile;

class Images extends Model
{
    protected $fillable = ['image', 'parent', 'parent_id'];

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return BelongsTo
     */
    public function designInterior(): BelongsTo
    {
        return $this->belongsTo(DesignInterior::class);
    }

    /**
     * @param string $image
     */
    public static function removeImage(string $image = null) {
        if ($image !== null) {
            array_map('unlink', glob(public_path('/uploads/*' . $image)));
        }
    }

    /**
     * @param UploadedFile $image
     * @param string|null $oldImage
     * @return string|void
     */
    public static function uploadImage(UploadedFile $image, string $oldImage = null)
    {
        if(!$image instanceof UploadedFile) return;

        static::removeImage($oldImage);

        $path = public_path('/uploads/');

        $filename = str_random(20) .'.' . $image->getClientOriginalExtension() ?: 'png';

        $img = ImageInt::make($image);
        $img->save($path . $filename);
        $img->resize(348,261)->save($path . '348x261-' . $filename);

        return $filename;
    }
}
