<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['style.title', 'title']
            ]
        ];
    }

    protected $fillable = ['title', 'slug', 'description', 'parent_id', 'style_id'];

    /**
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasMany
     */
    public function category(): HasMany
    {
        return $this->hasMany(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function style(): BelongsTo
    {
        return $this->belongsTo(Style::class);
    }

    /**
     * @return HasMany
     */
    public function product(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return string
     */
    public function getParentTitle(): string
    {
        if ($this->parent_id != null) {
            return $this->parent->title;
        }
        return '-';
    }

    /**
     * @return string
     */
    public function getStyleTitle(): string
    {
        if ($this->style_id != null) {
            return $this->style->title;
        }
        return '-';
    }

    /**
     * @return mixed
     */
    public static function getParentCategories()
    {
        return static::whereNull('parent_id')->get();
    }

    /**
     * @param int $parentId
     * @param int $styleId
     * @return mixed
     */
    public static function getSubCategories(int $parentId, int $styleId)
    {
        return static::where('parent_id', '=', $parentId)->where('style_id', '=', $styleId)->get();
    }
}
