<?php

namespace App\Http\Controllers;

use App\Page;
use App\Siteinfo;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function about()
    {
        $page = Page::whereSlug('about')->first();

        return view('pages.page', compact('page'));
    }

    public function contacts()
    {
        $siteInfo = Siteinfo::first();

        return view('pages.contacts', compact('siteInfo'));
    }
}
