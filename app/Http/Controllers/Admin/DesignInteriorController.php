<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\DesignInterior;
use App\Images;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class DesignInteriorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $designinteriors = DesignInterior::paginate();

        return view('admin.designinteriors.index', compact('designinteriors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.designinteriors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'nullable',
            'image' => 'nullable',
            'gallery' => 'nullable',
            'is_active' => 'nullable|integer'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $filename = Images::uploadImage($request->file('image'));
            $requestData['image'] = $filename;
        }

        $newDesign = DesignInterior::create($requestData);

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $file) {
                $filename = Images::uploadImage($file);

                $storeImages = new Images([
                    'image' => $filename,
                    'parent' => 'designinterior',
                    'parent_id' => $newDesign['id']
                ]);
                $storeImages->save();
            }
        }

        return redirect()->route('designinteriors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DesignInterior  $designInterior
     * @return Response
     */
    public function show(DesignInterior $designInterior)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param DesignInterior $designinterior
     * @return Response
     */
    public function edit(DesignInterior $designinterior)
    {
        $images = Images::where('parent', '=', 'designinterior')->where('parent_id', '=', $designinterior->id)->pluck('image', 'id');
        return view('admin.designinteriors.edit', compact('designinterior', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param DesignInterior $designinterior
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, DesignInterior $designinterior)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'nullable',
            'image' => 'nullable',
            'gallery' => 'nullable',
            'is_active' => 'nullable|integer'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $filename = Images::uploadImage($request->file('image'));
            $requestData['image'] = $filename;
        }

        $designinterior->update($requestData);

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $file) {
                $filename = Images::uploadImage($file);

                $storeImages = new Images([
                    'image' => $filename,
                    'parent' => 'designinterior',
                    'parent_id' => $designinterior->id
                ]);
                $storeImages->save();
            }
        }

        return redirect()->route('designinteriors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DesignInterior $designinterior
     * @return Response
     * @throws \Exception
     */
    public function destroy(DesignInterior $designinterior)
    {
        Images::removeImage($designinterior->image);
        $designinterior->delete();
        return redirect()->route('designinteriors.index');
    }
}
