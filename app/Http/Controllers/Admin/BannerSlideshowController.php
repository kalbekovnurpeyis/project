<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BannerSlideshow;
use Illuminate\Http\Request;

class BannerSlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slideshows = BannerSlideshow::paginate();

        return view('admin.slideshows.index', compact('slideshows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BannerSlideshow  $bannerSlideshow
     * @return \Illuminate\Http\Response
     */
    public function show(BannerSlideshow $bannerSlideshow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BannerSlideshow  $bannerSlideshow
     * @return \Illuminate\Http\Response
     */
    public function edit(BannerSlideshow $bannerSlideshow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BannerSlideshow  $bannerSlideshow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BannerSlideshow $bannerSlideshow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BannerSlideshow  $bannerSlideshow
     * @return \Illuminate\Http\Response
     */
    public function destroy(BannerSlideshow $bannerSlideshow)
    {
        //
    }
}
