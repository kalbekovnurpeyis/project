<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Siteinfo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SiteinfosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $siteinfo = Siteinfo::first();

        return view('admin.siteinfos.index', compact('siteinfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siteinfo $siteinfo
     * @return void
     */
    public function show(Siteinfo $siteinfo)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        $siteinfo = Siteinfo::first();

        return view('admin.siteinfos.edit', compact('siteinfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Siteinfo $siteinfo
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Siteinfo $siteinfo)
    {
        $this->validate($request, [
            'phone_number_1' => 'required|regex:/^[0-9\-\(\)\/\+\s]*$/',
            'phone_number_2' => 'nullable|regex:/^[0-9\-\(\)\/\+\s]*$/',
            'phone_number_3' => 'nullable|regex:/^[0-9\-\(\)\/\+\s]*$/',
            'email' => 'nullable|email',
            'whatsapp' => 'nullable|integer',
            'facebook' => 'nullable|string|max:255',
            'instagram' => 'nullable|string|max:255',
            'odnoklassniki' => 'nullable|string|max:255',
            'work_time' => 'nullable|max:255',
            'address' => 'required|string|max:255',
            'coordinates' => 'nullable|string||max:255',
        ]);

        $siteinfo->update($request->all());

        return redirect()->route('siteinfo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siteinfo $siteinfo
     * @return void
     */
    public function destroy(Siteinfo $siteinfo)
    {
        abort(404);
    }
}
