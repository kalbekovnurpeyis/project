<?php

namespace App\Http\Controllers\Admin;

use App\Style;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StylesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $styles = Style::paginate();
        return view('admin.styles.index', compact('styles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.styles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'nullable',
        ]);

        Style::create($request->all());

        return redirect()->route('styles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Style $style
     * @return void
     */
    public function show(Style $style)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Style  $style
     * @return Response
     */
    public function edit(Style $style)
    {
        return view('admin.styles.edit', compact('style'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Style $style
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Style $style)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'nullable',
        ]);

        $style->update($request->all());

        return redirect()->route('styles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Style $style
     * @return Response
     * @throws \Exception
     */
    public function destroy(Style $style)
    {
        $style->delete();
        return redirect()->route('styles.index');
    }
}
