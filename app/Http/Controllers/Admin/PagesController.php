<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Images;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pages = Page::paginate();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'content' => 'required',
            'image' => 'nullable'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $filename = Images::uploadImage($request->file('image'));
            $requestData['image'] = $filename;
        }

        Page::create($requestData);

        return redirect()->route('pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page $page
     * @return void
     */
    public function show(Page $page)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'content' => 'required',
            'image' => 'nullable'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $filename = Images::uploadImage($request->file('image'), $page->image);
            $requestData['image'] = $filename;
        }

        $page->update($requestData);

        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page $page
     * @return Response
     * @throws \Exception
     */
    public function destroy(Page $page)
    {
        Images::removeImage($page->image);
        $page->delete();
        return redirect()->route('pages.index');
    }
}
