<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Images;
use App\Product;
use App\Http\Controllers\Controller;
use App\Style;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $products = Product::paginate();

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $categories = Category::whereNotNull('parent_id')->pluck('title', 'id');
        $styles = Style::pluck('title', 'id');
        return view('admin.products.create', compact('categories', 'styles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'nullable',
            'category_id' => 'required|numeric',
            'style_id' => 'required|numeric',
            'image' => 'nullable',
            'gallery' => 'nullable',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/'
        ]);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $filename = Images::uploadImage($request->file('image'));
            $requestData['image'] = $filename;
        }

        $newPoroduct = Product::create($requestData);

        if (!empty($request->file('gallery'))) {
            foreach ($request->file('gallery') as $file) {
                $filename = Images::uploadImage($file);

                $storeImages = new Images([
                    'image' => $filename,
                    'parent' => 'product',
                    'parent_id' => $newPoroduct['id']
                ]);
                $storeImages->save();
            }
        }

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function show(Product $product)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function edit(Product $product)
    {
        $categories = Category::whereNotNull('parent_id')->pluck('title', 'id');
        $styles = Style::pluck('title', 'id');
        $images = Images::where('parent', '=', 'product')->where('parent_id', '=', $product->id)->pluck('image', 'id');
        return view('admin.products.edit', compact('product', 'categories', 'styles', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Product $product
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'nullable',
            'category_id' => 'required|numeric',
            'style_id' => 'required|numeric',
            'image' => 'nullable',
            'gallery' => 'nullable',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
        ]);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $filename = Images::uploadImage($request->file('image'), $product->image);
            $requestData['image'] = $filename;
        }

        $product->update($requestData);

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $file) {
                $filename = Images::uploadImage($file);

                $storeImages = new Images([
                    'image' => $filename,
                    'parent' => 'product',
                    'parent_id' => $product->id
                ]);
                $storeImages->save();
            }
        }

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product $product
     * @return void
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        Images::removeImage($product->image);
        $product->delete();
        return redirect()->route('products.index');
    }
}
