<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $slug)
    {
        $category = Category::whereSlug($slug)->firstOrFail();
        $products = $category->product()->paginate(1);
        return view('categories.categories', compact('category', 'products'));
    }
}
