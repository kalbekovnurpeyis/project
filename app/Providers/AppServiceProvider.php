<?php

namespace App\Providers;

use App\Category;
use App\Page;
use App\Siteinfo;
use App\Style;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $parentCategories;
    public $styleCategories;
    public $subCats;
    public $siteInfo;
    public $pages;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // --- Nav Bar Rendering ---
        $this->parentCategories = Category::getParentCategories();
        $this->styleCategories = Style::getStylesList();
        $this->siteInfo = Siteinfo::getSiteInfo();
        $this->pages = Page::getPages();

        foreach ($this->parentCategories as $parent) {
            foreach ($this->styleCategories as $style) {
                $this->subCats[$parent->slug][$style->slug] = Category::getSubCategories($parent->id, $style->id);
            }
        }
        // --- Nav Bar Rendering End ---

        // --- For Header ---
        view()->composer('layouts.header', function($view){
            $view->with('pages', $this->pages);
            $view->with('siteInfo', $this->siteInfo);

            $view->with('parentCategories', $this->parentCategories);
            $view->with('styleCategories', $this->styleCategories);
            $view->with('subCats', $this->subCats);
        });

        // --- For Footer ---
        view()->composer('layouts.footer', function($view){
            $view->with('pages', $this->pages);

            $view->with('parentCategories', $this->parentCategories);
            $view->with('styleCategories', $this->styleCategories);
            $view->with('subCats', $this->subCats);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
