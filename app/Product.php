<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = ['title', 'slug', 'description', 'image', 'price', 'category_id', 'style_id'];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function style(): BelongsTo
    {
        return $this->belongsTo(Style::class);
    }

    /**
     * @return HasMany
     */
    public function image(): HasMany
    {
        return $this->hasMany(Images::class);
    }

    /**
     * @return string
     */
    public function getCatgoryTitle(): string
    {
        if ($this->category_id != null) {
            return $this->category->title;
        }
        return '-';
    }

    /**
     * @return string
     */
    public function getStyleTitle(): string
    {
        if ($this->style_id != null) {
            return $this->style->title;
        }
        return '-';
    }
}
