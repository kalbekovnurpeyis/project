<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DesignInterior extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = ['title', 'slug', 'description', 'is_active', 'image'];

    /**
     * @return HasMany
     */
    public function image(): HasMany
    {
        return $this->hasMany(Images::class);
    }
}
