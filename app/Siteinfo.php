<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siteinfo extends Model
{
    protected $fillable = [
        'phone_number_1',
        'phone_number_2',
        'phone_number_3',
        'email',
        'whatsapp',
        'facebook',
        'instagram',
        'odnoklassniki',
        'work_time',
        'address',
        'coordinates',
    ];

    public static function getSiteInfo()
    {
        return static::first();
    }
}
